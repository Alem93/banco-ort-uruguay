﻿using BancoOrtUruguay;
using Excepciones;
using FluentApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class ControladorCategoriaGasto
    {
        private ColeccionCategoriaGastos CategoriaGastos { get; set; }

        public ControladorCategoriaGasto()
        {
            this.CategoriaGastos = new ColeccionCategoriaGastos();
        }

        public void AltaDeCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {
            ValidarAltaDeCategoriaGasto(unaCategoriaGasto);
            ColeccionCategoriaGastos.AgregarCategoriaGasto(unaCategoriaGasto);
        }

        private void ValidarAltaDeCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {
            if (CategoriaSinNombre(unaCategoriaGasto))
            {
                throw new ExcepcionCategoriaGastoSinNombre();
            }
            else if (ExisteCategoriaGasto(unaCategoriaGasto))
            {
                throw new ExcepcionCategoriaGastoNombresIguales();
            }
        }

        private bool CategoriaSinNombre(CategoriaGasto unaCategoriaGasto)
        {
            return unaCategoriaGasto.Categoria.Equals(string.Empty);
        }

        public void RemoverCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {
            ColeccionCategoriaGastos.RemoverCategoriaGasto(unaCategoriaGasto);
        }

        public void AltaSubCategoriaGasto(CategoriaGasto unaCategoriaGasto, CategoriaGasto subCat)
        {
            ValidarHayCategoria(unaCategoriaGasto);
            ColeccionCategoriaGastos.AgregarSubCategoriaGasto(unaCategoriaGasto, subCat);
        }

        public void RemoverSubCategoriaGasto(CategoriaGasto unaCategoriaGasto, CategoriaGasto subcategoria)
        {
            ValidarHayCategoria(unaCategoriaGasto);
            ColeccionCategoriaGastos.RemoverSubCategoriaGasto(unaCategoriaGasto, subcategoria);
        }

        private void ValidarHayCategoria(CategoriaGasto unaCategoriaGasto)
        {
            if (!HayCategoria(unaCategoriaGasto))
            {
                throw new ExcepcionCategoriaGastoSinCategoria();
            }
        }

        private bool HayCategoria(CategoriaGasto unaCategoriaGasto)
        {
            return ExisteCategoriaGasto(unaCategoriaGasto);
        }

        public bool ExisteCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {
            return ColeccionCategoriaGastos.ExisteCategoriaGasto(unaCategoriaGasto);
        }

        public int ObtenerNumeroDeCategoriasRegistradas()
        {
            return ColeccionCategoriaGastos.ObtenerNumeroDeCategoriasRegistradas();
        }

        public List<CategoriaGasto> ObtenerCategoriasDeGastoEnUnaLista()
        {
            return ColeccionCategoriaGastos.ObtenerColeccionDeCategoriasDeGastoEnUnaLista();
        }
    }
}
