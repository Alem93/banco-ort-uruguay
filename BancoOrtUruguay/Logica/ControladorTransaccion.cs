﻿using BancoOrtUruguay;
using Excepciones;
using FluentApi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class ControladorTransaccion
    {
        private ColeccionTransacciones Transacciones { get; set; }

        public ControladorTransaccion()
        {
            this.Transacciones = new ColeccionTransacciones();
        }


        public void TransaccionDentroDelBanco(Transaccion unaTransaccion)
        {
            ValidarTransaccionDentroDelBanco(unaTransaccion);
            DebitarCuentaOrigen(unaTransaccion);
            AcreditarCuentaDestino(unaTransaccion);
            GuardarTransaccion(unaTransaccion);
        }

        public void TransaccionFueraDelBanco(Transaccion unaTransaccion)
        {
            ValidarTransaccionFueraDelBanco(unaTransaccion);

            DebitarCuentaOrigen(unaTransaccion);
            GuardarTransaccion(unaTransaccion);
            GenerarArchivoDeTexto(unaTransaccion);
        }

        private void GenerarArchivoDeTexto(Transaccion unaTransaccion)
        {
            string swift = ((TransaccionInternacional)unaTransaccion).NumeroSwift.ToString();
            string nroCuenta = ((TransaccionInternacional)unaTransaccion).NumeroCuentaDestino.ToString();
            string nombreDestinatario = ((TransaccionInternacional)unaTransaccion).NombreDestinatario;
            string monto = ((TransaccionInternacional)unaTransaccion).CantidadDebitada.ToString();
            string moneda = ((TransaccionInternacional)unaTransaccion).CuentaOrigen.UnaMoneda.ToString();

            string[] lineaTrInternacional =
                {
                "Numero de swift: " +swift,
                "Numero de cuenta de banco destinatario: " +nroCuenta,
                "Monto: " +monto,
                "Moneda: " +moneda
            };
            int nuevo = unaTransaccion.Id;
            string ruta = System.Configuration.ConfigurationManager.AppSettings["directorio"];
            System.IO.File.WriteAllLines(@ruta+"TransferenciaExterna" + "_" + nuevo + ".txt", lineaTrInternacional);
        }

        private void DebitarCuentaOrigen(Transaccion unaTransaccion)
        {
            ColeccionTransacciones.DebitarCuentaOrigen(unaTransaccion);
        }

        private void AcreditarCuentaDestino(Transaccion unaTransaccion)
        {
            ColeccionTransacciones.AcreditarCuentaDestino(unaTransaccion);
        }

        private void GuardarTransaccion(Transaccion unaTransaccion)
        {
            ColeccionTransacciones.AgregarTransaccion(unaTransaccion);
        }

        private void ValidarTransaccionDentroDelBanco(Transaccion unaTransaccion)
        {

            if (!HayCuentaOrigen(unaTransaccion))
            {
                throw new ExcepcionTransaccionSinCuentaOrigen();
            }
            else if (!HayCuentaDestino(unaTransaccion))
            {
                throw new ExcepcionTransaccionSinCuentaDestino();
            }
            else if (!HayCantidadATransferir(unaTransaccion))
            {
                throw new ExcepcionTransaccionSinCantidadATransferir();
            }
            else if (!HayCategoriaGasto(unaTransaccion))
            {
                throw new ExcepcionTransaccionSinCategoriaGasto();
            }
        }

        private void ValidarTransaccionFueraDelBanco(Transaccion unaTransaccion)
        {

            if (!HayCuentaOrigen(unaTransaccion))
            {
                throw new ExcepcionTransaccionSinCuentaOrigen();
            }
            else if (!HayCantidadATransferir(unaTransaccion))
            {
                throw new ExcepcionTransaccionSinCantidadATransferir();
            }
            else if (!HayCategoriaGasto(unaTransaccion))
            {
                throw new ExcepcionTransaccionSinCategoriaGasto();
            }
            else if (!HayNumeroDeSwift(unaTransaccion))
            {
                throw new ExcepcionTransaccionInternacionalNroSwiftInvalido();
            }
            else if (!HayNumeroCuentaDestino(unaTransaccion))
            {
                throw new ExcepcionTransaccionInternacionalSinNroCuentaDestino();
            }
            else if (!HayNombreDestinatario(unaTransaccion))
            {
                throw new ExcepcionTransaccionInternacionalSinNombreDestinatario();
            }
        }

        private bool HayCuentaOrigen(Transaccion unaTransaccion)
        {
            Cuenta unCuenta = new Cuenta();
            return !unaTransaccion.CuentaOrigen.Equals(unCuenta);
        }

        private bool HayCuentaDestino(Transaccion unaTransaccion)
        {
            Cuenta unCuenta = new Cuenta();
            return !unaTransaccion.CuentaDestino.Equals(unCuenta);
        }

        private bool HayCantidadATransferir(Transaccion unaTransaccion)
        {
            return unaTransaccion.CantidadDebitada > 0;
        }

        private bool HayCategoriaGasto(Transaccion unaTransaccion)
        {
            return !unaTransaccion.CategoriaDeGasto.Equals(string.Empty);
        }

        private bool HayNumeroDeSwift(Transaccion unaTransaccion)
        {
            return ((TransaccionInternacional)unaTransaccion).NumeroSwift != 0;
        }

        private bool HayNumeroCuentaDestino(Transaccion unaTransaccion)
        {
            return ((TransaccionInternacional)unaTransaccion).NumeroCuentaDestino != 0;
        }

        private bool HayNombreDestinatario(Transaccion unaTransaccion)
        {
            return ((TransaccionInternacional)unaTransaccion).NombreDestinatario != string.Empty;
        }

        public List<Transaccion> ObtenerTransaccionesEnUnaLista()
        {
            return ColeccionTransacciones.ObtenerColeccionTransaccionesEnUnaLista();
        }
    }
}
