﻿using BancoOrtUruguay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Logica
{
    public class Orchestrator
    {

        public ControladorCuenta controladorCuenta;
        public ControladorUsuario controladorUsuario;
        public ControladorMoneda controladorMoneda;
        public ControladorTipoCambio controladorTipoCambio;
        public ControladorTransaccion controladorTransaccion;
        public ControladorCategoriaGasto controladorCategoriaGasto;



        public Orchestrator()
        {
            this.controladorCuenta = new ControladorCuenta();
            this.controladorUsuario = new ControladorUsuario();
            this.controladorMoneda = new ControladorMoneda();
            this.controladorTipoCambio = new ControladorTipoCambio();
            this.controladorTransaccion = new ControladorTransaccion();
            this.controladorCategoriaGasto = new ControladorCategoriaGasto();
        }


    }


}
