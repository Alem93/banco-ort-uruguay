﻿using BancoOrtUruguay;
using Excepciones;
using FluentApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class ControladorMoneda
    {
        private ColeccionMonedas Monedas { get; set; }

        public ControladorMoneda()
        {
            this.Monedas = new ColeccionMonedas();
        }


        public void AltaDeMoneda(Moneda unaMoneda)
        {
            ValidarMoneda(unaMoneda);
            ColeccionMonedas.AgregarMoneda(unaMoneda);
        }

        public void ValidarMoneda(Moneda unaMoneda)
        {
            if (ExisteMoneda(unaMoneda))
            {
                throw new ExcepcionMonedaExiste();
            }
            else if (unaMoneda.Nombre == TIPO_DE_MONEDA.SIN_TIPO)
            {
                throw new ExcepcionMonedaSinNombre();
            }
        }

        public bool ExisteMoneda(Moneda unaMoneda)
        {
            return ColeccionMonedas.ExisteMoneda(unaMoneda);
        }

        public Moneda[] ObtenerMonedasEnUnArray()
        {
            return ColeccionMonedas.ObtenerColeccionEnUnArray();
        }

        public int ObtenerNumeroMonedasRegistradas()
        {
            return ColeccionMonedas.ObtenerNumeroDeMonedasRegistradas();
        }
    }
}
