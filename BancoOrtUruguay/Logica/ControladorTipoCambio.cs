﻿using BancoOrtUruguay;
using Excepciones;
using FluentApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class ControladorTipoCambio
    {

        public void AltaTipoDeCambio(TipoCambio unTipoDeCambio)
        {
            ValidarTipoDeCambio(unTipoDeCambio);
            ColeccionTipoCambio.AgregarTipoDeCambio(unTipoDeCambio);
        }

        private void ValidarTipoDeCambio(TipoCambio unTipoDeCambio)
        {
            if (!TieneMonedaOrigenDestinoDistinto(unTipoDeCambio))
            {
                throw new ExcepcionTipoCambioOrigenDestinoIguales();
            }
            else if (!TieneMonedaOrigenDestinoIngresada(unTipoDeCambio))
            {
                throw new ExcepcionTipoCambioSinMonedaAsignada();
            }
            else if (!TieneCantidadIngresada(unTipoDeCambio))
            {
                throw new ExcepcionTipoDeCambioSinCantidad();
            }


        }

        private bool TieneMonedaOrigenDestinoIngresada(TipoCambio unTipoDeCambio)
        {
            return unTipoDeCambio.Origen.Nombre != TIPO_DE_MONEDA.SIN_TIPO &&
                unTipoDeCambio.Destino.Nombre != TIPO_DE_MONEDA.SIN_TIPO;
        }

        private bool TieneCantidadIngresada(TipoCambio unTipoDeCambio)
        {
            return unTipoDeCambio.Compra > 0;
        }

        private bool TieneMonedaOrigenDestinoDistinto(TipoCambio unTipoDeCambio)
        {
            return unTipoDeCambio.Origen.Nombre != unTipoDeCambio.Destino.Nombre;
        }

        public void ConvertirTipoCambio(Moneda unaMonedaOrigen, ref double cantidadATransferir, Moneda unaMonedaDestino)
        {
            int invertirCambio = 1;

            TipoCambio tipoCambioAUsar = ColeccionTipoCambio.ObtenerTipoDeCambioMasReciente(unaMonedaOrigen, unaMonedaDestino);
            ValidarTipoCambio(tipoCambioAUsar);

            if (!EstaInvertido(tipoCambioAUsar, unaMonedaOrigen, unaMonedaDestino))
            {
                cantidadATransferir = cantidadATransferir * tipoCambioAUsar.Compra;
            }
            else
            {
                cantidadATransferir = cantidadATransferir* (invertirCambio/ tipoCambioAUsar.Compra);
            }
        }
        private void ValidarTipoCambio(TipoCambio tipoCambioAUsar)
        {

            if (tipoCambioAUsar==null)
            {
                throw new ExcepcionTipoDeCambioNoRegistrado();
            }
        }

        private bool EstaInvertido(TipoCambio tipoCambioAUsar, Moneda unaMonedaOrigen, Moneda unaMonedaDestino)
        {
            bool retorno= tipoCambioAUsar.Destino.Nombre == unaMonedaOrigen.Nombre &&
                tipoCambioAUsar.Origen.Nombre == unaMonedaDestino.Nombre;

            return retorno;
        }

        public bool ExisteTipoCambio(TipoCambio unTipoCambio)
        {
            return ColeccionTipoCambio.ExisteTipoCambio(unTipoCambio);
        }

    }
}
