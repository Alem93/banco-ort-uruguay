﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BancoOrtUruguay;
using Excepciones;
using FluentApi;

namespace Logica
{
    public class ControladorUsuario
    {
        private Usuario UsuarioLogueado { get; set; }

        public ControladorUsuario()
        {
            this.UsuarioLogueado = null;
        }


        public void AltaDeUsuario(Usuario usuario, string verificadorContrasenia)
        {
            ValidarUsuario(usuario, verificadorContrasenia);
            ColeccionUsuarios.AgregarUsuario(usuario);

        }


        public void EnviarMensajeUsuario(Mensaje mensaje, Usuario usuario)
        {
            if (mensaje.unMensaje == string.Empty)
            {
                throw new ExcepcionMensajeSinMensaje();
            }
            ColeccionUsuarios.EnviarMensaje(mensaje, usuario);
        }

        public void EliminarMensajeUsuario(Mensaje mensaje, Usuario usuario)
        {
            ColeccionUsuarios.EliminarMensaje(mensaje, usuario);
        }


        public void AgregarFavoritaUsuario(Favorita favorita, Usuario usuario, bool esDuenio, bool existeNroRef)
        {
            ValidarAgregarFavorita(favorita, usuario, esDuenio, existeNroRef);
            ColeccionUsuarios.AgregarFavorita(usuario,favorita);
        }

        private void ValidarAgregarFavorita(Favorita favorita, Usuario usuario, bool esDuenio, bool existeNroRef)
        {
            if (ExisteFavorita(favorita, usuario))
            {
                throw new ExcepcionFavoritaYaExiste();
            }
            else if (HayCampoVacioFavorita(favorita))
            {
                throw new ExcepcionFavoritaFaltanCompletarCampos();
            }
            else if (esDuenio)
            {
                throw new ExcepcionFavoritaDueniosIguales();
            }
            else if (!existeNroRef)
            {
                throw new ExcepcionCuentaNoExisteNroRef();
            }
        }

        private bool HayCampoVacioFavorita(Favorita favorita)
        {
            return favorita.Alias == string.Empty || favorita.NumeroDeReferencia == 0;
        }

        private bool ExisteFavorita(Favorita favorita, Usuario usuario)
        {
            return ColeccionUsuarios.ExisteFavorito(usuario, favorita);
        }

        public void EliminarFavoritaUsuario(Favorita favorita, Usuario usuario)
        {
            ColeccionUsuarios.EliminarFavorita(usuario,favorita);
        }


        private void ValidarUsuario(Usuario usuario, string verificadorContrasenia)
        {
            if (!ValidarContrasenia(usuario.Contrasenia, verificadorContrasenia))
            {
                throw new ExcepcionUsuarioContraseniaIncorrecta();
            }
            else if (ExisteUsuario(usuario))
            {
                throw new ExcepcionUsuarioExiste();
            }
        }

        public bool ExisteUsuario(Usuario usuario)
        {
            return ColeccionUsuarios.ExisteUsuario(usuario);
        }

        private bool ValidarContrasenia(string contrasenia, string verificadorContrasenia)
        {
            return contrasenia == verificadorContrasenia;
        }


        public void Login(int cedula, string contrasenia)
        {
            Usuario usuarioAObtener = ColeccionUsuarios.ObtenerUsuarioLogueado(cedula, contrasenia);
            ValidarLogin(usuarioAObtener);
            UsuarioLogueado = usuarioAObtener;
        }

        private void ValidarLogin(Usuario usuarioAObtener)
        {
            if (usuarioAObtener == null)
            {
                throw new ExcepcionLoginUsuarioInvalido();
            }
        }

        public Usuario ObtenerUsuarioLogin()
        {
            ValidarUsuarioLogin(UsuarioLogueado);
            return this.UsuarioLogueado;
        }

        private void ValidarUsuarioLogin(Usuario usuarioAObtener)
        {
            if (usuarioAObtener == null)
            {
                throw new ExcepcionLoginSinUsuario();
            }
        }

        public void DesLoguear()
        {
            this.UsuarioLogueado = null;
        }

        public Usuario[] ObtenerUsuariosClientesEnUnArray()
        {
            return ColeccionUsuarios.ObtenerColeccionEnUnArray();
        }

        public int ObtenerNumeroUsuariosRegistrados()
        {
            return ColeccionUsuarios.ObtenerNumeroDeUsuariosRegistrados();
        }

        public List<Usuario> ObtenerUsuariosEnUnaLista()
        {
            return ColeccionUsuarios.ObtenerColeccionUsuariosEnUnaLista();
        }
    }
}
