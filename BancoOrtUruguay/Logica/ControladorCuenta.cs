﻿using BancoOrtUruguay;
using Excepciones;
using FluentApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class ControladorCuenta
    {
        public void AprobarAperturaCuenta(ref Cuenta unaCuenta, ESTADO_SOLICITUD_CUENTA accion)
        {
            unaCuenta.EstadoCuenta = accion;
            if (accion == ESTADO_SOLICITUD_CUENTA.RECHAZADA)
            {
                ColeccionCuentas.RemoverCuenta(unaCuenta);
            }
        }

        public void SolicitudAperturaCuentaBancaria(Cuenta unaCuenta)
        {
            ValidarCuenta(unaCuenta);
            //El numero que identifica a la cuenta y es unico lo autoasigna el sistema
            int NroReferencia = AsignarNumeroDeReferencia();
            unaCuenta.NumeroDeReferencia = NroReferencia;
            ColeccionCuentas.AgregarCuenta(unaCuenta);
        }

        public void AltaCuentaBancaria(Cuenta unaCuenta)
        {
            ValidarCuenta(unaCuenta);
            //El numero que identifica a la cuenta y es unico lo autoasigna el sistema
            int NroReferencia = AsignarNumeroDeReferencia();
            unaCuenta.NumeroDeReferencia = NroReferencia;
            unaCuenta.EstadoCuenta = ESTADO_SOLICITUD_CUENTA.ACEPTADA;
            ColeccionCuentas.AgregarCuenta(unaCuenta);
        }

        public void CambiarSaldoMinimo(Cuenta unaCuenta, double nuevoSaldoMinimo)
        {
            unaCuenta.MontoMinimo = nuevoSaldoMinimo;
        }

        private void ValidarCuenta(Cuenta unaCuenta)
        {
            if (!TieneTipoDeCuentaAsignado(unaCuenta))
            {
                throw new ExcepcionCuentaSinTipoDeCuenta();
            }
            else if (!TieneUnaMonedaAsignada(unaCuenta))
            {
                throw new ExcepcionCuentaSinMoneda();
            }
            else if (!TieneUsarioAsignado(unaCuenta))
            {
                throw new ExcepcionCuentaSinUsuarioAsignado();
            }
        }

        private bool TieneTipoDeCuentaAsignado(Cuenta unaCuenta)
        {
            return unaCuenta.TipoDeCuenta != TIPO_DE_CUENTA.SIN_TIPO;
        }

        private bool TieneUnaMonedaAsignada(Cuenta unaCuenta)
        {
            return unaCuenta.UnaMoneda.Nombre != TIPO_DE_MONEDA.SIN_TIPO;
        }

        private bool TieneUsarioAsignado(Cuenta unaCuenta)
        {
            return unaCuenta.UnCliente.Cedula != 0;
        }

        private int AsignarNumeroDeReferencia()
        {
            if (ColeccionCuentas.ObtenerNumeroDeCuentasRegistradas() == 0)
            {
                return 1;
            }
            else
            {
                return ColeccionCuentas.ObtenerUltimoNumeroReferencia() + 1;
            }
        }

        public bool ExisteCuenta(Cuenta unaCuenta)
        {
            return ColeccionCuentas.ExisteCuenta(unaCuenta);
        }

        public bool EsDuenioDeCuenta(Cuenta unaCuenta, Usuario unUsuario)
        {
            return unaCuenta.UnCliente.Equals(unUsuario);
        }

        public int ObtenerNumeroDeCuentasRegistradas()
        {
            return ColeccionCuentas.ObtenerNumeroDeCuentasRegistradas();
        }

        public Cuenta[] ObtenerCuentasPendientesEnUnArray()
        {
            return ColeccionCuentas.ObtenerColeccionSegunEstadoEnUnArray(ESTADO_SOLICITUD_CUENTA.PENDIENTE);
        }

        public Cuenta[] ObtenerCuentasAceptadasEnUnArray()
        {
            return ColeccionCuentas.ObtenerColeccionSegunEstadoEnUnArray(ESTADO_SOLICITUD_CUENTA.ACEPTADA);
        }

        public Cuenta[] ObtenerCuentasDeUsuarioEnUnArray(Usuario unUsuario)
        {
            return ColeccionCuentas.ObtenerColeccionDeUsuarioEnUnArray(unUsuario);
        }

        public Cuenta ObtenerCuentaPorNumeroDeRefencia(int unNroRef)
        {
            return new Cuenta();// ColeccionCuentas.ObtenerCuentaPorNumeroDeRefencia(unNroRef);
        }

        public Cuenta[] ObtenerCuentasActivasDeUsuarioEnUnArray(Usuario unUsuario)
        {
            return ColeccionCuentas.ObtenerColeccionActivaDeUsuarioEnUnArray(unUsuario);
        }

        public List<Cuenta> ObtenerCuentasEnUnaLista()
        {
            return ColeccionCuentas.ObtenerColeccionCuentasEnUnaLista();
        }
    }
}
