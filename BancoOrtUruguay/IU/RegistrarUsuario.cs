﻿using BancoOrtUruguay;
using Excepciones;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class RegistrarUsuario : Form
    {
        private Orchestrator orchestrator;

        public RegistrarUsuario(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            CargarRolesUsuario();
        }


        private void CargarRolesUsuario()
        {
            this.listBoxRolesUsuario.DataSource = Enum.GetValues(typeof(TIPO_DE_USUARIO));
        }

        private void buttonRegistrarUsuario_Click(object sender, EventArgs e)
        {
            string nombre = (string)textBoxNombre.Text;
            int cedula;
            int.TryParse(textBoxCedula.Text, out cedula);
            string email = (string)textBoxEmail.Text;
            string contrasenia = (string)textBoxContrasenia.Text;
            string verificadorContrasenia = (string)textBoxVerificarContrasenia.Text;

            Usuario nuevoUsuario = new Usuario()
            {
                Nombre=nombre,
                Cedula=cedula,
                Email=email,
                Contrasenia=contrasenia
            };

            RolUsuario nuevoRol = new RolUsuario();
            var rolesSeleccionados = this.listBoxRolesUsuario.SelectedItems;
            

            foreach (TIPO_DE_USUARIO rol in rolesSeleccionados)
            {
                nuevoRol.NombreRol = rol;
                nuevoUsuario.RolesDeUsuario.Add(nuevoRol);
            }

            try
            {
                this.orchestrator.controladorUsuario.AltaDeUsuario(nuevoUsuario,verificadorContrasenia);
                NotificacionUsuarioRegistradaOK();
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void NotificacionUsuarioRegistradaOK()
        {
            MessageBox.Show("Se ha registrado el usuario correctamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            MenuAdmin();
        }

        private void MenuAdmin()
        {
            this.Hide();
            BackOffice menuAdmin = new BackOffice(orchestrator);
            menuAdmin.Show();
        }

        private void textBoxCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
           if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }
    }
}
