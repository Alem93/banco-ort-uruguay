﻿namespace IU
{
    partial class BackOffice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCambiarVista = new System.Windows.Forms.Button();
            this.buttonAltaCuenta = new System.Windows.Forms.Button();
            this.buttonAprobarCuenta = new System.Windows.Forms.Button();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.comboBoxRolesUsuario = new System.Windows.Forms.ComboBox();
            this.buttonCambioDia = new System.Windows.Forms.Button();
            this.buttonRegistrarUsuario = new System.Windows.Forms.Button();
            this.buttonCategoriaGasto = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxMonto = new System.Windows.Forms.TextBox();
            this.listBoxUsuarios = new System.Windows.Forms.ListBox();
            this.listBoxCuentas = new System.Windows.Forms.ListBox();
            this.listBoxTransacciones = new System.Windows.Forms.ListBox();
            this.buttonLimpiarFiltro = new System.Windows.Forms.Button();
            this.buttonFiltrarPorMonto = new System.Windows.Forms.Button();
            this.buttonFiltrarPorUsuario = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonDashBoard = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCambiarVista
            // 
            this.buttonCambiarVista.Location = new System.Drawing.Point(180, 30);
            this.buttonCambiarVista.Name = "buttonCambiarVista";
            this.buttonCambiarVista.Size = new System.Drawing.Size(106, 23);
            this.buttonCambiarVista.TabIndex = 2;
            this.buttonCambiarVista.Text = "Cambio de vista";
            this.buttonCambiarVista.UseVisualStyleBackColor = true;
            this.buttonCambiarVista.Click += new System.EventHandler(this.buttonCambiarVista_Click);
            // 
            // buttonAltaCuenta
            // 
            this.buttonAltaCuenta.Location = new System.Drawing.Point(38, 69);
            this.buttonAltaCuenta.Name = "buttonAltaCuenta";
            this.buttonAltaCuenta.Size = new System.Drawing.Size(90, 47);
            this.buttonAltaCuenta.TabIndex = 3;
            this.buttonAltaCuenta.Text = "Alta de Cuenta";
            this.buttonAltaCuenta.UseVisualStyleBackColor = true;
            this.buttonAltaCuenta.Click += new System.EventHandler(this.buttonAltaCuenta_Click);
            // 
            // buttonAprobarCuenta
            // 
            this.buttonAprobarCuenta.Location = new System.Drawing.Point(38, 122);
            this.buttonAprobarCuenta.Name = "buttonAprobarCuenta";
            this.buttonAprobarCuenta.Size = new System.Drawing.Size(90, 47);
            this.buttonAprobarCuenta.TabIndex = 4;
            this.buttonAprobarCuenta.Text = "Aprobacion de cuentas";
            this.buttonAprobarCuenta.UseVisualStyleBackColor = true;
            this.buttonAprobarCuenta.Click += new System.EventHandler(this.buttonAprobarCuenta_Click);
            // 
            // buttonLogin
            // 
            this.buttonLogin.Location = new System.Drawing.Point(637, 32);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(94, 23);
            this.buttonLogin.TabIndex = 16;
            this.buttonLogin.Text = "Cerrar sesion";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.button4_Click);
            // 
            // comboBoxRolesUsuario
            // 
            this.comboBoxRolesUsuario.FormattingEnabled = true;
            this.comboBoxRolesUsuario.Location = new System.Drawing.Point(38, 32);
            this.comboBoxRolesUsuario.Name = "comboBoxRolesUsuario";
            this.comboBoxRolesUsuario.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRolesUsuario.TabIndex = 1;
            // 
            // buttonCambioDia
            // 
            this.buttonCambioDia.Location = new System.Drawing.Point(38, 175);
            this.buttonCambioDia.Name = "buttonCambioDia";
            this.buttonCambioDia.Size = new System.Drawing.Size(90, 47);
            this.buttonCambioDia.TabIndex = 5;
            this.buttonCambioDia.Text = "Cambio del dia";
            this.buttonCambioDia.UseVisualStyleBackColor = true;
            this.buttonCambioDia.Click += new System.EventHandler(this.buttonCambioDia_Click);
            // 
            // buttonRegistrarUsuario
            // 
            this.buttonRegistrarUsuario.Location = new System.Drawing.Point(38, 228);
            this.buttonRegistrarUsuario.Name = "buttonRegistrarUsuario";
            this.buttonRegistrarUsuario.Size = new System.Drawing.Size(90, 47);
            this.buttonRegistrarUsuario.TabIndex = 6;
            this.buttonRegistrarUsuario.Text = "Registrar usuario";
            this.buttonRegistrarUsuario.UseVisualStyleBackColor = true;
            this.buttonRegistrarUsuario.Click += new System.EventHandler(this.buttonRegistrarUsuario_Click);
            // 
            // buttonCategoriaGasto
            // 
            this.buttonCategoriaGasto.Location = new System.Drawing.Point(38, 287);
            this.buttonCategoriaGasto.Name = "buttonCategoriaGasto";
            this.buttonCategoriaGasto.Size = new System.Drawing.Size(90, 47);
            this.buttonCategoriaGasto.TabIndex = 7;
            this.buttonCategoriaGasto.Text = "Mantenimiento categoria gasto";
            this.buttonCategoriaGasto.UseVisualStyleBackColor = true;
            this.buttonCategoriaGasto.Click += new System.EventHandler(this.buttonCategoriaGasto_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Lista de usuarios";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(534, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Lista de cuentas";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(237, 317);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Transacciones que superan monto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(534, 262);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Lista de transacciones";
            // 
            // textBoxMonto
            // 
            this.textBoxMonto.Location = new System.Drawing.Point(414, 314);
            this.textBoxMonto.Name = "textBoxMonto";
            this.textBoxMonto.Size = new System.Drawing.Size(100, 20);
            this.textBoxMonto.TabIndex = 12;
            this.textBoxMonto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxMonto_KeyPress);
            // 
            // listBoxUsuarios
            // 
            this.listBoxUsuarios.FormattingEnabled = true;
            this.listBoxUsuarios.Location = new System.Drawing.Point(180, 86);
            this.listBoxUsuarios.Name = "listBoxUsuarios";
            this.listBoxUsuarios.Size = new System.Drawing.Size(217, 134);
            this.listBoxUsuarios.TabIndex = 9;
            // 
            // listBoxCuentas
            // 
            this.listBoxCuentas.FormattingEnabled = true;
            this.listBoxCuentas.Location = new System.Drawing.Point(537, 86);
            this.listBoxCuentas.Name = "listBoxCuentas";
            this.listBoxCuentas.Size = new System.Drawing.Size(217, 134);
            this.listBoxCuentas.TabIndex = 11;
            // 
            // listBoxTransacciones
            // 
            this.listBoxTransacciones.FormattingEnabled = true;
            this.listBoxTransacciones.Location = new System.Drawing.Point(537, 278);
            this.listBoxTransacciones.Name = "listBoxTransacciones";
            this.listBoxTransacciones.Size = new System.Drawing.Size(217, 134);
            this.listBoxTransacciones.TabIndex = 14;
            // 
            // buttonLimpiarFiltro
            // 
            this.buttonLimpiarFiltro.Location = new System.Drawing.Point(297, 245);
            this.buttonLimpiarFiltro.Name = "buttonLimpiarFiltro";
            this.buttonLimpiarFiltro.Size = new System.Drawing.Size(100, 47);
            this.buttonLimpiarFiltro.TabIndex = 15;
            this.buttonLimpiarFiltro.Text = "Limpiar filtros";
            this.buttonLimpiarFiltro.UseVisualStyleBackColor = true;
            this.buttonLimpiarFiltro.Click += new System.EventHandler(this.buttonLimpiarFiltro_Click);
            // 
            // buttonFiltrarPorMonto
            // 
            this.buttonFiltrarPorMonto.Location = new System.Drawing.Point(414, 340);
            this.buttonFiltrarPorMonto.Name = "buttonFiltrarPorMonto";
            this.buttonFiltrarPorMonto.Size = new System.Drawing.Size(100, 47);
            this.buttonFiltrarPorMonto.TabIndex = 13;
            this.buttonFiltrarPorMonto.Text = "Filtrar por monto";
            this.buttonFiltrarPorMonto.UseVisualStyleBackColor = true;
            this.buttonFiltrarPorMonto.Click += new System.EventHandler(this.buttonFiltrarPorMonto_Click);
            // 
            // buttonFiltrarPorUsuario
            // 
            this.buttonFiltrarPorUsuario.Location = new System.Drawing.Point(414, 133);
            this.buttonFiltrarPorUsuario.Name = "buttonFiltrarPorUsuario";
            this.buttonFiltrarPorUsuario.Size = new System.Drawing.Size(100, 47);
            this.buttonFiltrarPorUsuario.TabIndex = 10;
            this.buttonFiltrarPorUsuario.Text = "Filtrar transaccion por usuario - ->";
            this.buttonFiltrarPorUsuario.UseVisualStyleBackColor = true;
            this.buttonFiltrarPorUsuario.Click += new System.EventHandler(this.buttonFiltrarPorUsuario_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Roles asignados";
            // 
            // buttonDashBoard
            // 
            this.buttonDashBoard.Location = new System.Drawing.Point(38, 340);
            this.buttonDashBoard.Name = "buttonDashBoard";
            this.buttonDashBoard.Size = new System.Drawing.Size(90, 47);
            this.buttonDashBoard.TabIndex = 8;
            this.buttonDashBoard.Text = "DashBoard de consumo";
            this.buttonDashBoard.UseVisualStyleBackColor = true;
            this.buttonDashBoard.Click += new System.EventHandler(this.buttonDashBoard_Click);
            // 
            // BackOffice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 454);
            this.Controls.Add(this.buttonDashBoard);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.buttonFiltrarPorUsuario);
            this.Controls.Add(this.buttonFiltrarPorMonto);
            this.Controls.Add(this.buttonLimpiarFiltro);
            this.Controls.Add(this.listBoxTransacciones);
            this.Controls.Add(this.listBoxCuentas);
            this.Controls.Add(this.listBoxUsuarios);
            this.Controls.Add(this.textBoxMonto);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCategoriaGasto);
            this.Controls.Add(this.buttonRegistrarUsuario);
            this.Controls.Add(this.buttonCambioDia);
            this.Controls.Add(this.comboBoxRolesUsuario);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.buttonAprobarCuenta);
            this.Controls.Add(this.buttonAltaCuenta);
            this.Controls.Add(this.buttonCambiarVista);
            this.Name = "BackOffice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BackOffice";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonCambiarVista;
        private System.Windows.Forms.Button buttonAltaCuenta;
        private System.Windows.Forms.Button buttonAprobarCuenta;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.ComboBox comboBoxRolesUsuario;
        private System.Windows.Forms.Button buttonCambioDia;
        private System.Windows.Forms.Button buttonRegistrarUsuario;
        private System.Windows.Forms.Button buttonCategoriaGasto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxMonto;
        private System.Windows.Forms.ListBox listBoxUsuarios;
        private System.Windows.Forms.ListBox listBoxCuentas;
        private System.Windows.Forms.ListBox listBoxTransacciones;
        private System.Windows.Forms.Button buttonLimpiarFiltro;
        private System.Windows.Forms.Button buttonFiltrarPorMonto;
        private System.Windows.Forms.Button buttonFiltrarPorUsuario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonDashBoard;
    }
}