﻿using BancoOrtUruguay;
using Excepciones;
using FluentApi;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class Login : Form
    {
        private Orchestrator orchestrator;
        private string MensajeError { get; set; }

        private RolUsuario rolAdmin;
        private RolUsuario rolCli;

        public Login(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            rolAdmin = new RolUsuario() { NombreRol = TIPO_DE_USUARIO.ADMINISTRADOR };
            rolCli = new RolUsuario() { NombreRol = TIPO_DE_USUARIO.CLIENTE };
        }


        private void buttonIniciarSesion_Click(object sender, EventArgs e)
        {
            string contrasenia = this.textBoxContrasenia.Text;
            int cedula;
            int.TryParse(this.textBoxIdentificador.Text, out cedula);

            try
            {
                orchestrator.controladorUsuario.Login(cedula, contrasenia);
                MostrarMenu();
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MostrarMenu()
        {

            Usuario usuarioLogin = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            bool esAdmin = usuarioLogin.RolesDeUsuario.Contains(rolAdmin);
            bool esCli = usuarioLogin.RolesDeUsuario.Contains(rolCli);

            if (esAdmin)
            {
                CerrarVentana();
                BackOffice backOffice = new BackOffice(this.orchestrator);
                backOffice.Show();
            }
            else if (esCli)
            {
                CerrarVentana();
                Cliente cliente = new Cliente(this.orchestrator);
                cliente.Show();
            }
        }

        private void CerrarVentana()
        {
            this.Hide();
        }

        private void textBoxIdentificador_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxIdentificador_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
           if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
        }

        private void buttonInicializar_Click(object sender, EventArgs e)
        {
            InicializarSistema();
        }

        private void InicializarSistema()
        {

            Usuario usuarioAdmin = new Usuario()
            {
                Cedula = 1,
                Contrasenia = "1",
                Email = "jose@",
                Nombre = "jose"
            };

            Usuario usuarioCli = new Usuario()
            {
                Cedula = 2,
                Contrasenia = "2",
                Email = "pepe@",
                Nombre = "pepe"
            };

            Usuario usuarioCliAdmin = new Usuario()
            {
                Cedula = 3,
                Contrasenia = "3",
                Email = "mati@",
                Nombre = "mati"
            };

            Moneda unaMonedaUSD = new Moneda()
            {
                Nombre = TIPO_DE_MONEDA.USD
            };

            Moneda unaMonedaUYU = new Moneda()
            {
                Nombre = TIPO_DE_MONEDA.UYU
            };

            Moneda unaMonedaSinTipo = new Moneda()
            {
                Nombre = TIPO_DE_MONEDA.SIN_TIPO
            };

            Cuenta unaCuentaUsuarioCli = new Cuenta()
            {
                UnCliente = usuarioCli,
                Balance = 500,
                Descripcion = "Cuenta de cli",
                UnaMoneda = unaMonedaUSD,
                TipoDeCuenta = TIPO_DE_CUENTA.CAJA_DE_AHORRO
            };

            Cuenta unaCuentaUsuarioCli2 = new Cuenta()
            {
                UnCliente = usuarioCli,
                Balance = 800,
                Descripcion = "Cuenta de cli",
                UnaMoneda = unaMonedaUYU,
                TipoDeCuenta = TIPO_DE_CUENTA.CUENTA_CORRIENTE
            };

            Cuenta unaCuentaUsuarioCliAdmin = new Cuenta()
            {
                UnCliente = usuarioCliAdmin,
                Balance = 800,
                Descripcion = "Cuenta de cliAdmin",
                UnaMoneda = unaMonedaUSD,
                TipoDeCuenta = TIPO_DE_CUENTA.CUENTA_CORRIENTE
            };

            usuarioAdmin.RolesDeUsuario.Add(rolAdmin);
            usuarioCli.RolesDeUsuario.Add(rolCli);
            usuarioCliAdmin.RolesDeUsuario.Add(rolCli);
            usuarioCliAdmin.RolesDeUsuario.Add(rolAdmin);
            orchestrator.controladorUsuario.AltaDeUsuario(usuarioAdmin, usuarioAdmin.Contrasenia);
            orchestrator.controladorUsuario.AltaDeUsuario(usuarioCliAdmin, usuarioCliAdmin.Contrasenia);
            orchestrator.controladorUsuario.AltaDeUsuario(usuarioCli, usuarioCli.Contrasenia);
            orchestrator.controladorMoneda.AltaDeMoneda(unaMonedaUSD);
            orchestrator.controladorMoneda.AltaDeMoneda(unaMonedaUYU);
            orchestrator.controladorCuenta.SolicitudAperturaCuentaBancaria(unaCuentaUsuarioCli2);
            orchestrator.controladorCuenta.AltaCuentaBancaria(unaCuentaUsuarioCli);
            orchestrator.controladorCuenta.AltaCuentaBancaria(unaCuentaUsuarioCliAdmin);
            MessageBox.Show("Se ha realizado la transaccion correctamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }
}
