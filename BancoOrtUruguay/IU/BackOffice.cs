﻿using BancoOrtUruguay;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class BackOffice : Form
    {
        private Orchestrator orchestrator;

        public BackOffice(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            CargarRolesUsuario();
            CargarTransacciones();
            CargarUsuarios();
            CargarCuentas();
        }


        private void CargarTransacciones()
        {
            this.listBoxTransacciones.DataSource = null;
            this.listBoxTransacciones.DataSource = orchestrator.controladorTransaccion.ObtenerTransaccionesEnUnaLista().ToArray();
        }

        private void CargarUsuarios()
        {
            this.listBoxUsuarios.DataSource = null;
            this.listBoxUsuarios.DataSource = orchestrator.controladorUsuario.ObtenerUsuariosEnUnaLista().ToArray();
        }

        private void CargarCuentas()
        {
            this.listBoxCuentas.DataSource = null;
            this.listBoxCuentas.DataSource = orchestrator.controladorCuenta.ObtenerCuentasEnUnaLista().ToArray();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.orchestrator.controladorUsuario.DesLoguear();
            Login login = new Login(this.orchestrator);
            CerrarVentana();
            login.Show();
        }

        private void buttonAltaCuenta_Click(object sender, EventArgs e)
        {
            AltaDeCuenta altaDeCuenta = new AltaDeCuenta(this.orchestrator);
            CerrarVentana();
            altaDeCuenta.Show();
        }

        private void CargarRolesUsuario()
        {
            Usuario usuarioLogueado = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            this.comboBoxRolesUsuario.DataSource = usuarioLogueado.RolesDeUsuario.ToArray();
        }

        private void CerrarVentana()
        {
            this.Close();
        }

        private void buttonCambiarVista_Click(object sender, EventArgs e)
        {
            string rolUsuario = comboBoxRolesUsuario.Text;
            string rolCli = "CLIENTE";

            if (rolCli == rolUsuario)
            {
                Cliente cliente = new Cliente(this.orchestrator);
                CerrarVentana();
                cliente.Show();
            }

        }

        private void buttonAprobarCuenta_Click(object sender, EventArgs e)
        {
            CerrarVentana();
            AprobacionDeCuentas aprobacionDeCuentas = new AprobacionDeCuentas(this.orchestrator);
            aprobacionDeCuentas.Show();
        }

        private void buttonCambioDia_Click(object sender, EventArgs e)
        {
            CerrarVentana();
            CambioDelDia cambioDelDia = new CambioDelDia(this.orchestrator);
            cambioDelDia.Show();
        }

        private void buttonRegistrarUsuario_Click(object sender, EventArgs e)
        {
            CerrarVentana();
            RegistrarUsuario registrarUsuario = new RegistrarUsuario(this.orchestrator);
            registrarUsuario.Show();
        }

        private void buttonCategoriaGasto_Click(object sender, EventArgs e)
        {
            CerrarVentana();
            MantenimientoCategoriaGastos mantenimientoCategoriaGastos = new MantenimientoCategoriaGastos(this.orchestrator);
            mantenimientoCategoriaGastos.Show();
        }

        private void buttonFiltrarPorUsuario_Click(object sender, EventArgs e)
        {
            if (listBoxUsuarios.SelectedIndex != -1)
            {
                Usuario unUsuario = (Usuario)listBoxUsuarios.SelectedItem;
                List<Cuenta> cuentasRecuperadas = orchestrator.controladorCuenta.ObtenerCuentasEnUnaLista();

                listBoxCuentas.DataSource = null;
                listBoxCuentas.DataSource = cuentasRecuperadas.FindAll
                    (
                    cuentasDeUsuario => cuentasDeUsuario.UnCliente.Equals(unUsuario)
                    ).ToArray();
            }
        }

        private void buttonLimpiarFiltro_Click(object sender, EventArgs e)
        {
            CargarCuentas();
            CargarTransacciones();
            CargarUsuarios();
        }

        private void buttonFiltrarPorMonto_Click(object sender, EventArgs e)
        {
            int monto=0;
            int.TryParse(textBoxMonto.Text, out monto);
            List<Transaccion> transaccionesRecuperadas = orchestrator.controladorTransaccion.ObtenerTransaccionesEnUnaLista();

            listBoxTransacciones.DataSource = null;
            listBoxTransacciones.DataSource = transaccionesRecuperadas.FindAll
                (
                transaccionesFiltroPorMonto => transaccionesFiltroPorMonto.CantidadDebitada>monto
                ).ToArray();
        }

        private void textBoxMonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
           if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void buttonDashBoard_Click(object sender, EventArgs e)
        {
            DashBoardConsumo dashBoardConsumo = new DashBoardConsumo(this.orchestrator);
            dashBoardConsumo.Show();
        }
    }
}
