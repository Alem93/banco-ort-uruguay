﻿using BancoOrtUruguay;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace IU
{
    public partial class DashBoardConsumo : Form
    {
        private Orchestrator orchestrator;


        private List<Transaccion> TransaccionesConCatSubCat;

        private List<string> CategoriasConSubcategorias;
        private List<double> CategoriasConSubcategoriasMontos;

        private static char[] SEPARADOR = { '-' };
        private static string SIN_SUB_CAT = "Sin SubCategoria";

        private List<string> Categorias;
        private List<double> CategoriaMontos;

        private List<string> SubCategorias;
        private List<double> SubCategoriaMontos;
        public DashBoardConsumo(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;

            this.TransaccionesConCatSubCat = new List<Transaccion>();

            this.CategoriasConSubcategorias = new List<string>();
            this.CategoriasConSubcategoriasMontos = new List<double>();

            this.Categorias = new List<string>();
            this.CategoriaMontos = new List<double>();

            this.SubCategorias = new List<string>();
            this.SubCategoriaMontos = new List<double>();

            CargarDashBoardPorCategoria();
        }

        public void CargarDashBoardPorCategoria()
        {

            foreach (Form frm in Application.OpenForms)
            {
                if (frm.GetType() == typeof(Cliente))
                {
                    MessageBox.Show("El formulario cliente esta abierto");
                    CargarTransaccionesDelCliente();
                    break;
                }
                else if (frm.GetType() == typeof(BackOffice))
                {
                    MessageBox.Show("El formulario backOffice esta abierto");
                    CargarTodasLasTransacciones();
                    break;
                }
            }

            foreach (Transaccion tr in TransaccionesConCatSubCat)
            {
                if (this.CategoriasConSubcategorias.Contains(tr.CategoriaDeGasto))
                {
                    int pos = this.CategoriasConSubcategorias.IndexOf(tr.CategoriaDeGasto);
                    this.CategoriasConSubcategoriasMontos[pos] = this.CategoriasConSubcategoriasMontos[pos] + tr.CantidadDebitada;
                }
                else
                {
                    this.CategoriasConSubcategorias.Add(tr.CategoriaDeGasto);
                    this.CategoriasConSubcategoriasMontos.Add(tr.CantidadDebitada);
                }
            }

            foreach (string t in CategoriasConSubcategorias)
            {
                string[] obtenerCategoria = t.Split(SEPARADOR, System.StringSplitOptions.RemoveEmptyEntries);

                int POS_CAT = 0;

                if (this.Categorias.Contains(obtenerCategoria[POS_CAT]))
                {

                    int posASumar = this.CategoriasConSubcategorias.IndexOf(t);
                    double montoASumar = this.CategoriasConSubcategoriasMontos[posASumar];

                    int posCatExistente = this.Categorias.IndexOf(obtenerCategoria[POS_CAT]);
                    this.CategoriaMontos[posCatExistente] = this.CategoriaMontos[posCatExistente] + montoASumar;
                }
                else
                {
                    this.Categorias.Add(obtenerCategoria[POS_CAT]);
                    int posASumar = this.CategoriasConSubcategorias.IndexOf(t);
                    this.CategoriaMontos.Add(this.CategoriasConSubcategoriasMontos[posASumar]);

                }
            }

            chartConsumo.Palette = ChartColorPalette.Berry;
            chartConsumo.Titles.Add("Estadisticas por categoria");

            for (int i = 0; i < this.Categorias.Count; i++)
            {
                Series serie = chartConsumo.Series.Add(this.Categorias[i]);

                serie.Points.Add(CategoriaMontos[i]);
                serie.AxisLabel = this.Categorias[i];
                serie.LegendText = this.Categorias[i];
                serie.Label = CategoriaMontos[i].ToString();
            }


        }

        public void CargarTodasLasTransacciones()
        {
            List<Transaccion> transaccionesRecuperadas = orchestrator.controladorTransaccion.ObtenerTransaccionesEnUnaLista();
            this.TransaccionesConCatSubCat = transaccionesRecuperadas;
        }

        public void CargarTransaccionesDelCliente()
        {
            Usuario usuarioLogueado = orchestrator.controladorUsuario.ObtenerUsuarioLogin();

            List<Transaccion> transaccionesRecuperadas = orchestrator.controladorTransaccion.ObtenerTransaccionesEnUnaLista();
            List<Transaccion> transaccionesDelCliente = transaccionesRecuperadas.FindAll(
                transaccionesDeUsuario => transaccionesDeUsuario.CuentaOrigen.UnCliente.Equals(usuarioLogueado) ||
                transaccionesDeUsuario.CuentaDestino.UnCliente.Equals(usuarioLogueado)
                );
            this.TransaccionesConCatSubCat = transaccionesDelCliente;
        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chartConsumo_Click(object sender, EventArgs e)
        {
            var chart = ((Chart)sender);
            var mouseEvent = (MouseEventArgs)e;
            var result = chart.HitTest(mouseEvent.X, mouseEvent.Y);
            if (result.ChartElementType == ChartElementType.DataPoint)
            {
                string nombreCategoriaSeleccionada = result.Series.Name;

                foreach (string t in CategoriasConSubcategorias)
                {
                    string[] categoriaConSubcategoria = t.Split(SEPARADOR, System.StringSplitOptions.RemoveEmptyEntries);
                    int POS_CAT = 0;
                    string obtenerCategoria = categoriaConSubcategoria[POS_CAT];
                    int POS_SUB_CAT = 1;
                    string obtenerSubCategoria = categoriaConSubcategoria[POS_SUB_CAT];


                    if (this.SubCategorias.Contains(obtenerSubCategoria) && obtenerCategoria == nombreCategoriaSeleccionada)
                    {
                        int posASumar = this.CategoriasConSubcategorias.IndexOf(t);
                        double montoASumar = this.CategoriasConSubcategoriasMontos[posASumar];

                        int posCatExistente = this.SubCategorias.IndexOf(categoriaConSubcategoria[POS_SUB_CAT]);
                        this.SubCategoriaMontos[posCatExistente] = this.SubCategoriaMontos[posCatExistente] + montoASumar;
                    }
                    else if (obtenerSubCategoria != SIN_SUB_CAT && obtenerCategoria == nombreCategoriaSeleccionada)
                    {
                        this.SubCategorias.Add(categoriaConSubcategoria[POS_SUB_CAT]);
                        int posASumar = this.CategoriasConSubcategorias.IndexOf(t);
                        this.SubCategoriaMontos.Add(this.CategoriasConSubcategoriasMontos[posASumar]);
                    }
                }

                for (int i = 0; i < this.SubCategorias.Count; i++)
                {
                    this.chartConsumo.Series.Clear();
                    Series serie = chartConsumo.Series.Add(this.SubCategorias[i]);

                    serie.Points.Add(SubCategoriaMontos[i]);
                    serie.AxisLabel = this.SubCategorias[i];
                    serie.LegendText = this.SubCategorias[i];
                    serie.Label = SubCategoriaMontos[i].ToString();
                }

                if (this.SubCategorias.Count > 0)
                {
                    chartConsumo.Palette = ChartColorPalette.BrightPastel;
                }
            }
        }
    }
}
