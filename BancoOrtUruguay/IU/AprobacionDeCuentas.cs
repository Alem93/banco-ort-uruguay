﻿using BancoOrtUruguay;
using Excepciones;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class AprobacionDeCuentas : Form
    {
        private Orchestrator orchestrator;

        public AprobacionDeCuentas(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            CargarCuentasPendientes();
            CargarCuentasDeBanco();
        }


        public void CargarCuentasPendientes()
        {
            this.listBoxCuentasPendientes.DataSource = null;
            this.listBoxCuentasPendientes.DataSource = orchestrator.controladorCuenta.ObtenerCuentasPendientesEnUnArray();
        }

        public void CargarCuentasDeBanco()
        {
            this.listBoxCuentasBanco.DataSource = null;
            this.listBoxCuentasBanco.DataSource = orchestrator.controladorCuenta.ObtenerCuentasEnUnaLista().ToArray();
        }
        private void MenuAdmin()
        {
            this.Hide();
            BackOffice menuAdmin = new BackOffice(orchestrator);
            menuAdmin.Show();
        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            MenuAdmin();
        }

        private void buttonAprobar_Click(object sender, EventArgs e)
        {
            AceptaRechazarCuenta(ESTADO_SOLICITUD_CUENTA.ACEPTADA);
        }

        private void buttonRechazar_Click(object sender, EventArgs e)
        {
            AceptaRechazarCuenta(ESTADO_SOLICITUD_CUENTA.RECHAZADA);
        }

        private void NotificacionCuentaAccionOK()
        {
            MessageBox.Show("Se ha realizado la accion correctamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void AprobacionDeCuentas_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void EnvioMensajeUsuario(string mensaje, Usuario usuario)
        {
            Mensaje nuevoMensaje = new Mensaje() { unMensaje = mensaje };

            this.orchestrator.controladorUsuario.EnviarMensajeUsuario(nuevoMensaje, usuario);
        }

        private void AceptaRechazarCuenta(ESTADO_SOLICITUD_CUENTA accionSobreCuenta)
        {
            Cuenta unaCuenta = new Cuenta();
            if (listBoxCuentasPendientes.SelectedIndex != -1)
            {
                unaCuenta = (Cuenta)listBoxCuentasPendientes.SelectedItem;
            }

            Usuario usuCli = unaCuenta.UnCliente;
            string unMotivo = textBoxMotivo.Text;
            try
            {
                bool sinCuentas = orchestrator.controladorCuenta.ExisteCuenta(unaCuenta);
                if (sinCuentas)
                {
                    EnvioMensajeUsuario(unMotivo, usuCli);
                    this.orchestrator.controladorCuenta.AprobarAperturaCuenta(ref unaCuenta, accionSobreCuenta);
                    NotificacionCuentaAccionOK();
                    this.textBoxMotivo.Clear();
                }
                CargarCuentasPendientes();      
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonCambiarSaldoMinimo_Click(object sender, EventArgs e)
        {
            Cuenta unaCuenta = new Cuenta();

            double saldoMinimoNuevo;
            double.TryParse(textBoxSaldoMinimoNuevo.Text, out saldoMinimoNuevo);
            if (listBoxCuentasBanco.SelectedIndex != -1)
            {
                unaCuenta = (Cuenta)listBoxCuentasBanco.SelectedItem;
            }

            Usuario usuCli = unaCuenta.UnCliente;
            string unMotivo = textBoxMotivo.Text;
            try
            {
                bool sinCuentas = orchestrator.controladorCuenta.ExisteCuenta(unaCuenta);
                if (sinCuentas)
                {
                    EnvioMensajeUsuario(unMotivo, usuCli);
                    this.orchestrator.controladorCuenta.CambiarSaldoMinimo(unaCuenta,saldoMinimoNuevo);
                    NotificacionCuentaAccionOK();
                    this.textBoxMotivo.Clear();
                }
                CargarCuentasDeBanco();
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void SoloNumerosEnTextBox(KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
           if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void textBoxSaldoMinimoNuevo_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumerosEnTextBox(e);
        }
    }
}
