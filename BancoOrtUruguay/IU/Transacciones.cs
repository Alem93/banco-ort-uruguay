﻿using BancoOrtUruguay;
using Excepciones;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class Transacciones : Form
    {
        private Orchestrator orchestrator;
        private Usuario usuarioLogin;

        public Transacciones(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            CargarListaCuentas();
            CargarFavoritas();
            CargarCategorias();
            cargarSubCategorias();
        }


        public void CargarListaCuentas()
        {
            this.listBoxCuentaOrigen.DataSource = null;
            this.listBoxCuentaDestino.DataSource = null;
            this.usuarioLogin = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            this.listBoxCuentaOrigen.DataSource = orchestrator.controladorCuenta.ObtenerCuentasActivasDeUsuarioEnUnArray(usuarioLogin);
            this.listBoxCuentaDestino.DataSource = orchestrator.controladorCuenta.ObtenerCuentasAceptadasEnUnArray();
        }

        public void CargarFavoritas()
        {
            this.listBoxFavoritas.DataSource = null;
            this.usuarioLogin = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            this.listBoxFavoritas.DataSource = usuarioLogin.Favoritas.ToArray();
        }

        private void CargarCategorias()
        {

            List<CategoriaGasto> catGastos = orchestrator.controladorCategoriaGasto.ObtenerCategoriasDeGastoEnUnaLista();

            foreach (CategoriaGasto cat in catGastos)
            {
                comboBoxCategoria.Items.Add(cat.Categoria);
            }
        }

        private void cargarSubCategorias()
        {
            if (comboBoxCategoria.SelectedIndex != -1)
            {
                comboBoxSubCat.Items.Clear();
                
                string nombreCat = comboBoxCategoria.SelectedItem.ToString();
                List<CategoriaGasto> todasLasCatGastos = orchestrator.controladorCategoriaGasto.ObtenerCategoriasDeGastoEnUnaLista();

                CategoriaGasto recuperarCat = todasLasCatGastos.Find(unaCategoria => unaCategoria.Categoria.Equals(nombreCat));

                List<CategoriaGasto> subCat = recuperarCat.SubCategorias;

                foreach (CategoriaGasto sub in subCat)
                {
                    comboBoxSubCat.Items.Add(sub.Categoria);
                }
            }
        }
        private void buttonTransferir_Click(object sender, EventArgs e)
        {
            bool esTransferenciaFueraDelBanco = checkBoxTransferenciaFueraDelBanco.Checked;

            Cuenta cuentaOrigen = new Cuenta();
            Cuenta cuentaDestino = new Cuenta();
            Moneda monedaOrigen = new Moneda();
            Moneda monedaDestino = new Moneda();
            string unaCategoriaGasto = string.Empty;

            unaCategoriaGasto = GuardarCategoria();

            if (listBoxCuentaOrigen.SelectedIndex != -1)
            {
                cuentaOrigen = (Cuenta)listBoxCuentaOrigen.SelectedItem;
                monedaOrigen.Nombre = cuentaOrigen.UnaMoneda.Nombre;
            }

            double cantidadTransferir;
            double.TryParse(textBoxCantidadTransferir.Text, out cantidadTransferir);

            long numeroDeSwift;
            long.TryParse(textBoxNroSwift.Text, out numeroDeSwift);

            long numeroDeCuentaDestino;
            long.TryParse(textBoxNroCuentaDestinatario.Text, out numeroDeCuentaDestino);

            string nombreDelDestinatario=textBoxNombreDestinatario.Text;

            Transaccion nuevaTransaccionLocal = new Transaccion()
                {
                    CantidadDebitada = cantidadTransferir,
                    CuentaOrigen = cuentaOrigen,
                    CuentaDestino = cuentaDestino,
                    CategoriaDeGasto = unaCategoriaGasto
                };

            Transaccion nuevaTransaccionInternacional = new TransaccionInternacional()
            {
                CantidadDebitada = cantidadTransferir,
                CuentaOrigen = cuentaOrigen,
                CuentaDestino = cuentaDestino,
                CategoriaDeGasto = unaCategoriaGasto,
                NombreDestinatario=nombreDelDestinatario,
                NumeroCuentaDestino=numeroDeCuentaDestino,
                NumeroSwift=numeroDeSwift,   
            };


            try
            {
                bool esTransferenciaFavorita = this.listBoxFavoritas.SelectedIndex != -1;
                bool esTransferenciaDestino = this.listBoxCuentaDestino.SelectedIndex != -1;

                if (esTransferenciaFueraDelBanco)
                {
                    orchestrator.controladorTransaccion.TransaccionFueraDelBanco(nuevaTransaccionInternacional);
                    NotificacionTransaccionRegistradaOK();
                    CargarListaCuentas();
                }
                else if (esTransferenciaFavorita)
                {
                    Favorita unaFavorita = (Favorita)this.listBoxFavoritas.SelectedItem;
                    int nroRef = unaFavorita.NumeroDeReferencia;
                    Cuenta cuentaFavorita = orchestrator.controladorCuenta.ObtenerCuentaPorNumeroDeRefencia(nroRef);
                    monedaDestino.Nombre = cuentaFavorita.UnaMoneda.Nombre;
                    nuevaTransaccionLocal.CuentaDestino = cuentaFavorita;

                    if (HayQueHacerConversion(monedaOrigen, monedaDestino))
                    {
                        orchestrator.controladorTipoCambio.ConvertirTipoCambio(monedaOrigen, ref cantidadTransferir, monedaDestino);
                    }

                    orchestrator.controladorTransaccion.TransaccionDentroDelBanco(nuevaTransaccionLocal);
                    NotificacionTransaccionRegistradaOK();
                    CargarListaCuentas();
                }
                else if (esTransferenciaDestino)
                {
                    nuevaTransaccionLocal.CuentaDestino = (Cuenta)listBoxCuentaDestino.SelectedItem;
                    monedaDestino.Nombre = nuevaTransaccionLocal.CuentaDestino.UnaMoneda.Nombre;

                    if (HayQueHacerConversion(monedaOrigen, monedaDestino))
                    {
                        orchestrator.controladorTipoCambio.ConvertirTipoCambio(monedaOrigen, ref cantidadTransferir, monedaDestino);
                    }

                    orchestrator.controladorTransaccion.TransaccionDentroDelBanco(nuevaTransaccionLocal);
                    NotificacionTransaccionRegistradaOK();
                    CargarListaCuentas();
                }
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GuardarCategoria()
        {
            StringBuilder stringBuilder = new StringBuilder();
            char Separador = '-';

            if (comboBoxCategoria.SelectedIndex != -1)
            {
                stringBuilder.Append((string)comboBoxCategoria.SelectedItem);
                stringBuilder.Append(Separador);

                if (comboBoxSubCat.SelectedIndex != -1)
                {
                    stringBuilder.Append((string)comboBoxSubCat.SelectedItem);
                }
                else
                {
                    string sinSubCat = "Sin SubCategoria";
                    stringBuilder.Append(sinSubCat);
                }
            }

            return stringBuilder.ToString();
        }

        private bool HayQueHacerConversion(Moneda origen, Moneda destino)
        {
            return !origen.Equals(destino);
        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            MenuCli();
        }

        private void MenuCli()
        {
            this.Hide();
            Cliente menuCli = new Cliente(orchestrator);
            menuCli.Show();
        }

        private void textBoxCantidadTransferir_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumerosEnTextBox(e);
        }

        private void SoloNumerosEnTextBox(KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
           if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }
        private void NotificacionTransaccionRegistradaOK()
        {
            MessageBox.Show("Se ha realizado la transaccion correctamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void listBoxCuentaDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxFavoritas.SelectedIndex != -1)
            {
                this.listBoxFavoritas.ClearSelected();
            }

            if (this.checkBoxTransferenciaFueraDelBanco.Checked == true)
            {
                this.checkBoxTransferenciaFueraDelBanco.Checked = false;
            }
        }

        private void listBoxFavoritas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxCuentaDestino.SelectedIndex != -1)
            {
                this.listBoxCuentaDestino.ClearSelected();
            }

            if (this.checkBoxTransferenciaFueraDelBanco.Checked == true)
            {
                this.checkBoxTransferenciaFueraDelBanco.Checked = false;
            }
        }

        private void checkBoxTransferenciaFueraDelBanco_CheckedChanged(object sender, EventArgs e)
        {
            if (listBoxFavoritas.SelectedIndex != -1)
            {
                this.listBoxFavoritas.ClearSelected();
            }

            if (listBoxCuentaDestino.SelectedIndex != -1)
            {
                this.listBoxCuentaDestino.ClearSelected();
            }
        }

        private void comboBoxCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarSubCategorias();
        }

        private void comboBoxCategoria_Click(object sender, EventArgs e)
        {
            cargarSubCategorias();
        }

        private void textBoxNroSwift_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumerosEnTextBox(e);
        }

        private void textBoxNroCuentaDestinatario_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumerosEnTextBox(e);
        }
    }
}
