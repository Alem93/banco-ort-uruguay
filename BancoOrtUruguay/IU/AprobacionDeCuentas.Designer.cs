﻿namespace IU
{
    partial class AprobacionDeCuentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxCuentasPendientes = new System.Windows.Forms.ListBox();
            this.buttonAprobar = new System.Windows.Forms.Button();
            this.buttonRechazar = new System.Windows.Forms.Button();
            this.buttonVolver = new System.Windows.Forms.Button();
            this.labelMotivo = new System.Windows.Forms.Label();
            this.textBoxMotivo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxCuentasBanco = new System.Windows.Forms.ListBox();
            this.buttonCambiarSaldoMinimo = new System.Windows.Forms.Button();
            this.textBoxSaldoMinimoNuevo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBoxCuentasPendientes
            // 
            this.listBoxCuentasPendientes.FormattingEnabled = true;
            this.listBoxCuentasPendientes.Location = new System.Drawing.Point(499, 52);
            this.listBoxCuentasPendientes.Name = "listBoxCuentasPendientes";
            this.listBoxCuentasPendientes.Size = new System.Drawing.Size(336, 264);
            this.listBoxCuentasPendientes.TabIndex = 4;
            // 
            // buttonAprobar
            // 
            this.buttonAprobar.Location = new System.Drawing.Point(408, 222);
            this.buttonAprobar.Name = "buttonAprobar";
            this.buttonAprobar.Size = new System.Drawing.Size(85, 44);
            this.buttonAprobar.TabIndex = 6;
            this.buttonAprobar.Text = "Aprobar cuenta";
            this.buttonAprobar.UseVisualStyleBackColor = true;
            this.buttonAprobar.Click += new System.EventHandler(this.buttonAprobar_Click);
            // 
            // buttonRechazar
            // 
            this.buttonRechazar.Location = new System.Drawing.Point(405, 272);
            this.buttonRechazar.Name = "buttonRechazar";
            this.buttonRechazar.Size = new System.Drawing.Size(88, 44);
            this.buttonRechazar.TabIndex = 7;
            this.buttonRechazar.Text = "Rechazar cuenta";
            this.buttonRechazar.UseVisualStyleBackColor = true;
            this.buttonRechazar.Click += new System.EventHandler(this.buttonRechazar_Click);
            // 
            // buttonVolver
            // 
            this.buttonVolver.Location = new System.Drawing.Point(737, 333);
            this.buttonVolver.Name = "buttonVolver";
            this.buttonVolver.Size = new System.Drawing.Size(88, 44);
            this.buttonVolver.TabIndex = 8;
            this.buttonVolver.Text = "Volver";
            this.buttonVolver.UseVisualStyleBackColor = true;
            this.buttonVolver.Click += new System.EventHandler(this.buttonVolver_Click);
            // 
            // labelMotivo
            // 
            this.labelMotivo.AutoSize = true;
            this.labelMotivo.Location = new System.Drawing.Point(284, 33);
            this.labelMotivo.Name = "labelMotivo";
            this.labelMotivo.Size = new System.Drawing.Size(153, 13);
            this.labelMotivo.TabIndex = 4;
            this.labelMotivo.Text = "Motivo de aceptacion/rechazo";
            // 
            // textBoxMotivo
            // 
            this.textBoxMotivo.Location = new System.Drawing.Point(287, 60);
            this.textBoxMotivo.Name = "textBoxMotivo";
            this.textBoxMotivo.Size = new System.Drawing.Size(150, 20);
            this.textBoxMotivo.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(499, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Cuentas pendientes";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Cuentas en el banco";
            // 
            // listBoxCuentasBanco
            // 
            this.listBoxCuentasBanco.FormattingEnabled = true;
            this.listBoxCuentasBanco.Location = new System.Drawing.Point(16, 52);
            this.listBoxCuentasBanco.Name = "listBoxCuentasBanco";
            this.listBoxCuentasBanco.Size = new System.Drawing.Size(223, 264);
            this.listBoxCuentasBanco.TabIndex = 1;
            // 
            // buttonCambiarSaldoMinimo
            // 
            this.buttonCambiarSaldoMinimo.Location = new System.Drawing.Point(245, 171);
            this.buttonCambiarSaldoMinimo.Name = "buttonCambiarSaldoMinimo";
            this.buttonCambiarSaldoMinimo.Size = new System.Drawing.Size(85, 43);
            this.buttonCambiarSaldoMinimo.TabIndex = 3;
            this.buttonCambiarSaldoMinimo.Text = "Aceptar";
            this.buttonCambiarSaldoMinimo.UseVisualStyleBackColor = true;
            this.buttonCambiarSaldoMinimo.Click += new System.EventHandler(this.buttonCambiarSaldoMinimo_Click);
            // 
            // textBoxSaldoMinimoNuevo
            // 
            this.textBoxSaldoMinimoNuevo.Location = new System.Drawing.Point(245, 133);
            this.textBoxSaldoMinimoNuevo.Name = "textBoxSaldoMinimoNuevo";
            this.textBoxSaldoMinimoNuevo.Size = new System.Drawing.Size(100, 20);
            this.textBoxSaldoMinimoNuevo.TabIndex = 2;
            this.textBoxSaldoMinimoNuevo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSaldoMinimoNuevo_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(245, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Ingrese saldo minimo de cuenta deseado";
            // 
            // AprobacionDeCuentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 432);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxSaldoMinimoNuevo);
            this.Controls.Add(this.buttonCambiarSaldoMinimo);
            this.Controls.Add(this.listBoxCuentasBanco);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxMotivo);
            this.Controls.Add(this.labelMotivo);
            this.Controls.Add(this.buttonVolver);
            this.Controls.Add(this.buttonRechazar);
            this.Controls.Add(this.buttonAprobar);
            this.Controls.Add(this.listBoxCuentasPendientes);
            this.Name = "AprobacionDeCuentas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AprobacionDeCuentas";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AprobacionDeCuentas_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxCuentasPendientes;
        private System.Windows.Forms.Button buttonAprobar;
        private System.Windows.Forms.Button buttonRechazar;
        private System.Windows.Forms.Button buttonVolver;
        private System.Windows.Forms.Label labelMotivo;
        private System.Windows.Forms.TextBox textBoxMotivo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBoxCuentasBanco;
        private System.Windows.Forms.Button buttonCambiarSaldoMinimo;
        private System.Windows.Forms.TextBox textBoxSaldoMinimoNuevo;
        private System.Windows.Forms.Label label3;
    }
}