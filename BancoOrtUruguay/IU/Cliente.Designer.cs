﻿namespace IU
{
    partial class Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxRolesUsuario = new System.Windows.Forms.ComboBox();
            this.buttonCambiarVista = new System.Windows.Forms.Button();
            this.buttonSolicitudApertura = new System.Windows.Forms.Button();
            this.buttonCerrarSesion = new System.Windows.Forms.Button();
            this.listBoxCuentas = new System.Windows.Forms.ListBox();
            this.labelCuentas = new System.Windows.Forms.Label();
            this.buttonBuzonMensajes = new System.Windows.Forms.Button();
            this.buttonTransaccion = new System.Windows.Forms.Button();
            this.buttonAgregarFavorita = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxTransacciones = new System.Windows.Forms.ListBox();
            this.buttonLimpiarFiltros = new System.Windows.Forms.Button();
            this.dateTimePickerFdesde = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerFhasta = new System.Windows.Forms.DateTimePicker();
            this.labelFdesde = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonFiltrarFecha = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonDashBoard = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBoxRolesUsuario
            // 
            this.comboBoxRolesUsuario.FormattingEnabled = true;
            this.comboBoxRolesUsuario.Location = new System.Drawing.Point(13, 32);
            this.comboBoxRolesUsuario.Name = "comboBoxRolesUsuario";
            this.comboBoxRolesUsuario.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRolesUsuario.TabIndex = 1;
            // 
            // buttonCambiarVista
            // 
            this.buttonCambiarVista.Location = new System.Drawing.Point(140, 30);
            this.buttonCambiarVista.Name = "buttonCambiarVista";
            this.buttonCambiarVista.Size = new System.Drawing.Size(95, 23);
            this.buttonCambiarVista.TabIndex = 2;
            this.buttonCambiarVista.Text = "Cambio de vista";
            this.buttonCambiarVista.UseVisualStyleBackColor = true;
            this.buttonCambiarVista.Click += new System.EventHandler(this.buttonCambiarVista_Click);
            // 
            // buttonSolicitudApertura
            // 
            this.buttonSolicitudApertura.Location = new System.Drawing.Point(13, 106);
            this.buttonSolicitudApertura.Name = "buttonSolicitudApertura";
            this.buttonSolicitudApertura.Size = new System.Drawing.Size(106, 40);
            this.buttonSolicitudApertura.TabIndex = 3;
            this.buttonSolicitudApertura.Text = "Solicitar apertura de cuenta";
            this.buttonSolicitudApertura.UseVisualStyleBackColor = true;
            this.buttonSolicitudApertura.Click += new System.EventHandler(this.buttonSolicitudApertura_Click);
            // 
            // buttonCerrarSesion
            // 
            this.buttonCerrarSesion.Location = new System.Drawing.Point(675, 30);
            this.buttonCerrarSesion.Name = "buttonCerrarSesion";
            this.buttonCerrarSesion.Size = new System.Drawing.Size(88, 21);
            this.buttonCerrarSesion.TabIndex = 14;
            this.buttonCerrarSesion.Text = "Cerrar sesion";
            this.buttonCerrarSesion.UseVisualStyleBackColor = true;
            this.buttonCerrarSesion.Click += new System.EventHandler(this.buttonCerrarSesion_Click);
            // 
            // listBoxCuentas
            // 
            this.listBoxCuentas.FormattingEnabled = true;
            this.listBoxCuentas.Location = new System.Drawing.Point(184, 106);
            this.listBoxCuentas.Name = "listBoxCuentas";
            this.listBoxCuentas.Size = new System.Drawing.Size(251, 186);
            this.listBoxCuentas.TabIndex = 8;
            // 
            // labelCuentas
            // 
            this.labelCuentas.AutoSize = true;
            this.labelCuentas.Location = new System.Drawing.Point(181, 79);
            this.labelCuentas.Name = "labelCuentas";
            this.labelCuentas.Size = new System.Drawing.Size(115, 13);
            this.labelCuentas.TabIndex = 6;
            this.labelCuentas.Text = "Sus cuentas bancarias";
            // 
            // buttonBuzonMensajes
            // 
            this.buttonBuzonMensajes.Location = new System.Drawing.Point(13, 163);
            this.buttonBuzonMensajes.Name = "buttonBuzonMensajes";
            this.buttonBuzonMensajes.Size = new System.Drawing.Size(106, 40);
            this.buttonBuzonMensajes.TabIndex = 4;
            this.buttonBuzonMensajes.Text = "Buzon de Mensajes";
            this.buttonBuzonMensajes.UseVisualStyleBackColor = true;
            this.buttonBuzonMensajes.Click += new System.EventHandler(this.buttonBuzonMensajes_Click);
            // 
            // buttonTransaccion
            // 
            this.buttonTransaccion.Location = new System.Drawing.Point(13, 223);
            this.buttonTransaccion.Name = "buttonTransaccion";
            this.buttonTransaccion.Size = new System.Drawing.Size(106, 40);
            this.buttonTransaccion.TabIndex = 5;
            this.buttonTransaccion.Text = "Realizar transaccion";
            this.buttonTransaccion.UseVisualStyleBackColor = true;
            this.buttonTransaccion.Click += new System.EventHandler(this.buttonTransaccion_Click);
            // 
            // buttonAgregarFavorita
            // 
            this.buttonAgregarFavorita.Location = new System.Drawing.Point(13, 281);
            this.buttonAgregarFavorita.Name = "buttonAgregarFavorita";
            this.buttonAgregarFavorita.Size = new System.Drawing.Size(106, 40);
            this.buttonAgregarFavorita.TabIndex = 6;
            this.buttonAgregarFavorita.Text = "Agregar favorita";
            this.buttonAgregarFavorita.UseVisualStyleBackColor = true;
            this.buttonAgregarFavorita.Click += new System.EventHandler(this.buttonAgregarFavorita_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(509, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Sus transacciones";
            // 
            // listBoxTransacciones
            // 
            this.listBoxTransacciones.FormattingEnabled = true;
            this.listBoxTransacciones.Location = new System.Drawing.Point(512, 106);
            this.listBoxTransacciones.Name = "listBoxTransacciones";
            this.listBoxTransacciones.Size = new System.Drawing.Size(251, 186);
            this.listBoxTransacciones.TabIndex = 9;
            this.listBoxTransacciones.SelectedIndexChanged += new System.EventHandler(this.listBoxTransacciones_SelectedIndexChanged);
            // 
            // buttonLimpiarFiltros
            // 
            this.buttonLimpiarFiltros.Location = new System.Drawing.Point(335, 334);
            this.buttonLimpiarFiltros.Name = "buttonLimpiarFiltros";
            this.buttonLimpiarFiltros.Size = new System.Drawing.Size(100, 51);
            this.buttonLimpiarFiltros.TabIndex = 13;
            this.buttonLimpiarFiltros.Text = "Limpiar Filtros";
            this.buttonLimpiarFiltros.UseVisualStyleBackColor = true;
            this.buttonLimpiarFiltros.Click += new System.EventHandler(this.buttonLimpiarFiltros_Click);
            // 
            // dateTimePickerFdesde
            // 
            this.dateTimePickerFdesde.Location = new System.Drawing.Point(548, 308);
            this.dateTimePickerFdesde.Name = "dateTimePickerFdesde";
            this.dateTimePickerFdesde.Size = new System.Drawing.Size(215, 20);
            this.dateTimePickerFdesde.TabIndex = 10;
            // 
            // dateTimePickerFhasta
            // 
            this.dateTimePickerFhasta.Location = new System.Drawing.Point(548, 334);
            this.dateTimePickerFhasta.Name = "dateTimePickerFhasta";
            this.dateTimePickerFhasta.Size = new System.Drawing.Size(215, 20);
            this.dateTimePickerFhasta.TabIndex = 11;
            // 
            // labelFdesde
            // 
            this.labelFdesde.AutoSize = true;
            this.labelFdesde.Location = new System.Drawing.Point(473, 314);
            this.labelFdesde.Name = "labelFdesde";
            this.labelFdesde.Size = new System.Drawing.Size(69, 13);
            this.labelFdesde.TabIndex = 16;
            this.labelFdesde.Text = "Fecha desde";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(476, 340);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Fecha hasta";
            // 
            // buttonFiltrarFecha
            // 
            this.buttonFiltrarFecha.Location = new System.Drawing.Point(655, 370);
            this.buttonFiltrarFecha.Name = "buttonFiltrarFecha";
            this.buttonFiltrarFecha.Size = new System.Drawing.Size(108, 51);
            this.buttonFiltrarFecha.TabIndex = 12;
            this.buttonFiltrarFecha.Text = "Filtrar transaccion por fecha";
            this.buttonFiltrarFecha.UseVisualStyleBackColor = true;
            this.buttonFiltrarFecha.Click += new System.EventHandler(this.buttonFiltrarFecha_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Roles asignados";
            // 
            // buttonDashBoard
            // 
            this.buttonDashBoard.Location = new System.Drawing.Point(16, 340);
            this.buttonDashBoard.Name = "buttonDashBoard";
            this.buttonDashBoard.Size = new System.Drawing.Size(103, 40);
            this.buttonDashBoard.TabIndex = 7;
            this.buttonDashBoard.Text = "DashBoard de consumo";
            this.buttonDashBoard.UseVisualStyleBackColor = true;
            this.buttonDashBoard.Click += new System.EventHandler(this.buttonDashBoard_Click);
            // 
            // Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 453);
            this.Controls.Add(this.buttonDashBoard);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonFiltrarFecha);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelFdesde);
            this.Controls.Add(this.dateTimePickerFhasta);
            this.Controls.Add(this.dateTimePickerFdesde);
            this.Controls.Add(this.buttonLimpiarFiltros);
            this.Controls.Add(this.listBoxTransacciones);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAgregarFavorita);
            this.Controls.Add(this.buttonTransaccion);
            this.Controls.Add(this.buttonBuzonMensajes);
            this.Controls.Add(this.labelCuentas);
            this.Controls.Add(this.listBoxCuentas);
            this.Controls.Add(this.buttonCerrarSesion);
            this.Controls.Add(this.buttonSolicitudApertura);
            this.Controls.Add(this.buttonCambiarVista);
            this.Controls.Add(this.comboBoxRolesUsuario);
            this.Name = "Cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cliente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxRolesUsuario;
        private System.Windows.Forms.Button buttonCambiarVista;
        private System.Windows.Forms.Button buttonSolicitudApertura;
        private System.Windows.Forms.Button buttonCerrarSesion;
        private System.Windows.Forms.ListBox listBoxCuentas;
        private System.Windows.Forms.Label labelCuentas;
        private System.Windows.Forms.Button buttonBuzonMensajes;
        private System.Windows.Forms.Button buttonTransaccion;
        private System.Windows.Forms.Button buttonAgregarFavorita;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxTransacciones;
        private System.Windows.Forms.Button buttonLimpiarFiltros;
        private System.Windows.Forms.DateTimePicker dateTimePickerFdesde;
        private System.Windows.Forms.DateTimePicker dateTimePickerFhasta;
        private System.Windows.Forms.Label labelFdesde;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonFiltrarFecha;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonDashBoard;
    }
}