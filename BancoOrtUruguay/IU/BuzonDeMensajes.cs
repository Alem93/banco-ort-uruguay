﻿using BancoOrtUruguay;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class BuzonDeMensajes : Form
    {
        private Orchestrator orchestrator;
        private Usuario usuarioLogin;

        public BuzonDeMensajes(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            usuarioLogin = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            CargarMensajes();
        }

        private void CargarMensajes()
        {
            this.listBoxMensajes.DataSource = null;
            this.listBoxMensajes.DataSource = usuarioLogin.BuzonDeMensajes.ToArray();
        }

        private void buttonEliminarMensaje_Click(object sender, EventArgs e)
        {
            Mensaje mensajeABorrar = (Mensaje)listBoxMensajes.SelectedItem;
            orchestrator.controladorUsuario.EliminarMensajeUsuario(mensajeABorrar, usuarioLogin);
            CargarMensajes();
        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            MenuCli();
        }

        private void MenuCli()
        {
            this.Hide();
            Cliente menuCli = new Cliente(orchestrator);
            menuCli.Show();
        }
    }
}
