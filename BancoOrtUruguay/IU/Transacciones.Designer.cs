﻿namespace IU
{
    partial class Transacciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxCuentaOrigen = new System.Windows.Forms.ListBox();
            this.listBoxCuentaDestino = new System.Windows.Forms.ListBox();
            this.labelCuentaOrigen = new System.Windows.Forms.Label();
            this.labelCuentaDestino = new System.Windows.Forms.Label();
            this.checkBoxTransferenciaFueraDelBanco = new System.Windows.Forms.CheckBox();
            this.buttonTransferir = new System.Windows.Forms.Button();
            this.buttonVolver = new System.Windows.Forms.Button();
            this.textBoxCantidadTransferir = new System.Windows.Forms.TextBox();
            this.labelCantidadTransferencia = new System.Windows.Forms.Label();
            this.listBoxFavoritas = new System.Windows.Forms.ListBox();
            this.labelFavoritas = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxCategoria = new System.Windows.Forms.ComboBox();
            this.comboBoxSubCat = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxNroSwift = new System.Windows.Forms.TextBox();
            this.textBoxNroCuentaDestinatario = new System.Windows.Forms.TextBox();
            this.textBoxNombreDestinatario = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // listBoxCuentaOrigen
            // 
            this.listBoxCuentaOrigen.FormattingEnabled = true;
            this.listBoxCuentaOrigen.Location = new System.Drawing.Point(12, 63);
            this.listBoxCuentaOrigen.Name = "listBoxCuentaOrigen";
            this.listBoxCuentaOrigen.Size = new System.Drawing.Size(194, 108);
            this.listBoxCuentaOrigen.TabIndex = 1;
            // 
            // listBoxCuentaDestino
            // 
            this.listBoxCuentaDestino.FormattingEnabled = true;
            this.listBoxCuentaDestino.Location = new System.Drawing.Point(332, 63);
            this.listBoxCuentaDestino.Name = "listBoxCuentaDestino";
            this.listBoxCuentaDestino.Size = new System.Drawing.Size(194, 108);
            this.listBoxCuentaDestino.TabIndex = 3;
            this.listBoxCuentaDestino.SelectedIndexChanged += new System.EventHandler(this.listBoxCuentaDestino_SelectedIndexChanged);
            // 
            // labelCuentaOrigen
            // 
            this.labelCuentaOrigen.AutoSize = true;
            this.labelCuentaOrigen.Location = new System.Drawing.Point(118, 43);
            this.labelCuentaOrigen.Name = "labelCuentaOrigen";
            this.labelCuentaOrigen.Size = new System.Drawing.Size(88, 13);
            this.labelCuentaOrigen.TabIndex = 2;
            this.labelCuentaOrigen.Text = "Cuenta de origen";
            // 
            // labelCuentaDestino
            // 
            this.labelCuentaDestino.AutoSize = true;
            this.labelCuentaDestino.Location = new System.Drawing.Point(329, 43);
            this.labelCuentaDestino.Name = "labelCuentaDestino";
            this.labelCuentaDestino.Size = new System.Drawing.Size(93, 13);
            this.labelCuentaDestino.TabIndex = 3;
            this.labelCuentaDestino.Text = "Cuenta de destino";
            // 
            // checkBoxTransferenciaFueraDelBanco
            // 
            this.checkBoxTransferenciaFueraDelBanco.AutoSize = true;
            this.checkBoxTransferenciaFueraDelBanco.Location = new System.Drawing.Point(266, 177);
            this.checkBoxTransferenciaFueraDelBanco.Name = "checkBoxTransferenciaFueraDelBanco";
            this.checkBoxTransferenciaFueraDelBanco.Size = new System.Drawing.Size(168, 17);
            this.checkBoxTransferenciaFueraDelBanco.TabIndex = 6;
            this.checkBoxTransferenciaFueraDelBanco.Text = "Transferencia fuera del banco";
            this.checkBoxTransferenciaFueraDelBanco.UseVisualStyleBackColor = true;
            this.checkBoxTransferenciaFueraDelBanco.CheckedChanged += new System.EventHandler(this.checkBoxTransferenciaFueraDelBanco_CheckedChanged);
            // 
            // buttonTransferir
            // 
            this.buttonTransferir.Location = new System.Drawing.Point(215, 131);
            this.buttonTransferir.Name = "buttonTransferir";
            this.buttonTransferir.Size = new System.Drawing.Size(100, 40);
            this.buttonTransferir.TabIndex = 5;
            this.buttonTransferir.Text = " Transferir - - >\r\n       \\|/";
            this.buttonTransferir.UseVisualStyleBackColor = true;
            this.buttonTransferir.Click += new System.EventHandler(this.buttonTransferir_Click);
            // 
            // buttonVolver
            // 
            this.buttonVolver.Location = new System.Drawing.Point(540, 374);
            this.buttonVolver.Name = "buttonVolver";
            this.buttonVolver.Size = new System.Drawing.Size(75, 29);
            this.buttonVolver.TabIndex = 12;
            this.buttonVolver.Text = "Volver";
            this.buttonVolver.UseVisualStyleBackColor = true;
            this.buttonVolver.Click += new System.EventHandler(this.buttonVolver_Click);
            // 
            // textBoxCantidadTransferir
            // 
            this.textBoxCantidadTransferir.Location = new System.Drawing.Point(215, 86);
            this.textBoxCantidadTransferir.Name = "textBoxCantidadTransferir";
            this.textBoxCantidadTransferir.Size = new System.Drawing.Size(100, 20);
            this.textBoxCantidadTransferir.TabIndex = 2;
            this.textBoxCantidadTransferir.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCantidadTransferir_KeyPress);
            // 
            // labelCantidadTransferencia
            // 
            this.labelCantidadTransferencia.AutoSize = true;
            this.labelCantidadTransferencia.Location = new System.Drawing.Point(212, 70);
            this.labelCantidadTransferencia.Name = "labelCantidadTransferencia";
            this.labelCantidadTransferencia.Size = new System.Drawing.Size(101, 13);
            this.labelCantidadTransferencia.TabIndex = 8;
            this.labelCantidadTransferencia.Text = "Cantidad a transferir";
            // 
            // listBoxFavoritas
            // 
            this.listBoxFavoritas.FormattingEnabled = true;
            this.listBoxFavoritas.Location = new System.Drawing.Point(540, 63);
            this.listBoxFavoritas.Name = "listBoxFavoritas";
            this.listBoxFavoritas.Size = new System.Drawing.Size(99, 108);
            this.listBoxFavoritas.TabIndex = 4;
            this.listBoxFavoritas.SelectedIndexChanged += new System.EventHandler(this.listBoxFavoritas_SelectedIndexChanged);
            // 
            // labelFavoritas
            // 
            this.labelFavoritas.AutoSize = true;
            this.labelFavoritas.Location = new System.Drawing.Point(537, 43);
            this.labelFavoritas.Name = "labelFavoritas";
            this.labelFavoritas.Size = new System.Drawing.Size(68, 13);
            this.labelFavoritas.TabIndex = 10;
            this.labelFavoritas.Text = "Sus favoritas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(182, 333);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Categoria";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(329, 333);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Sub-categoria";
            // 
            // comboBoxCategoria
            // 
            this.comboBoxCategoria.FormattingEnabled = true;
            this.comboBoxCategoria.Location = new System.Drawing.Point(185, 362);
            this.comboBoxCategoria.Name = "comboBoxCategoria";
            this.comboBoxCategoria.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCategoria.TabIndex = 10;
            this.comboBoxCategoria.SelectedIndexChanged += new System.EventHandler(this.comboBoxCategoria_SelectedIndexChanged);
            this.comboBoxCategoria.Click += new System.EventHandler(this.comboBoxCategoria_Click);
            // 
            // comboBoxSubCat
            // 
            this.comboBoxSubCat.FormattingEnabled = true;
            this.comboBoxSubCat.Location = new System.Drawing.Point(332, 362);
            this.comboBoxSubCat.Name = "comboBoxSubCat";
            this.comboBoxSubCat.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSubCat.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(177, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Numero de swift";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(93, 246);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Numero de cuenta de destinatario";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(140, 287);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Nombre del Destinatario";
            // 
            // textBoxNroSwift
            // 
            this.textBoxNroSwift.Location = new System.Drawing.Point(266, 205);
            this.textBoxNroSwift.Name = "textBoxNroSwift";
            this.textBoxNroSwift.Size = new System.Drawing.Size(100, 20);
            this.textBoxNroSwift.TabIndex = 7;
            this.textBoxNroSwift.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNroSwift_KeyPress);
            // 
            // textBoxNroCuentaDestinatario
            // 
            this.textBoxNroCuentaDestinatario.Location = new System.Drawing.Point(266, 243);
            this.textBoxNroCuentaDestinatario.Name = "textBoxNroCuentaDestinatario";
            this.textBoxNroCuentaDestinatario.Size = new System.Drawing.Size(100, 20);
            this.textBoxNroCuentaDestinatario.TabIndex = 8;
            this.textBoxNroCuentaDestinatario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNroCuentaDestinatario_KeyPress);
            // 
            // textBoxNombreDestinatario
            // 
            this.textBoxNombreDestinatario.Location = new System.Drawing.Point(266, 284);
            this.textBoxNombreDestinatario.Name = "textBoxNombreDestinatario";
            this.textBoxNombreDestinatario.Size = new System.Drawing.Size(100, 20);
            this.textBoxNombreDestinatario.TabIndex = 9;
            // 
            // Transacciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 485);
            this.Controls.Add(this.textBoxNombreDestinatario);
            this.Controls.Add(this.textBoxNroCuentaDestinatario);
            this.Controls.Add(this.textBoxNroSwift);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxSubCat);
            this.Controls.Add(this.comboBoxCategoria);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelFavoritas);
            this.Controls.Add(this.listBoxFavoritas);
            this.Controls.Add(this.labelCantidadTransferencia);
            this.Controls.Add(this.textBoxCantidadTransferir);
            this.Controls.Add(this.buttonVolver);
            this.Controls.Add(this.buttonTransferir);
            this.Controls.Add(this.checkBoxTransferenciaFueraDelBanco);
            this.Controls.Add(this.labelCuentaDestino);
            this.Controls.Add(this.labelCuentaOrigen);
            this.Controls.Add(this.listBoxCuentaDestino);
            this.Controls.Add(this.listBoxCuentaOrigen);
            this.Name = "Transacciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transaccion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxCuentaOrigen;
        private System.Windows.Forms.ListBox listBoxCuentaDestino;
        private System.Windows.Forms.Label labelCuentaOrigen;
        private System.Windows.Forms.Label labelCuentaDestino;
        private System.Windows.Forms.CheckBox checkBoxTransferenciaFueraDelBanco;
        private System.Windows.Forms.Button buttonTransferir;
        private System.Windows.Forms.Button buttonVolver;
        private System.Windows.Forms.TextBox textBoxCantidadTransferir;
        private System.Windows.Forms.Label labelCantidadTransferencia;
        private System.Windows.Forms.ListBox listBoxFavoritas;
        private System.Windows.Forms.Label labelFavoritas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxCategoria;
        private System.Windows.Forms.ComboBox comboBoxSubCat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxNroSwift;
        private System.Windows.Forms.TextBox textBoxNroCuentaDestinatario;
        private System.Windows.Forms.TextBox textBoxNombreDestinatario;
    }
}