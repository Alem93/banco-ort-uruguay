﻿using BancoOrtUruguay;
using Excepciones;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class AgregarFavorita : Form
    {
        private Orchestrator orchestrator;
        private Usuario usuarioLogin;

        public AgregarFavorita(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            CargarFavoritas();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            this.buttonEliminarFav.Hide();
        }


        public void CargarFavoritas()
        {
            this.listBoxFavoritos.DataSource = null;
            this.usuarioLogin = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            this.listBoxFavoritos.DataSource = usuarioLogin.Favoritas.ToArray();
        }

        private void buttonAgregarFav_Click(object sender, EventArgs e)
        {
            string alias = (string)textBoxAlias.Text;
            int nroRef;
            int.TryParse(textBoxNroRef.Text, out nroRef);
            this.usuarioLogin = orchestrator.controladorUsuario.ObtenerUsuarioLogin();

            Favorita nuevaFavorita = new Favorita()
            {
                Alias = alias,
                NumeroDeReferencia = nroRef
            };

            Cuenta cuentaPorNroRef = orchestrator.controladorCuenta.ObtenerCuentaPorNumeroDeRefencia(nroRef);
            bool existeNroRef = false;
            bool esDuenio = false;

            if (cuentaPorNroRef!=null)
            {
                existeNroRef = true;
                esDuenio = orchestrator.controladorCuenta.EsDuenioDeCuenta(cuentaPorNroRef, usuarioLogin);
            }

            try
            {
                orchestrator.controladorUsuario.AgregarFavoritaUsuario(nuevaFavorita,usuarioLogin,esDuenio,existeNroRef);
                NotificacionFavoritaRegistradaOK();
                CargarFavoritas();
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonEliminarFav_Click(object sender, EventArgs e)
        {
            this.usuarioLogin = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            Favorita favoritaBorrar = (Favorita)listBoxFavoritos.SelectedItem;
            orchestrator.controladorUsuario.EliminarFavoritaUsuario(favoritaBorrar, usuarioLogin);
            CargarFavoritas();
        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            MenuCli();
        }

        private void MenuCli()
        {
            this.Hide();
            Cliente menuCli = new Cliente(orchestrator);
            menuCli.Show();
        }

        private void textBoxNroRef_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
           if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void NotificacionFavoritaRegistradaOK()
        {
            MessageBox.Show("Se ha agregado favorita correctamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
