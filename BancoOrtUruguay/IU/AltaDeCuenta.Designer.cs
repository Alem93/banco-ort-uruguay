﻿namespace IU
{
    partial class AltaDeCuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAltaDeCuenta = new System.Windows.Forms.Button();
            this.listBoxClientes = new System.Windows.Forms.ListBox();
            this.comboBoxTipoCuenta = new System.Windows.Forms.ComboBox();
            this.textBoxBalance = new System.Windows.Forms.TextBox();
            this.textBoxDescripcion = new System.Windows.Forms.TextBox();
            this.comboBoxTipoMoneda = new System.Windows.Forms.ComboBox();
            this.labelTipoCuenta = new System.Windows.Forms.Label();
            this.labelBalance = new System.Windows.Forms.Label();
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.labelMoneda = new System.Windows.Forms.Label();
            this.buttonVolver = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAltaDeCuenta
            // 
            this.buttonAltaDeCuenta.Location = new System.Drawing.Point(299, 285);
            this.buttonAltaDeCuenta.Name = "buttonAltaDeCuenta";
            this.buttonAltaDeCuenta.Size = new System.Drawing.Size(99, 35);
            this.buttonAltaDeCuenta.TabIndex = 6;
            this.buttonAltaDeCuenta.Text = "Alta de Cuenta";
            this.buttonAltaDeCuenta.UseVisualStyleBackColor = true;
            this.buttonAltaDeCuenta.Click += new System.EventHandler(this.buttonAltaDeCuenta_Click);
            // 
            // listBoxClientes
            // 
            this.listBoxClientes.FormattingEnabled = true;
            this.listBoxClientes.Location = new System.Drawing.Point(25, 41);
            this.listBoxClientes.Name = "listBoxClientes";
            this.listBoxClientes.Size = new System.Drawing.Size(222, 264);
            this.listBoxClientes.TabIndex = 1;
            // 
            // comboBoxTipoCuenta
            // 
            this.comboBoxTipoCuenta.FormattingEnabled = true;
            this.comboBoxTipoCuenta.Location = new System.Drawing.Point(277, 68);
            this.comboBoxTipoCuenta.Name = "comboBoxTipoCuenta";
            this.comboBoxTipoCuenta.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTipoCuenta.TabIndex = 2;
            // 
            // textBoxBalance
            // 
            this.textBoxBalance.Location = new System.Drawing.Point(277, 142);
            this.textBoxBalance.Name = "textBoxBalance";
            this.textBoxBalance.Size = new System.Drawing.Size(100, 20);
            this.textBoxBalance.TabIndex = 4;
            this.textBoxBalance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxBalance_KeyPress);
            // 
            // textBoxDescripcion
            // 
            this.textBoxDescripcion.Location = new System.Drawing.Point(277, 213);
            this.textBoxDescripcion.Name = "textBoxDescripcion";
            this.textBoxDescripcion.Size = new System.Drawing.Size(100, 20);
            this.textBoxDescripcion.TabIndex = 5;
            // 
            // comboBoxTipoMoneda
            // 
            this.comboBoxTipoMoneda.FormattingEnabled = true;
            this.comboBoxTipoMoneda.Location = new System.Drawing.Point(420, 68);
            this.comboBoxTipoMoneda.Name = "comboBoxTipoMoneda";
            this.comboBoxTipoMoneda.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTipoMoneda.TabIndex = 3;
            // 
            // labelTipoCuenta
            // 
            this.labelTipoCuenta.AutoSize = true;
            this.labelTipoCuenta.Location = new System.Drawing.Point(274, 41);
            this.labelTipoCuenta.Name = "labelTipoCuenta";
            this.labelTipoCuenta.Size = new System.Drawing.Size(82, 13);
            this.labelTipoCuenta.TabIndex = 6;
            this.labelTipoCuenta.Text = "Tipo De Cuenta";
            // 
            // labelBalance
            // 
            this.labelBalance.AutoSize = true;
            this.labelBalance.Location = new System.Drawing.Point(274, 113);
            this.labelBalance.Name = "labelBalance";
            this.labelBalance.Size = new System.Drawing.Size(46, 13);
            this.labelBalance.TabIndex = 7;
            this.labelBalance.Text = "Balance";
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.AutoSize = true;
            this.labelDescripcion.Location = new System.Drawing.Point(274, 185);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(63, 13);
            this.labelDescripcion.TabIndex = 8;
            this.labelDescripcion.Text = "Descripcion";
            // 
            // labelMoneda
            // 
            this.labelMoneda.AutoSize = true;
            this.labelMoneda.Location = new System.Drawing.Point(417, 41);
            this.labelMoneda.Name = "labelMoneda";
            this.labelMoneda.Size = new System.Drawing.Size(46, 13);
            this.labelMoneda.TabIndex = 9;
            this.labelMoneda.Text = "Moneda";
            // 
            // buttonVolver
            // 
            this.buttonVolver.Location = new System.Drawing.Point(459, 285);
            this.buttonVolver.Name = "buttonVolver";
            this.buttonVolver.Size = new System.Drawing.Size(82, 35);
            this.buttonVolver.TabIndex = 10;
            this.buttonVolver.Text = "Volver";
            this.buttonVolver.UseVisualStyleBackColor = true;
            this.buttonVolver.Click += new System.EventHandler(this.buttonVolver_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Lista de clientes";
            // 
            // AltaDeCuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 373);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonVolver);
            this.Controls.Add(this.labelMoneda);
            this.Controls.Add(this.labelDescripcion);
            this.Controls.Add(this.labelBalance);
            this.Controls.Add(this.labelTipoCuenta);
            this.Controls.Add(this.comboBoxTipoMoneda);
            this.Controls.Add(this.textBoxDescripcion);
            this.Controls.Add(this.textBoxBalance);
            this.Controls.Add(this.comboBoxTipoCuenta);
            this.Controls.Add(this.listBoxClientes);
            this.Controls.Add(this.buttonAltaDeCuenta);
            this.Name = "AltaDeCuenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AltaDeCuenta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAltaDeCuenta;
        private System.Windows.Forms.ListBox listBoxClientes;
        private System.Windows.Forms.ComboBox comboBoxTipoCuenta;
        private System.Windows.Forms.TextBox textBoxBalance;
        private System.Windows.Forms.TextBox textBoxDescripcion;
        private System.Windows.Forms.ComboBox comboBoxTipoMoneda;
        private System.Windows.Forms.Label labelTipoCuenta;
        private System.Windows.Forms.Label labelBalance;
        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.Label labelMoneda;
        private System.Windows.Forms.Button buttonVolver;
        private System.Windows.Forms.Label label1;
    }
}