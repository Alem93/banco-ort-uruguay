﻿using BancoOrtUruguay;
using Excepciones;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class MantenimientoCategoriaGastos : Form
    {
        private Orchestrator orchestrator;

        public MantenimientoCategoriaGastos(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            CargarCategoriaGastos();
        }


        public void CargarCategoriaGastos()
        {
            treeViewCatGastos.Nodes.Clear();
            List<CategoriaGasto> categoriasGastos = orchestrator.controladorCategoriaGasto.ObtenerCategoriasDeGastoEnUnaLista();

            foreach (CategoriaGasto catGasto in categoriasGastos)
            {
                TreeNode NodoPadre = new TreeNode();
                NodoPadre.Text = catGasto.Categoria;
                NodoPadre.Name = catGasto.Id.ToString();
                NodoPadre.ForeColor = Color.Black;
                NodoPadre.BackColor = Color.White;
                NodoPadre.ImageIndex = 0;
                NodoPadre.SelectedImageIndex = 0;
                treeViewCatGastos.Nodes.Add(NodoPadre);

                List<CategoriaGasto> SubCategoriasGastos = catGasto.SubCategorias;
                foreach (CategoriaGasto subCatGasto in SubCategoriasGastos)
                {

                    TreeNode NodoHijo = new TreeNode();
                    NodoHijo.Text = subCatGasto.Categoria;
                    NodoHijo.Name = subCatGasto.Id.ToString();
                    NodoHijo.ForeColor = Color.Black;
                    NodoHijo.BackColor = Color.White;
                    NodoHijo.ImageIndex = 0;
                    NodoHijo.SelectedImageIndex = 0;
                    NodoPadre.Nodes.Add(NodoHijo);
                }
            }
            
        }

        private void buttonAgregarCategoria_Click(object sender, EventArgs e)
        {
            string nombreCategoria = textBoxNuevaCategoria.Text;

            CategoriaGasto nuevaCategoriaGasto = new CategoriaGasto()
            {
                Categoria = nombreCategoria,
                EsPadre=true
            };

            try
            {
                orchestrator.controladorCategoriaGasto.AltaDeCategoriaGasto(nuevaCategoriaGasto);
                CargarCategoriaGastos();
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
 
            CategoriaGasto recuperarCat = new CategoriaGasto();

          //  orchestrator.controladorCategoriaGasto.RemoverCategoriaGasto(recuperarCat);

            if (treeViewCatGastos.SelectedNode != null && treeViewCatGastos.SelectedNode.SelectedImageIndex != -1)
            {
                recuperarCat.Categoria = treeViewCatGastos.SelectedNode.Text;
               // string nombreSubCategoria = treeViewCatGastos.n;
               // orchestrator.controladorCategoriaGasto.RemoverSubCategoriaGasto(recuperarCat, nombreSubCategoria);
            }
        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            MenuAdmin();
        }

        private void MenuAdmin()
        {
            this.Hide();
            BackOffice menuAdmin = new BackOffice(orchestrator);
            menuAdmin.Show();
        }

        private void buttonNuevaSubCat_Click(object sender, EventArgs e)
        {
            string nombreSubCategoria = textBoxNuevaSubCat.Text;
            CategoriaGasto recuperarCat = new CategoriaGasto();

            CategoriaGasto nuevaSubCategoriaGasto = new CategoriaGasto()
            {
                Categoria = nombreSubCategoria
            };

            if (treeViewCatGastos.SelectedNode != null && treeViewCatGastos.SelectedNode.SelectedImageIndex!=-1) {
                recuperarCat.Id = int.Parse(treeViewCatGastos.SelectedNode.Name);
                recuperarCat.Categoria = treeViewCatGastos.SelectedNode.Text;
            }

            try
            {
                orchestrator.controladorCategoriaGasto.AltaSubCategoriaGasto(recuperarCat, nuevaSubCategoriaGasto);
                CargarCategoriaGastos();
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
