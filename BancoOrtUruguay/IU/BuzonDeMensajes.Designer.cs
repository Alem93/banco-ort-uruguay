﻿namespace IU
{
    partial class BuzonDeMensajes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonVolver = new System.Windows.Forms.Button();
            this.buttonEliminarMensaje = new System.Windows.Forms.Button();
            this.listBoxMensajes = new System.Windows.Forms.ListBox();
            this.labelBuzonMensajes = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonVolver
            // 
            this.buttonVolver.Location = new System.Drawing.Point(377, 280);
            this.buttonVolver.Name = "buttonVolver";
            this.buttonVolver.Size = new System.Drawing.Size(87, 39);
            this.buttonVolver.TabIndex = 3;
            this.buttonVolver.Text = "Volver";
            this.buttonVolver.UseVisualStyleBackColor = true;
            this.buttonVolver.Click += new System.EventHandler(this.buttonVolver_Click);
            // 
            // buttonEliminarMensaje
            // 
            this.buttonEliminarMensaje.Location = new System.Drawing.Point(377, 137);
            this.buttonEliminarMensaje.Name = "buttonEliminarMensaje";
            this.buttonEliminarMensaje.Size = new System.Drawing.Size(87, 39);
            this.buttonEliminarMensaje.TabIndex = 2;
            this.buttonEliminarMensaje.Text = "Eliminar mensaje";
            this.buttonEliminarMensaje.UseVisualStyleBackColor = true;
            this.buttonEliminarMensaje.Click += new System.EventHandler(this.buttonEliminarMensaje_Click);
            // 
            // listBoxMensajes
            // 
            this.listBoxMensajes.FormattingEnabled = true;
            this.listBoxMensajes.Location = new System.Drawing.Point(52, 76);
            this.listBoxMensajes.Name = "listBoxMensajes";
            this.listBoxMensajes.Size = new System.Drawing.Size(304, 173);
            this.listBoxMensajes.TabIndex = 1;
            // 
            // labelBuzonMensajes
            // 
            this.labelBuzonMensajes.AutoSize = true;
            this.labelBuzonMensajes.Location = new System.Drawing.Point(49, 51);
            this.labelBuzonMensajes.Name = "labelBuzonMensajes";
            this.labelBuzonMensajes.Size = new System.Drawing.Size(99, 13);
            this.labelBuzonMensajes.TabIndex = 3;
            this.labelBuzonMensajes.Text = "Casilla de mensajes";
            // 
            // BuzonDeMensajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 361);
            this.Controls.Add(this.labelBuzonMensajes);
            this.Controls.Add(this.listBoxMensajes);
            this.Controls.Add(this.buttonEliminarMensaje);
            this.Controls.Add(this.buttonVolver);
            this.Name = "BuzonDeMensajes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BuzonDeMensajes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonVolver;
        private System.Windows.Forms.Button buttonEliminarMensaje;
        private System.Windows.Forms.ListBox listBoxMensajes;
        private System.Windows.Forms.Label labelBuzonMensajes;
    }
}