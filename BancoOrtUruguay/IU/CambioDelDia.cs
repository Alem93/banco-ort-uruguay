﻿using BancoOrtUruguay;
using Excepciones;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class CambioDelDia : Form
    {
        private Orchestrator orchestrator;

        public CambioDelDia(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            CargarTipoMonedas();
        }


        private void CargarTipoMonedas()
        {
            this.comboBoxMonedaOrigen.DataSource = orchestrator.controladorMoneda.ObtenerMonedasEnUnArray();
            this.comboBoxMonedaDestino.DataSource = orchestrator.controladorMoneda.ObtenerMonedasEnUnArray();
        }

        private void buttonAltaTipoCambio_Click(object sender, EventArgs e)
        {
            Moneda monedaOrigen = (Moneda)comboBoxMonedaOrigen.SelectedItem;
            Moneda monedaDestino = (Moneda)comboBoxMonedaDestino.SelectedItem;
            int unaCantidad;
            int.TryParse(textBoxCantidad.Text, out unaCantidad);
            TipoCambio nuevoTipoCambio = new TipoCambio()
            {
                Origen = monedaOrigen,
                Destino = monedaDestino,
                Compra = unaCantidad
            };
            try
            {
                orchestrator.controladorTipoCambio.AltaTipoDeCambio(nuevoTipoCambio);
                NotificacionTipoCambioRegistradaOK();
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception er)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            MenuAdmin();
        }

        private void MenuAdmin()
        {
            this.Hide();
            BackOffice menuAdmin = new BackOffice(orchestrator);
            menuAdmin.Show();
        }

        private void textBoxCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
           if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void NotificacionTipoCambioRegistradaOK()
        {
            MessageBox.Show("Se ha agregado tipo de cambio correctamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
