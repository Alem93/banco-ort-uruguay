﻿namespace IU
{
    partial class MantenimientoCategoriaGastos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeViewCatGastos = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNuevaCategoria = new System.Windows.Forms.TextBox();
            this.buttonAgregarCategoria = new System.Windows.Forms.Button();
            this.buttonVolver = new System.Windows.Forms.Button();
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonNuevaSubCat = new System.Windows.Forms.Button();
            this.textBoxNuevaSubCat = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // treeViewCatGastos
            // 
            this.treeViewCatGastos.Location = new System.Drawing.Point(48, 12);
            this.treeViewCatGastos.Name = "treeViewCatGastos";
            this.treeViewCatGastos.Size = new System.Drawing.Size(296, 167);
            this.treeViewCatGastos.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 218);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nueva categoria";
            // 
            // textBoxNuevaCategoria
            // 
            this.textBoxNuevaCategoria.Location = new System.Drawing.Point(48, 247);
            this.textBoxNuevaCategoria.Name = "textBoxNuevaCategoria";
            this.textBoxNuevaCategoria.Size = new System.Drawing.Size(100, 20);
            this.textBoxNuevaCategoria.TabIndex = 3;
            // 
            // buttonAgregarCategoria
            // 
            this.buttonAgregarCategoria.Location = new System.Drawing.Point(48, 273);
            this.buttonAgregarCategoria.Name = "buttonAgregarCategoria";
            this.buttonAgregarCategoria.Size = new System.Drawing.Size(83, 55);
            this.buttonAgregarCategoria.TabIndex = 4;
            this.buttonAgregarCategoria.Text = "Agregar categoria";
            this.buttonAgregarCategoria.UseVisualStyleBackColor = true;
            this.buttonAgregarCategoria.Click += new System.EventHandler(this.buttonAgregarCategoria_Click);
            // 
            // buttonVolver
            // 
            this.buttonVolver.Location = new System.Drawing.Point(378, 305);
            this.buttonVolver.Name = "buttonVolver";
            this.buttonVolver.Size = new System.Drawing.Size(75, 23);
            this.buttonVolver.TabIndex = 7;
            this.buttonVolver.Text = "Volver";
            this.buttonVolver.UseVisualStyleBackColor = true;
            this.buttonVolver.Click += new System.EventHandler(this.buttonVolver_Click);
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.Location = new System.Drawing.Point(350, 85);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(75, 23);
            this.buttonEliminar.TabIndex = 2;
            this.buttonEliminar.Text = "Eliminar";
            this.buttonEliminar.UseVisualStyleBackColor = true;
            this.buttonEliminar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(217, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Seleccione una Categoria";
            // 
            // buttonNuevaSubCat
            // 
            this.buttonNuevaSubCat.Location = new System.Drawing.Point(220, 273);
            this.buttonNuevaSubCat.Name = "buttonNuevaSubCat";
            this.buttonNuevaSubCat.Size = new System.Drawing.Size(83, 55);
            this.buttonNuevaSubCat.TabIndex = 6;
            this.buttonNuevaSubCat.Text = "Agregar sub-Categoria";
            this.buttonNuevaSubCat.UseVisualStyleBackColor = true;
            this.buttonNuevaSubCat.Click += new System.EventHandler(this.buttonNuevaSubCat_Click);
            // 
            // textBoxNuevaSubCat
            // 
            this.textBoxNuevaSubCat.Location = new System.Drawing.Point(220, 247);
            this.textBoxNuevaSubCat.Name = "textBoxNuevaSubCat";
            this.textBoxNuevaSubCat.Size = new System.Drawing.Size(100, 20);
            this.textBoxNuevaSubCat.TabIndex = 5;
            // 
            // MantenimientoCategoriaGastos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 377);
            this.Controls.Add(this.textBoxNuevaSubCat);
            this.Controls.Add(this.buttonNuevaSubCat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonEliminar);
            this.Controls.Add(this.buttonVolver);
            this.Controls.Add(this.buttonAgregarCategoria);
            this.Controls.Add(this.textBoxNuevaCategoria);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.treeViewCatGastos);
            this.Name = "MantenimientoCategoriaGastos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MantenimientoCategoriaGastos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeViewCatGastos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNuevaCategoria;
        private System.Windows.Forms.Button buttonAgregarCategoria;
        private System.Windows.Forms.Button buttonVolver;
        private System.Windows.Forms.Button buttonEliminar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonNuevaSubCat;
        private System.Windows.Forms.TextBox textBoxNuevaSubCat;
    }
}