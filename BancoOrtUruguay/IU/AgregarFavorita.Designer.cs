﻿namespace IU
{
    partial class AgregarFavorita
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAgregarFav = new System.Windows.Forms.Button();
            this.buttonEliminarFav = new System.Windows.Forms.Button();
            this.listBoxFavoritos = new System.Windows.Forms.ListBox();
            this.labelNroRef = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelFavoritos = new System.Windows.Forms.Label();
            this.buttonVolver = new System.Windows.Forms.Button();
            this.textBoxNroRef = new System.Windows.Forms.TextBox();
            this.textBoxAlias = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAgregarFav
            // 
            this.buttonAgregarFav.Location = new System.Drawing.Point(101, 138);
            this.buttonAgregarFav.Name = "buttonAgregarFav";
            this.buttonAgregarFav.Size = new System.Drawing.Size(87, 40);
            this.buttonAgregarFav.TabIndex = 3;
            this.buttonAgregarFav.Text = "Agregar favorito";
            this.buttonAgregarFav.UseVisualStyleBackColor = true;
            this.buttonAgregarFav.Click += new System.EventHandler(this.buttonAgregarFav_Click);
            // 
            // buttonEliminarFav
            // 
            this.buttonEliminarFav.Location = new System.Drawing.Point(396, 101);
            this.buttonEliminarFav.Name = "buttonEliminarFav";
            this.buttonEliminarFav.Size = new System.Drawing.Size(87, 40);
            this.buttonEliminarFav.TabIndex = 5;
            this.buttonEliminarFav.Text = "Eliminar favorito";
            this.buttonEliminarFav.UseVisualStyleBackColor = true;
            this.buttonEliminarFav.Click += new System.EventHandler(this.buttonEliminarFav_Click);
            // 
            // listBoxFavoritos
            // 
            this.listBoxFavoritos.FormattingEnabled = true;
            this.listBoxFavoritos.Location = new System.Drawing.Point(270, 74);
            this.listBoxFavoritos.Name = "listBoxFavoritos";
            this.listBoxFavoritos.Size = new System.Drawing.Size(120, 95);
            this.listBoxFavoritos.TabIndex = 4;
            // 
            // labelNroRef
            // 
            this.labelNroRef.AutoSize = true;
            this.labelNroRef.Location = new System.Drawing.Point(32, 77);
            this.labelNroRef.Name = "labelNroRef";
            this.labelNroRef.Size = new System.Drawing.Size(50, 13);
            this.labelNroRef.TabIndex = 3;
            this.labelNroRef.Text = "Nro. Ref.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Alias";
            // 
            // labelFavoritos
            // 
            this.labelFavoritos.AutoSize = true;
            this.labelFavoritos.Location = new System.Drawing.Point(267, 58);
            this.labelFavoritos.Name = "labelFavoritos";
            this.labelFavoritos.Size = new System.Drawing.Size(68, 13);
            this.labelFavoritos.TabIndex = 5;
            this.labelFavoritos.Text = "Sus favoritos";
            // 
            // buttonVolver
            // 
            this.buttonVolver.Location = new System.Drawing.Point(303, 195);
            this.buttonVolver.Name = "buttonVolver";
            this.buttonVolver.Size = new System.Drawing.Size(87, 29);
            this.buttonVolver.TabIndex = 6;
            this.buttonVolver.Text = "Volver";
            this.buttonVolver.UseVisualStyleBackColor = true;
            this.buttonVolver.Click += new System.EventHandler(this.buttonVolver_Click);
            // 
            // textBoxNroRef
            // 
            this.textBoxNroRef.Location = new System.Drawing.Point(88, 74);
            this.textBoxNroRef.Name = "textBoxNroRef";
            this.textBoxNroRef.Size = new System.Drawing.Size(100, 20);
            this.textBoxNroRef.TabIndex = 1;
            this.textBoxNroRef.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNroRef_KeyPress);
            // 
            // textBoxAlias
            // 
            this.textBoxAlias.Location = new System.Drawing.Point(88, 112);
            this.textBoxAlias.Name = "textBoxAlias";
            this.textBoxAlias.Size = new System.Drawing.Size(100, 20);
            this.textBoxAlias.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Registre una cuenta como favorito";
            // 
            // AgregarFavorita
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 294);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxAlias);
            this.Controls.Add(this.textBoxNroRef);
            this.Controls.Add(this.buttonVolver);
            this.Controls.Add(this.labelFavoritos);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelNroRef);
            this.Controls.Add(this.listBoxFavoritos);
            this.Controls.Add(this.buttonEliminarFav);
            this.Controls.Add(this.buttonAgregarFav);
            this.Name = "AgregarFavorita";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AgregarFavorita";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAgregarFav;
        private System.Windows.Forms.Button buttonEliminarFav;
        private System.Windows.Forms.ListBox listBoxFavoritos;
        private System.Windows.Forms.Label labelNroRef;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelFavoritos;
        private System.Windows.Forms.Button buttonVolver;
        private System.Windows.Forms.TextBox textBoxNroRef;
        private System.Windows.Forms.TextBox textBoxAlias;
        private System.Windows.Forms.Label label1;
    }
}