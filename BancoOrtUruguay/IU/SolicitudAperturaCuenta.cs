﻿using BancoOrtUruguay;
using Excepciones;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class SolicitudAperturaCuenta : Form
    {
        private Orchestrator orchestrator;

        public SolicitudAperturaCuenta(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            CargarTipoMonedas();
            CargarTipoCuentas();
        }


        private void CargarTipoMonedas()
        {
            this.comboBoxTipoMoneda.DataSource = orchestrator.controladorMoneda.ObtenerMonedasEnUnArray();
        }

        private void CargarTipoCuentas()
        {
            this.comboBoxTipoCuenta.DataSource = Enum.GetValues(typeof(TIPO_DE_CUENTA));
        }

        private void CerrarVentana()
        {
            this.Hide();
        }

        private void buttonVolver_Click(object sender, EventArgs e)
        {
            MenuCli();
        }

        private void MenuCli()
        {
            this.Hide();
            Cliente menuCli = new Cliente(orchestrator);
            menuCli.Show();
        }

        private void textBoxBalance_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
           if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void buttonSolicitarCuenta_Click(object sender, EventArgs e)
        {
            Usuario unCliente = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            TIPO_DE_CUENTA unTipoCuenta = (TIPO_DE_CUENTA)comboBoxTipoCuenta.SelectedItem;
            string unaDescripcion = textBoxDescripcion.Text;
            int unBalance = 0;
            int.TryParse(textBoxBalance.Text, out unBalance);
            Moneda unaMoneda = (Moneda)comboBoxTipoMoneda.SelectedItem;


            Cuenta nuevaCuenta = new Cuenta()
            {
                Balance = unBalance,
                Descripcion = unaDescripcion,
                TipoDeCuenta = unTipoCuenta,
                UnaMoneda = unaMoneda,
                UnCliente = unCliente
            };

            try
            {
                this.orchestrator.controladorCuenta.SolicitudAperturaCuentaBancaria(nuevaCuenta);
                NotificacionCuentaSoliciatadaOK();
                MenuCli();
            }
            catch (ExcepcionGeneral ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void NotificacionCuentaSoliciatadaOK()
        {
            MessageBox.Show("Se ha solicitado una cuenta correctamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
