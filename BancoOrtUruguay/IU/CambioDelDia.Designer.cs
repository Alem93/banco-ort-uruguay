﻿namespace IU
{
    partial class CambioDelDia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAltaTipoCambio = new System.Windows.Forms.Button();
            this.buttonVolver = new System.Windows.Forms.Button();
            this.comboBoxMonedaOrigen = new System.Windows.Forms.ComboBox();
            this.labelMonedaOrigen = new System.Windows.Forms.Label();
            this.labelCantidad = new System.Windows.Forms.Label();
            this.textBoxCantidad = new System.Windows.Forms.TextBox();
            this.labelMonedaDestino = new System.Windows.Forms.Label();
            this.comboBoxMonedaDestino = new System.Windows.Forms.ComboBox();
            this.labelAclaracion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAltaTipoCambio
            // 
            this.buttonAltaTipoCambio.Location = new System.Drawing.Point(144, 195);
            this.buttonAltaTipoCambio.Name = "buttonAltaTipoCambio";
            this.buttonAltaTipoCambio.Size = new System.Drawing.Size(100, 39);
            this.buttonAltaTipoCambio.TabIndex = 4;
            this.buttonAltaTipoCambio.Text = "Registrar cambio";
            this.buttonAltaTipoCambio.UseVisualStyleBackColor = true;
            this.buttonAltaTipoCambio.Click += new System.EventHandler(this.buttonAltaTipoCambio_Click);
            // 
            // buttonVolver
            // 
            this.buttonVolver.Location = new System.Drawing.Point(266, 261);
            this.buttonVolver.Name = "buttonVolver";
            this.buttonVolver.Size = new System.Drawing.Size(75, 32);
            this.buttonVolver.TabIndex = 5;
            this.buttonVolver.Text = "Volver";
            this.buttonVolver.UseVisualStyleBackColor = true;
            this.buttonVolver.Click += new System.EventHandler(this.buttonVolver_Click);
            // 
            // comboBoxMonedaOrigen
            // 
            this.comboBoxMonedaOrigen.FormattingEnabled = true;
            this.comboBoxMonedaOrigen.Location = new System.Drawing.Point(43, 93);
            this.comboBoxMonedaOrigen.Name = "comboBoxMonedaOrigen";
            this.comboBoxMonedaOrigen.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMonedaOrigen.TabIndex = 1;
            // 
            // labelMonedaOrigen
            // 
            this.labelMonedaOrigen.AutoSize = true;
            this.labelMonedaOrigen.Location = new System.Drawing.Point(40, 73);
            this.labelMonedaOrigen.Name = "labelMonedaOrigen";
            this.labelMonedaOrigen.Size = new System.Drawing.Size(78, 13);
            this.labelMonedaOrigen.TabIndex = 3;
            this.labelMonedaOrigen.Text = "Moneda origen";
            // 
            // labelCantidad
            // 
            this.labelCantidad.AutoSize = true;
            this.labelCantidad.Location = new System.Drawing.Point(167, 130);
            this.labelCantidad.Name = "labelCantidad";
            this.labelCantidad.Size = new System.Drawing.Size(52, 13);
            this.labelCantidad.TabIndex = 4;
            this.labelCantidad.Text = "Cantidad ";
            // 
            // textBoxCantidad
            // 
            this.textBoxCantidad.Location = new System.Drawing.Point(144, 146);
            this.textBoxCantidad.Name = "textBoxCantidad";
            this.textBoxCantidad.Size = new System.Drawing.Size(100, 20);
            this.textBoxCantidad.TabIndex = 3;
            this.textBoxCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCantidad_KeyPress);
            // 
            // labelMonedaDestino
            // 
            this.labelMonedaDestino.AutoSize = true;
            this.labelMonedaDestino.Location = new System.Drawing.Point(217, 73);
            this.labelMonedaDestino.Name = "labelMonedaDestino";
            this.labelMonedaDestino.Size = new System.Drawing.Size(83, 13);
            this.labelMonedaDestino.TabIndex = 6;
            this.labelMonedaDestino.Text = "Moneda destino";
            // 
            // comboBoxMonedaDestino
            // 
            this.comboBoxMonedaDestino.FormattingEnabled = true;
            this.comboBoxMonedaDestino.Location = new System.Drawing.Point(220, 93);
            this.comboBoxMonedaDestino.Name = "comboBoxMonedaDestino";
            this.comboBoxMonedaDestino.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMonedaDestino.TabIndex = 2;
            // 
            // labelAclaracion
            // 
            this.labelAclaracion.AutoSize = true;
            this.labelAclaracion.Location = new System.Drawing.Point(69, 37);
            this.labelAclaracion.Name = "labelAclaracion";
            this.labelAclaracion.Size = new System.Drawing.Size(231, 13);
            this.labelAclaracion.TabIndex = 7;
            this.labelAclaracion.Text = "Cantidad moneda origen --> 1X moneda destino";
            // 
            // CambioDelDia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 341);
            this.Controls.Add(this.labelAclaracion);
            this.Controls.Add(this.comboBoxMonedaDestino);
            this.Controls.Add(this.labelMonedaDestino);
            this.Controls.Add(this.textBoxCantidad);
            this.Controls.Add(this.labelCantidad);
            this.Controls.Add(this.labelMonedaOrigen);
            this.Controls.Add(this.comboBoxMonedaOrigen);
            this.Controls.Add(this.buttonVolver);
            this.Controls.Add(this.buttonAltaTipoCambio);
            this.Name = "CambioDelDia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CambioDelDia";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAltaTipoCambio;
        private System.Windows.Forms.Button buttonVolver;
        private System.Windows.Forms.ComboBox comboBoxMonedaOrigen;
        private System.Windows.Forms.Label labelMonedaOrigen;
        private System.Windows.Forms.Label labelCantidad;
        private System.Windows.Forms.TextBox textBoxCantidad;
        private System.Windows.Forms.Label labelMonedaDestino;
        private System.Windows.Forms.ComboBox comboBoxMonedaDestino;
        private System.Windows.Forms.Label labelAclaracion;
    }
}