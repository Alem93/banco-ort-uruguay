﻿using BancoOrtUruguay;
using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IU
{
    public partial class Cliente : Form
    {
        private Orchestrator orchestrator;

        public Cliente(Orchestrator unOrchestrator)
        {
            orchestrator = unOrchestrator;
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            CargarRolesUsuario();
            CargarCuentasUsuario();
            CargarTransacciones();
        }

        private void CargarRolesUsuario()
        {
            Usuario usuarioLogueado = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            this.comboBoxRolesUsuario.DataSource = usuarioLogueado.RolesDeUsuario.ToArray();
        }

        private void CargarTransacciones()
        {
            Usuario usuarioLogueado = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            this.listBoxTransacciones.DataSource = null;

            List<Transaccion> transaccionesRecuperadas = orchestrator.controladorTransaccion.ObtenerTransaccionesEnUnaLista();
            this.listBoxTransacciones.DataSource = transaccionesRecuperadas.FindAll(
                transaccionesDeUsuario => transaccionesDeUsuario.CuentaOrigen.UnCliente.Equals(usuarioLogueado)||
                transaccionesDeUsuario.CuentaDestino.UnCliente.Equals(usuarioLogueado)
                ).ToArray();
        }

        private void CargarCuentasUsuario()
        {
            Usuario usuarioLogueado = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            this.listBoxCuentas.DataSource = orchestrator.controladorCuenta.ObtenerCuentasDeUsuarioEnUnArray(usuarioLogueado);
        }

        private void buttonCambiarVista_Click(object sender, EventArgs e)
        {
            string rolUsuario = comboBoxRolesUsuario.Text;
            string rolCli = "ADMINISTRADOR";

            if (rolCli == rolUsuario)
            {             
                BackOffice backOffice = new BackOffice(this.orchestrator);
                CerrarVentana();
                backOffice.Show();
            }
        }

        private void CerrarVentana()
        {
            this.Close();
        }

        private void buttonCerrarSesion_Click(object sender, EventArgs e)
        {
            this.orchestrator.controladorUsuario.DesLoguear();   
            Login login = new Login(this.orchestrator);
            CerrarVentana();
            login.Show();
        }

        private void buttonSolicitudApertura_Click(object sender, EventArgs e)
        {
            CerrarVentana();
            SolicitudAperturaCuenta solicitudAperturaCuenta = new SolicitudAperturaCuenta(this.orchestrator);
            solicitudAperturaCuenta.Show();
        }

        private void buttonBuzonMensajes_Click(object sender, EventArgs e)
        {
            BuzonDeMensajes buzonDeMensajes = new BuzonDeMensajes(this.orchestrator);
            CerrarVentana();
            buzonDeMensajes.Show();      
        }

        private void buttonTransaccion_Click(object sender, EventArgs e)
        {
            Transacciones transaccion = new Transacciones(this.orchestrator);
            CerrarVentana();
            transaccion.Show();

        }

        private void buttonAgregarFavorita_Click(object sender, EventArgs e)
        {
            AgregarFavorita agregarFavorita = new AgregarFavorita(this.orchestrator);
            CerrarVentana();
            agregarFavorita.Show();
        }

        private void buttonLimpiarFiltros_Click(object sender, EventArgs e)
        {
            CargarTransacciones();
        }

        private void buttonFiltrarFecha_Click(object sender, EventArgs e)
        {
            DateTime fechaDesde= dateTimePickerFdesde.Value;
            DateTime fechaHasta = dateTimePickerFhasta.Value;
            Usuario usuarioLogueado = orchestrator.controladorUsuario.ObtenerUsuarioLogin();
            this.listBoxTransacciones.DataSource = null;

            List<Transaccion> transaccionesRecuperadas = orchestrator.controladorTransaccion.ObtenerTransaccionesEnUnaLista();
            List<Transaccion> transaccionesPropietario = transaccionesRecuperadas.FindAll(
                transaccionesDeUsuario => transaccionesDeUsuario.CuentaOrigen.UnCliente.Equals(usuarioLogueado) ||
                transaccionesDeUsuario.CuentaDestino.UnCliente.Equals(usuarioLogueado)
                );

            this.listBoxTransacciones.DataSource=transaccionesPropietario.FindAll(
                transaccionesDeUsuario => transaccionesDeUsuario.Fecha >= fechaDesde &&
                transaccionesDeUsuario.Fecha <= fechaHasta
                ).ToArray();
        }

        private void listBoxTransacciones_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonDashBoard_Click(object sender, EventArgs e)
        {          
            DashBoardConsumo dashBoard = new DashBoardConsumo(this.orchestrator);
            dashBoard.Show();
        }
    }
}
