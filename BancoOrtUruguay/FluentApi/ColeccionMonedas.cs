﻿using BancoOrtUruguay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApi
{
    public class ColeccionMonedas
    {

        public static void AgregarMoneda(Moneda unaMoneda)
        {
            using (var contexto = new DataContext())
            {
                contexto.Monedas.Add(unaMoneda);
                contexto.SaveChanges();
            }
        }

        public static int ObtenerNumeroDeMonedasRegistradas()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Monedas.ToList().Count;

                return query;
            }
        }

        public static bool ExisteMoneda(Moneda unaMoneda)
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Monedas.Where(e => e.Id == unaMoneda.Id);

                return query.ToList().Count > 0;
            }
        }

        public static Moneda[] ObtenerColeccionEnUnArray()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Monedas.ToList();

                return query.ToArray();
            }
        }
    }
}
