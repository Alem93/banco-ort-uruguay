﻿using BancoOrtUruguay;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApi
{
    public class ColeccionCategoriaGastos
    {
        public static void AgregarCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {
            using (var contexto = new DataContext())
            {
                contexto.CategoriasGasto.Add(unaCategoriaGasto);
                contexto.SaveChanges();
            }
        }

        public static void RemoverCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {
            using (var contexto = new DataContext())
            {
                contexto.CategoriasGasto.Attach(unaCategoriaGasto);
                contexto.CategoriasGasto.Remove(unaCategoriaGasto);
                contexto.SaveChanges();
            }
        }


        public static void AgregarSubCategoriaGasto(CategoriaGasto unaCategoriaGasto, CategoriaGasto subcategoria)
        {

            using (var contexto = new DataContext())
            {
                contexto.CategoriasGasto.Attach(unaCategoriaGasto);
                unaCategoriaGasto.SubCategorias.Add(subcategoria);
                contexto.SaveChanges();
            }
        }

        public static void RemoverSubCategoriaGasto(CategoriaGasto unaCategoriaGasto, CategoriaGasto subcategoria)
        {
            using (var contexto = new DataContext())
            {
                contexto.CategoriasGasto.Attach(unaCategoriaGasto);
                unaCategoriaGasto.SubCategorias.Remove(subcategoria);
                contexto.SaveChanges();
            }
        }

        public static bool ExisteCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {

            using (var contexto = new DataContext())
            {
                var query = contexto.CategoriasGasto.Where(e => e.Categoria == unaCategoriaGasto.Categoria);

                return query.ToList().Count > 0;
            }
        }

        public static int ObtenerNumeroDeCategoriasRegistradas()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.CategoriasGasto.ToList().Count;

                return query;
            }
        }

        public static List<CategoriaGasto> ObtenerColeccionDeCategoriasDeGastoEnUnaLista()
        {
            using (var contexto = new DataContext())
            {
                bool EsPadre = true;
                var query = (contexto.CategoriasGasto.Where(e => e.EsPadre == EsPadre)).Include(e => e.SubCategorias);

                return query.ToList();
            }
        }
    }
}
