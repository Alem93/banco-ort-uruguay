﻿using BancoOrtUruguay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApi
{
    public class ColeccionTransacciones
    {

        public static void AgregarTransaccion(Transaccion unaTransaccion)
        {
            using (var contexto = new DataContext())
            {
                contexto.Transacciones.Add(unaTransaccion);
                contexto.SaveChanges();
            }
        }

        public static void AgregarTransaccionInternacional(Transaccion unaTransaccion)
        {
            using (var contexto = new DataContext())
            {
                contexto.Transacciones.Add(unaTransaccion);
                contexto.SaveChanges();
            }
        }

        public static bool ExisteTransaccion(Transaccion transaccionBuscada)
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Transacciones.Where(e => e.Id == transaccionBuscada.Id);

                return query.ToList().Count > 0;
            }
        }

        public static int ObtenerNumeroDeTransaccionesRegistradas()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Transacciones.ToList().Count;

                return query;
            }
        }

        public static List<Transaccion> ObtenerColeccionTransaccionesEnUnaLista()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Transacciones.ToList();

                return query;
            }
        }

        public static void DebitarCuentaOrigen(Transaccion unaTransaccion)
        {
            using (var contexto = new DataContext())
            {
                contexto.Cuentas.Attach(unaTransaccion.CuentaOrigen);
                unaTransaccion.CuentaOrigen.Balance = unaTransaccion.CuentaOrigen.Balance - unaTransaccion.CantidadDebitada;
                if (BalanceMenorAlSaldoMinimo(unaTransaccion))
                {
                    unaTransaccion.CuentaOrigen.EstadoCuenta = ESTADO_SOLICITUD_CUENTA.SUSPENDIDA;
                }
                contexto.SaveChanges();
            }
        }

        private static bool BalanceMenorAlSaldoMinimo(Transaccion unaTransaccion)
        {
            double saldoMinimo = unaTransaccion.CuentaOrigen.MontoMinimo;
            double balanceDeCuenta = unaTransaccion.CuentaOrigen.Balance;

            return balanceDeCuenta < saldoMinimo;
        }

        public static void AcreditarCuentaDestino(Transaccion unaTransaccion)
        {
            using (var contexto = new DataContext())
            {
                contexto.Cuentas.Attach(unaTransaccion.CuentaDestino);
                unaTransaccion.CuentaDestino.Balance = unaTransaccion.CuentaDestino.Balance + unaTransaccion.CantidadDebitada;
                contexto.SaveChanges();
            }       
        }
    }
}
