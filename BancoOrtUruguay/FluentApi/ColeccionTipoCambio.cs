﻿using BancoOrtUruguay;
using Excepciones;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApi
{
    public class ColeccionTipoCambio
    {
        public static void AgregarTipoDeCambio(TipoCambio unTipoDeCambio)
        {
            DateTime fecha = Convert.ToDateTime(DateTime.Now).Date;
            unTipoDeCambio.Fecha = fecha;

                using (var contexto = new DataContext())
                {
                    contexto.Monedas.Attach(unTipoDeCambio.Origen);
                    contexto.Monedas.Attach(unTipoDeCambio.Destino);
                    contexto.TiposDeCambio.Add(unTipoDeCambio);
                    contexto.SaveChanges();
                }
            
        }

        public static TipoCambio ObtenerTipoDeCambioMasReciente(Moneda unaMonedaOrigen, Moneda unaMonedaDestino)
        {
            using (var contexto = new DataContext())
            {
                TipoCambio tipo = null;
                bool existe = contexto.TiposDeCambio.Any(tipoCambio =>
                            (tipoCambio.Origen.Id == unaMonedaOrigen.Id
                              && tipoCambio.Destino.Id == unaMonedaDestino.Id)
                              || (tipoCambio.Origen.Id == unaMonedaDestino.Id
                                && tipoCambio.Destino.Id == unaMonedaOrigen.Id)
                            );

                if (existe)
                {

                    var query = ((from tipoCambio in contexto.TiposDeCambio
                                  where tipoCambio.Origen.Id == unaMonedaOrigen.Id
                                  && tipoCambio.Destino.Id == unaMonedaDestino.Id ||
                                  tipoCambio.Origen.Id == unaMonedaDestino.Id
                                  && tipoCambio.Destino.Id == unaMonedaOrigen.Id
                                  orderby tipoCambio.Fecha descending
                                  select tipoCambio).Include(e=>e.Origen).
                                  Include(e=>e.Destino)).FirstOrDefault();

                    tipo = (TipoCambio)query;

                    DateTime fecha = Convert.ToDateTime(tipo.Fecha).Date;

                    tipo.Fecha = fecha;
                }

                return tipo;
            }
        }

        public static int ObtenerNumeroDeTipoCambioRegistrados()
        {
                using (var contexto = new DataContext())
                {
                    var query = contexto.TiposDeCambio.ToList().Count;

                    return query;
                }

        }

        public static bool ExisteTipoCambio(TipoCambio tipoCambioBuscado)
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.TiposDeCambio.Any(e=>e.Id==tipoCambioBuscado.Id);

                return query;
            }
        }
    }
}
