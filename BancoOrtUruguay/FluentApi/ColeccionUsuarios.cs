﻿using BancoOrtUruguay;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApi
{
    public class ColeccionUsuarios
    {
        public static void AgregarUsuario(Usuario unUsuario)
        {
            using (var contexto = new DataContext())
            {
                contexto.Usuarios.Add(unUsuario);
                contexto.SaveChanges();
            }
        }

        public static Usuario ObtenerUsuarioLogueado(int cedula, string contrasenia)
        {
                using (var contexto = new DataContext())
                {
                    Usuario UsuarioLogin = null;

                    foreach (Usuario usu in (contexto.Usuarios.
                                 Include(e => e.BuzonDeMensajes).
                                 Include(e => e.Favoritas).
                                 Include(e => e.RolesDeUsuario)))
                    {
                        if (usu.Cedula == cedula && usu.Contrasenia == contrasenia)
                        {
                            UsuarioLogin = usu;
                        }
                    }

                    return UsuarioLogin;
                }
        }

        public static int ObtenerNumeroDeUsuariosRegistrados()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Usuarios.ToList().Count;

                return query;
            }
        }

        public static bool ExisteUsuario(Usuario usuarioBuscado)
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Usuarios.Any(e => e.Id == usuarioBuscado.Id && e.Cedula==usuarioBuscado.Cedula);

                return query;
            }
        }

        public static Usuario[] ObtenerColeccionEnUnArray()
        {
            using (var contexto = new DataContext())
            {
                List<Usuario> usuariosEncontrados = new List<Usuario>();

                foreach (Usuario usu in (contexto.Usuarios.
                    Include(e => e.RolesDeUsuario).
                    Include(e => e.Favoritas).
                    Include(e => e.BuzonDeMensajes)))
                {
                    foreach (RolUsuario rol in usu.RolesDeUsuario)
                    {
                        if (rol.NombreRol == TIPO_DE_USUARIO.CLIENTE)
                        {
                            usuariosEncontrados.Add(usu);
                        }
                    }
                }

                return usuariosEncontrados.ToArray();
            }
        }

        public static List<Usuario> ObtenerColeccionUsuariosEnUnaLista()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Usuarios.ToList();

                return query;
            }
        }

        public static void AgregarFavorita(Usuario usuario, Favorita favorita)
        {
            using (var contexto = new DataContext())
            {
                contexto.Usuarios.Find(usuario.Id).Favoritas.Add(favorita);
                contexto.SaveChanges();
            }
        }

        public static void EliminarFavorita(Usuario usuario, Favorita unaFavorita)
        {
            using (var contexto = new DataContext())
            {
                contexto.Favoritas.Attach(unaFavorita);
                contexto.Favoritas.Remove(unaFavorita);
                contexto.SaveChanges();
            }
        }

        public static void EnviarMensaje(Mensaje mensaje, Usuario usuario)
        {
            using (var contexto = new DataContext())
            {
                contexto.Usuarios.Find(usuario.Id).BuzonDeMensajes.Add(mensaje);
                contexto.SaveChanges();
            }
        }

        public static void EliminarMensaje(Mensaje mensaje, Usuario usuario)
        {
            using (var contexto = new DataContext())
            {
                contexto.Mensajes.Attach(mensaje);
                contexto.Mensajes.Remove(mensaje);
                contexto.SaveChanges();
            }
        }

        public static bool ExisteFavorito(Usuario usuario, Favorita favorito)
        {
            bool retorno = false;

            using (var contexto = new DataContext())
            {

                foreach (Usuario usu in (contexto.Usuarios.Include(e => e.Favoritas)))
                {
                    if (usu.Favoritas.Contains(favorito))
                    {
                        retorno = true;
                    }
                }

            }

            return retorno;
        }
    }
}

