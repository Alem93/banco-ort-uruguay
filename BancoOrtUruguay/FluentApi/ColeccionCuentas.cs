﻿using BancoOrtUruguay;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApi
{
    public class ColeccionCuentas
    {
        public static void AgregarCuenta(Cuenta unaCuenta)
        {
            using (var contexto = new DataContext())
            {
                contexto.Usuarios.Attach(unaCuenta.UnCliente);
                contexto.Monedas.Attach(unaCuenta.UnaMoneda);
                contexto.Cuentas.Add(unaCuenta);
                contexto.SaveChanges();
            }
        }

        public static void RemoverCuenta(Cuenta unaCuenta)
        {
            using (var contexto = new DataContext())
            {
                contexto.Cuentas.Attach(unaCuenta);
                contexto.Cuentas.Remove(unaCuenta);
                contexto.SaveChanges();
            }
        }

        public static bool ExisteCuenta(Cuenta unaCuenta)
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Cuentas.Where(e => e.Id == unaCuenta.Id);

                return query.ToList().Count > 0;
            }
        }

        public static int ObtenerUltimoNumeroReferencia()
        {
            using (var contexto = new DataContext())
            {
                var query = (from cuenta in contexto.Cuentas
                              orderby cuenta.NumeroDeReferencia descending
                              select cuenta.NumeroDeReferencia).First();

                return query;
            }
        }

        public static int ObtenerNumeroDeCuentasRegistradas()
        {
            using (var contexto = new DataContext())
            {
                return contexto.Cuentas.ToList().Count;
            }
        }

        public static Cuenta[] ObtenerColeccionDeUsuarioEnUnArray(Usuario unUsuario)
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Cuentas.Where(e => e.UnCliente.Id == unUsuario.Id);

                return query.ToArray();
            }
        }

        public static Cuenta[] ObtenerColeccionActivaDeUsuarioEnUnArray(Usuario unUsuario)
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Cuentas.Where(e => e.UnCliente.Id == unUsuario.Id && e.EstadoCuenta==ESTADO_SOLICITUD_CUENTA.ACEPTADA);

                return query.ToArray();
            }
        }

        public static Cuenta[] ObtenerColeccionSegunEstadoEnUnArray(ESTADO_SOLICITUD_CUENTA estadoCuenta)
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Cuentas.Where(e => e.EstadoCuenta == estadoCuenta).Include(e=>e.UnCliente).
                    Include(e=>e.UnaMoneda);

                return query.ToArray();
            }
        }

        //public static Cuenta ObtenerCuentaPorNumeroDeRefencia(int nroRef)
        //{
        //    //Cuenta nroRefCuentas = this.Cuentas.Find(unaCuenta => unaCuenta.NumeroDeReferencia.Equals(nroRef));
        //    //return nroRefCuentas;
        //    return null;
        //}

        public static List<Cuenta> ObtenerColeccionCuentasEnUnaLista()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.Cuentas.ToList();

                return query;
            }
        }
    }
}
