﻿using BancoOrtUruguay;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApi
{
    public class DataContext:DbContext
    {
        

        public DbSet<CategoriaGasto> CategoriasGasto { get; set; }

        public DbSet<Cuenta> Cuentas { get; set; }

        public DbSet<Favorita> Favoritas { get; set; }

        public DbSet<Mensaje> Mensajes { get; set; }

        public DbSet<Moneda> Monedas { get; set; }

        public DbSet<TipoCambio> TiposDeCambio { get; set; }

        public DbSet<Transaccion> Transacciones { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<RolUsuario> RolesDeUsarios { get; set; }
    }
}
