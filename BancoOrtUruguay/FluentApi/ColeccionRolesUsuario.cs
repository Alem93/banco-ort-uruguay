﻿using BancoOrtUruguay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApi
{
    public class ColeccionRolesUsuario
    {
        public static void AgregarRol(RolUsuario unRol)
        {
            using (var contexto = new DataContext())
            {
                contexto.RolesDeUsarios.Add(unRol);
                contexto.SaveChanges();
            }
        }

        public static int ObtenerNumeroDeRolesRegistrados()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.RolesDeUsarios.ToList().Count;

                return query;
            }
        }

        public static bool ExisteRol(RolUsuario unRol)
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.RolesDeUsarios.Where(e => e.Id == unRol.Id);

                return query.ToList().Count > 0;
            }
        }

        public static RolUsuario[] ObtenerColeccionEnUnArray()
        {
            using (var contexto = new DataContext())
            {
                var query = contexto.RolesDeUsarios.ToList();

                return query.ToArray();
            }
        }
    }
}
