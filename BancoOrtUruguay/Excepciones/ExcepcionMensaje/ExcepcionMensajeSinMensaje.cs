﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionMensajeSinMensaje : ExcepcionGeneral
    {
        public ExcepcionMensajeSinMensaje() : base("ERROR: No ingreso ningun mensaje")
        {
        }

        public ExcepcionMensajeSinMensaje(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionMensajeSinMensaje(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
