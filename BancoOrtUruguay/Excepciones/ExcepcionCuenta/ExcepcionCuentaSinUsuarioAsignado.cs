﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionCuentaSinUsuarioAsignado : ExcepcionGeneral
    {
        public ExcepcionCuentaSinUsuarioAsignado() : base("ERROR: La cuenta no tiene usuario asignado")
        {
        }

        public ExcepcionCuentaSinUsuarioAsignado(string message) : base(message)
        {
        }

        public ExcepcionCuentaSinUsuarioAsignado(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
