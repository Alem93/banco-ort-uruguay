﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionCuentaSinTipoDeCuenta: ExcepcionGeneral
    {
        public ExcepcionCuentaSinTipoDeCuenta() : base("ERROR: La cuenta no tiene un tipo asignado")
        {
        }

        public ExcepcionCuentaSinTipoDeCuenta(string message) : base(message)
        {
        }

        public ExcepcionCuentaSinTipoDeCuenta(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
