﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionCuentaNoExisteNroRef : ExcepcionGeneral
    {
        public ExcepcionCuentaNoExisteNroRef() : base("ERROR: No existe una cuenta con ese NroRef")
        {
        }

        public ExcepcionCuentaNoExisteNroRef(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionCuentaNoExisteNroRef(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
