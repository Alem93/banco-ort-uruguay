﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionCuentaUsuarioSinPermiso: ExcepcionGeneral
    {
        public ExcepcionCuentaUsuarioSinPermiso() : base("ERROR: El usuario no tiene permiso para realizar la accion")
        {
        }

        public ExcepcionCuentaUsuarioSinPermiso(string message) : base(message)
        {
        }

        public ExcepcionCuentaUsuarioSinPermiso(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
