﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionCuentaSinMoneda : ExcepcionGeneral
    {
        public ExcepcionCuentaSinMoneda(): base("ERROR: La cuenta no tiene una moneda asignada")
        {
        }

        public ExcepcionCuentaSinMoneda(string message) : base(message)
        {
        }

        public ExcepcionCuentaSinMoneda(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
