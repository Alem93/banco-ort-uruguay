﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionUsuarioExiste : ExcepcionGeneral
    {
        public ExcepcionUsuarioExiste() : base("ERROR: El usuario ya se encuentra registrado en el sistema")
        {
        }

        public ExcepcionUsuarioExiste(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionUsuarioExiste(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
