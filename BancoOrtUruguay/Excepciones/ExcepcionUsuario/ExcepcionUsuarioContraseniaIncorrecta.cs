﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionUsuarioContraseniaIncorrecta : ExcepcionGeneral
    {
        public ExcepcionUsuarioContraseniaIncorrecta() : base("ERROR: Contrasenia incorrecta")
        {
        }
        public ExcepcionUsuarioContraseniaIncorrecta(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionUsuarioContraseniaIncorrecta(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
