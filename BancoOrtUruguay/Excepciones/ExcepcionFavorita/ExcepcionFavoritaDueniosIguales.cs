﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionFavoritaDueniosIguales : ExcepcionGeneral
    {
        public ExcepcionFavoritaDueniosIguales() : base("ERROR: No puede auto-asignarse una cuenta propia como favorito")
        {
        }

        public ExcepcionFavoritaDueniosIguales(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionFavoritaDueniosIguales(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
