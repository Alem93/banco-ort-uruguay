﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionFavoritaYaExiste : ExcepcionGeneral
    {
        public ExcepcionFavoritaYaExiste() : base("ERROR: Favorita ya existe")
        {
        }
        public ExcepcionFavoritaYaExiste(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionFavoritaYaExiste(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
