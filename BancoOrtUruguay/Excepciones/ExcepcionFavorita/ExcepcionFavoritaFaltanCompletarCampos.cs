﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionFavoritaFaltanCompletarCampos : ExcepcionGeneral
    {
        public ExcepcionFavoritaFaltanCompletarCampos() : base("ERROR: Campos sin completar")
        {
        }
        public ExcepcionFavoritaFaltanCompletarCampos(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionFavoritaFaltanCompletarCampos(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
