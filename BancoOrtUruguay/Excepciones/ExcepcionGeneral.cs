﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionGeneral : Exception
    {
        public ExcepcionGeneral()
        {
        }

        public ExcepcionGeneral(string message) : base(message)
        {
        }

        public ExcepcionGeneral(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionGeneral(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
