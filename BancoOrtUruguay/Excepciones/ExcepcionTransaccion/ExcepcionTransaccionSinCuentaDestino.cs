﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTransaccionSinCuentaDestino : ExcepcionGeneral
    {
        public ExcepcionTransaccionSinCuentaDestino() : base("ERROR: Seleccione una cuenta de destino")
        {
        }
        public ExcepcionTransaccionSinCuentaDestino(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionTransaccionSinCuentaDestino(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
