﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTransaccionSinCantidadATransferir : ExcepcionGeneral
    {
        public ExcepcionTransaccionSinCantidadATransferir() : base("ERROR: Ingrese una cantidad a transferir")
        {
        }
        public ExcepcionTransaccionSinCantidadATransferir(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionTransaccionSinCantidadATransferir(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
