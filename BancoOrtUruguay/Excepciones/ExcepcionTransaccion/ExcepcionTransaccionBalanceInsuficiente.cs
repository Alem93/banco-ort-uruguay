﻿using Excepciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTransaccionBalanceInsuficiente : ExcepcionGeneral
    {
        public ExcepcionTransaccionBalanceInsuficiente() : base("ERROR: Balance de cuenta de origen insuficiente")
        {
        }
        public ExcepcionTransaccionBalanceInsuficiente(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionTransaccionBalanceInsuficiente(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
