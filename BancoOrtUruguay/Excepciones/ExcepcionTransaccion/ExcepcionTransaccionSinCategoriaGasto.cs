﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTransaccionSinCategoriaGasto : ExcepcionGeneral
    {
        public ExcepcionTransaccionSinCategoriaGasto() : base("ERROR: Debe seleccionar una categoria de gasto")
        {
        }

        public ExcepcionTransaccionSinCategoriaGasto(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionTransaccionSinCategoriaGasto(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
