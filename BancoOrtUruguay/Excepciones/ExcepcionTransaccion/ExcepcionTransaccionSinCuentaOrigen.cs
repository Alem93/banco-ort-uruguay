﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTransaccionSinCuentaOrigen : ExcepcionGeneral
    {
        public ExcepcionTransaccionSinCuentaOrigen() : base("ERROR: Seleccione una cuenta de origen")
        {
        }
        public ExcepcionTransaccionSinCuentaOrigen(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionTransaccionSinCuentaOrigen(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
