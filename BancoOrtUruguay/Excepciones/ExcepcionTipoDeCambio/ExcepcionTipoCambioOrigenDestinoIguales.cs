﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTipoCambioOrigenDestinoIguales : ExcepcionGeneral
    {
        public ExcepcionTipoCambioOrigenDestinoIguales() : base("ERROR: Moneda origen y destino iguales")
        {
        }

        public ExcepcionTipoCambioOrigenDestinoIguales(string message) : base(message)
        {
        }

        public ExcepcionTipoCambioOrigenDestinoIguales(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
