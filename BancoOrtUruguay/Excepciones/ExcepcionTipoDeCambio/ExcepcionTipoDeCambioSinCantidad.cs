﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTipoDeCambioSinCantidad : ExcepcionGeneral
    {
        public ExcepcionTipoDeCambioSinCantidad() : base("ERROR: No se ingreso una cantidad")
        {
        }

        public ExcepcionTipoDeCambioSinCantidad(string message) : base(message)
        {
        }

        public ExcepcionTipoDeCambioSinCantidad(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
