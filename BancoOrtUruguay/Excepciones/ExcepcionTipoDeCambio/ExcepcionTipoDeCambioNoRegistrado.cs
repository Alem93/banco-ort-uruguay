﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTipoDeCambioNoRegistrado: ExcepcionGeneral
    {
        public ExcepcionTipoDeCambioNoRegistrado() : base("ERROR: Tipo de cambio no registrado")
        {
        }

        public ExcepcionTipoDeCambioNoRegistrado(string message) : base(message)
        {
        }

        public ExcepcionTipoDeCambioNoRegistrado(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
