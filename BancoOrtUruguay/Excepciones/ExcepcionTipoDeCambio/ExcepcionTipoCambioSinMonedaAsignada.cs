﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTipoCambioSinMonedaAsignada : ExcepcionGeneral
    {
        public ExcepcionTipoCambioSinMonedaAsignada() : base("ERROR: No se ingreso una moneda de orgen/destino")
        {
        }

        public ExcepcionTipoCambioSinMonedaAsignada(string message) : base(message)
        {
        }

        public ExcepcionTipoCambioSinMonedaAsignada(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
