﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionLoginUsuarioInvalido : ExcepcionGeneral
    {
        public ExcepcionLoginUsuarioInvalido() : base("ERROR: Usuario invalido")
        {
        }

        public ExcepcionLoginUsuarioInvalido(string message) : base(message)
        {
        }

        public ExcepcionLoginUsuarioInvalido(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
