﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionLoginSinUsuario : ExcepcionGeneral
    {
        public ExcepcionLoginSinUsuario() : base("ERROR: No hay usuario logueado en el sistema")
        {
        }

        public ExcepcionLoginSinUsuario(string message) : base(message)
        {
        }

        public ExcepcionLoginSinUsuario(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
