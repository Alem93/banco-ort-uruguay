﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionCategoriaGastoSinCategoria : ExcepcionGeneral
    {
        public ExcepcionCategoriaGastoSinCategoria() : base("ERROR: Debe seleccionar una categoria")
        {
        }
        public ExcepcionCategoriaGastoSinCategoria(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionCategoriaGastoSinCategoria(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
