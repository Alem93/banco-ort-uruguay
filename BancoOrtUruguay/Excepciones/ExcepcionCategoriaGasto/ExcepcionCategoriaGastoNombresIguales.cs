﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionCategoriaGastoNombresIguales : ExcepcionGeneral
    {
        public ExcepcionCategoriaGastoNombresIguales() : base("ERROR: Ya existe una categoria con ese nombre")
        {
        }
        public ExcepcionCategoriaGastoNombresIguales(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionCategoriaGastoNombresIguales(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
