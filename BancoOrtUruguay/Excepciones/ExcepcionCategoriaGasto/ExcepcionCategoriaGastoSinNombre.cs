﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionCategoriaGastoSinNombre : ExcepcionGeneral
    {
        public ExcepcionCategoriaGastoSinNombre() : base("ERROR: Ingrese nombre de categoria")
        {
        }
        public ExcepcionCategoriaGastoSinNombre(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionCategoriaGastoSinNombre(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
