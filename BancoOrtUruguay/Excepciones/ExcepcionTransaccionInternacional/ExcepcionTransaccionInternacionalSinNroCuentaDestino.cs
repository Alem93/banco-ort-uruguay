﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTransaccionInternacionalSinNroCuentaDestino : ExcepcionGeneral
    {
        public ExcepcionTransaccionInternacionalSinNroCuentaDestino() : base("ERROR: Ingrese un numero de cuenta destino valido")
        {
        }
        public ExcepcionTransaccionInternacionalSinNroCuentaDestino(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionTransaccionInternacionalSinNroCuentaDestino(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
