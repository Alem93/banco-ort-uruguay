﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTransaccionInternacionalNroSwiftInvalido : ExcepcionGeneral
    {
        public ExcepcionTransaccionInternacionalNroSwiftInvalido() : base("ERROR: Ingrese un numero swift valido")
        {
        }
        public ExcepcionTransaccionInternacionalNroSwiftInvalido(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionTransaccionInternacionalNroSwiftInvalido(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
