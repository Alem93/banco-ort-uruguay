﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionTransaccionInternacionalSinNombreDestinatario : ExcepcionGeneral
    {
        public ExcepcionTransaccionInternacionalSinNombreDestinatario() : base("ERROR: Ingrese un nombre de destinatario valido")
        {
        }
        public ExcepcionTransaccionInternacionalSinNombreDestinatario(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExcepcionTransaccionInternacionalSinNombreDestinatario(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
