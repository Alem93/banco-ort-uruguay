﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionMonedaExiste : ExcepcionGeneral
    {
        public ExcepcionMonedaExiste() : base("ERROR: La moneda ya se ingreso en el sistema")
        {
        }

        public ExcepcionMonedaExiste(string message) : base(message)
        {
        }

        public ExcepcionMonedaExiste(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
