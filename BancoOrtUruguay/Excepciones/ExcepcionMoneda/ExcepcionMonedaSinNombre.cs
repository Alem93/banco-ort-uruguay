﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionMonedaSinNombre: ExcepcionGeneral
    {
        public ExcepcionMonedaSinNombre() : base("ERROR: La moneda no tiene nombre")
        {
        }

        public ExcepcionMonedaSinNombre(string message) : base(message)
        {
        }

        public ExcepcionMonedaSinNombre(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
