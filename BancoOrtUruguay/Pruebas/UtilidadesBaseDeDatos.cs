﻿using BancoOrtUruguay;
using EntityFramework.Extensions;
using FluentApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Pruebas
{
    public class UtilidadesBaseDeDatos
    {
        public static void VaciarTablas()
        {
            using (var contexto = new DataContext())
            {
                var queryTC = (from tipoCambio in contexto.TiposDeCambio
                               select tipoCambio);
                queryTC.Delete();

                var queryTR = (from transaccion in contexto.Transacciones
                               select transaccion);
                queryTR.Delete();

                var queryCtas = (from cuentas in contexto.Cuentas
                                 select cuentas);
                queryCtas.Delete();

                var queryM = (from monedas in contexto.Monedas
                              select monedas);
                queryM.Delete();

                var queryCG = (from categoriaGastos in contexto.CategoriasGasto
                               select categoriaGastos);
                queryCG.Delete();

                var queryR = (from rol in contexto.RolesDeUsarios
                              select rol);
                queryR.Delete();

                var queryFAV = (from fav in contexto.Favoritas
                              select fav);
                queryFAV.Delete();

                var queryMSJ = (from msj in contexto.Mensajes
                                select msj);
                queryMSJ.Delete();

                var queryUS = (from usuario in contexto.Usuarios
                               select usuario);
                queryUS.Delete();

                contexto.SaveChanges();
            }
        }

        public static void CargarRolSegunTipoDeUsuario(RolUsuario UnRol, TIPO_DE_USUARIO UnTipoDeUsuario)
        {
            UnRol.NombreRol = TIPO_DE_USUARIO.CLIENTE;
            ColeccionRolesUsuario.AgregarRol(UnRol);
        }
        public static void AltaDeUsurioConRol(Usuario UnUsuario, RolUsuario UnRol)
        {
            UnUsuario.Nombre = "Jose";
            UnUsuario.Cedula = 4567;
            UnUsuario.Contrasenia = "contr24";
            UnUsuario.Email = "def@";
            UnUsuario.RolesDeUsuario.Add(UnRol);
            ColeccionUsuarios.AgregarUsuario(UnUsuario);
        }

        public static void AltaDeUsurioSinRol(Usuario UnUsuario)
        {
            UnUsuario.Nombre = "Jose";
            UnUsuario.Cedula = 4567;
            UnUsuario.Contrasenia = "contr24";
            UnUsuario.Email = "def@";
            ColeccionUsuarios.AgregarUsuario(UnUsuario);
        }

        public static void AltaDeMonedaSegunTipoDeMoneda(Moneda UnaMoneda, TIPO_DE_MONEDA UnTipoDeMoneda)
        {
            UnaMoneda.Nombre = UnTipoDeMoneda;
            ColeccionMonedas.AgregarMoneda(UnaMoneda);
        }

        public static void AltaDeTipoDeCambio(TipoCambio UnTipoDeCambio, Moneda UnaMonedaOrigen, Moneda UnaMonedaDestino)
        {
            UnTipoDeCambio.Origen = UnaMonedaOrigen;
            UnTipoDeCambio.Destino = UnaMonedaDestino;
            UnTipoDeCambio.Compra = 25;
            UnTipoDeCambio.Fecha = DateTime.Now;
        }

        public static void AltaDeCuenta(Cuenta UnaCuenta, Moneda UnaMoneda, Usuario UnUsuario)
        {
            UnaCuenta.UnaMoneda = UnaMoneda;
            UnaCuenta.UnCliente = UnUsuario;
            UnaCuenta.Balance = 500;
            UnaCuenta.Descripcion = "Esta es una cuenta de prueba";
            UnaCuenta.TipoDeCuenta = TIPO_DE_CUENTA.CAJA_DE_AHORRO;
            ColeccionCuentas.AgregarCuenta(UnaCuenta);

        }

        public static bool ExisteMensaje(Usuario usuario, Mensaje mensaje)
        {
            bool retorno = false;

            using (var contexto = new DataContext())
            {

                foreach (Usuario usu in (contexto.Usuarios.Include(e=>e.BuzonDeMensajes)))
                {
                    if (usu.BuzonDeMensajes.Contains(mensaje))
                    {
                        retorno = true;
                    }
                }

            }

            return retorno;
        }

        public static bool ExisteFavorito(Usuario usuario, Favorita favorito)
        {
            bool retorno = false;

            using (var contexto = new DataContext())
            {

                foreach (Usuario usu in (contexto.Usuarios.Include(e => e.Favoritas)))
                {
                    if (usu.Favoritas.Contains(favorito))
                    {
                        retorno = true;
                    }
                }

            }

            return retorno;
        }

        public static void ModificarFechaTipoCambio(TipoCambio tipoCambio, DateTime nuevaFecha)
        {
            using (var contexto=new DataContext())
            {
                contexto.TiposDeCambio.Attach(tipoCambio);
                tipoCambio.Fecha = nuevaFecha;
                contexto.SaveChanges();
            }
        }

        public static void AltaDeCategoriaDeGasto(CategoriaGasto categoria)
        {
            categoria.Categoria = "Limpieza-escobas";
            ColeccionCategoriaGastos.AgregarCategoriaGasto(categoria);
        }
    }
}