﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;
using FluentApi;

namespace Pruebas
{
    [TestClass]
    public class PruebaColeccionTransaccionesBD
    {
        private Moneda UnaMonedaDePrueba;
        private Usuario UnUsuarioDePrueba;
        private Cuenta UnaCuentaDePrueba;

        private Transaccion TransaccionDePrueba;
        private Transaccion TransaccionInternacionalDePrueba;

        [TestInitialize]
        public void CargarDatos()
        {
            UnaMonedaDePrueba = new Moneda();
            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaDePrueba,TIPO_DE_MONEDA.UYU);

            UnUsuarioDePrueba = new Usuario();
            UtilidadesBaseDeDatos.AltaDeUsurioSinRol(UnUsuarioDePrueba);

            UnaCuentaDePrueba = new Cuenta();
            UtilidadesBaseDeDatos.AltaDeCuenta(UnaCuentaDePrueba,UnaMonedaDePrueba,UnUsuarioDePrueba);

            this.TransaccionDePrueba = new Transaccion()
            {
                CuentaOrigen=UnaCuentaDePrueba,
                CuentaDestino=UnaCuentaDePrueba
            };

            this.TransaccionInternacionalDePrueba = new TransaccionInternacional()
            {
                CuentaOrigen = UnaCuentaDePrueba,
                CuentaDestino = null
            };
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AgregarTransaccion()
        {
            int tamanioColeccionTransaccion = ColeccionTransacciones.ObtenerNumeroDeTransaccionesRegistradas();
            ColeccionTransacciones.AgregarTransaccion(TransaccionDePrueba);
            int resultadoTamanioColeccionUsuarios = ColeccionTransacciones.ObtenerNumeroDeTransaccionesRegistradas();

            Assert.AreEqual(resultadoTamanioColeccionUsuarios, tamanioColeccionTransaccion + 1);
        }

        [TestMethod]
        public void AgregarTransaccionInternacioanl()
        {
            int tamanioColeccionTransaccion = ColeccionTransacciones.ObtenerNumeroDeTransaccionesRegistradas();
            ColeccionTransacciones.AgregarTransaccionInternacional(TransaccionInternacionalDePrueba);
            int resultadoTamanioColeccionUsuarios = ColeccionTransacciones.ObtenerNumeroDeTransaccionesRegistradas();

            Assert.AreEqual(resultadoTamanioColeccionUsuarios, tamanioColeccionTransaccion + 1);
        }

        [TestMethod]
        public void ObtenerNumeroDeTransaccionesRegistradas()
        {
            ColeccionTransacciones.AgregarTransaccion(TransaccionDePrueba);
            ColeccionTransacciones.AgregarTransaccion(TransaccionDePrueba);
            int numeroTransacciones = 2;
            int resultadoNumeroTransacciones = ColeccionTransacciones.ObtenerNumeroDeTransaccionesRegistradas();

            Assert.AreEqual(resultadoNumeroTransacciones, numeroTransacciones);
        }

        [TestMethod]
        public void ExisteTransaccion()
        {
            ColeccionTransacciones.AgregarTransaccion(TransaccionDePrueba);
            Assert.IsTrue(ColeccionTransacciones.ExisteTransaccion(TransaccionDePrueba));
        }

        [TestMethod]
        public void ObtenerColeccionTransaccionesEnUnaLista()
        {
            ColeccionTransacciones.AgregarTransaccion(TransaccionDePrueba);
            ColeccionTransacciones.AgregarTransaccion(TransaccionDePrueba);
            int numeroTransacciones = 2;
            int resultadoNumeroTransacciones = ColeccionTransacciones.ObtenerColeccionTransaccionesEnUnaLista().Count;

            Assert.AreEqual(resultadoNumeroTransacciones, numeroTransacciones);
        }
    }
}
