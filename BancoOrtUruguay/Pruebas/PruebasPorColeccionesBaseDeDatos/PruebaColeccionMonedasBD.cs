﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentApi;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaColeccionMonedasBD
    {
        private Moneda MonedaDePrueba;

        [TestInitialize]
        public void CargarDatos()
        {
            MonedaDePrueba = new Moneda();

        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AgregarMoneda()
        {
            int numeroMonedasRegistradas = ColeccionMonedas.ObtenerNumeroDeMonedasRegistradas();
            ColeccionMonedas.AgregarMoneda(MonedaDePrueba);
            int resultadoNumeroDeMonedasRegistradas = ColeccionMonedas.ObtenerNumeroDeMonedasRegistradas();

            Assert.AreEqual(resultadoNumeroDeMonedasRegistradas, numeroMonedasRegistradas + 1);
        }

        [TestMethod]
        public void ObtenerNumeroDeMonedasRegistradas()
        {
            ColeccionMonedas.AgregarMoneda(MonedaDePrueba);
            ColeccionMonedas.AgregarMoneda(MonedaDePrueba);
            int numeroDeMonedas = 2;
            int resultadoNumeroDeMonedas = ColeccionMonedas.ObtenerNumeroDeMonedasRegistradas();

            Assert.AreEqual(resultadoNumeroDeMonedas, numeroDeMonedas);
        }

        [TestMethod]
        public void ExisteMoneda()
        {
            ColeccionMonedas.AgregarMoneda(MonedaDePrueba);
            Assert.IsTrue(ColeccionMonedas.ExisteMoneda(MonedaDePrueba));
        }

        [TestMethod]
        public void ObtenerColeccionEnUnArray()
        {
            ColeccionMonedas.AgregarMoneda(MonedaDePrueba);
            int MonedasTamanioInicial = ColeccionMonedas.ObtenerNumeroDeMonedasRegistradas();

            Moneda[] monedasArray = ColeccionMonedas.ObtenerColeccionEnUnArray();
            int MonedasTamanioFinal = monedasArray.Length;

            Assert.AreEqual(MonedasTamanioInicial, MonedasTamanioFinal);
        }
    }
}
