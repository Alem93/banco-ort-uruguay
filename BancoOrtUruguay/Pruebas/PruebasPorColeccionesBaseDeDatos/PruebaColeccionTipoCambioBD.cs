﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;
using FluentApi;
using System.Collections.Generic;

namespace Pruebas
{
    [TestClass]
    public class PruebaColeccionTipoCambioBD
    {
        private Moneda UnaMonedaUSD;
        private Moneda UnaMonedaUYU;
        private TipoCambio TipoCambioDePrueba;

        [TestInitialize]
        public void CargarDatos()
        {
            UnaMonedaUSD = new Moneda();
            UnaMonedaUYU = new Moneda();

            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaUSD, TIPO_DE_MONEDA.USD);
            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaUYU, TIPO_DE_MONEDA.UYU);

            TipoCambioDePrueba = new TipoCambio()
            {
                Origen = UnaMonedaUSD,
                Destino = UnaMonedaUYU,
                Compra = 25,
                Fecha = DateTime.Now
            };
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AgregarTipoDeCambio()
        {
            int numeroTipoCambio = ColeccionTipoCambio.ObtenerNumeroDeTipoCambioRegistrados();
            ColeccionTipoCambio.AgregarTipoDeCambio(TipoCambioDePrueba);
            int resultadoNumeroTipoCambio = ColeccionTipoCambio.ObtenerNumeroDeTipoCambioRegistrados();

            Assert.AreEqual(resultadoNumeroTipoCambio, 1);
        }

        [TestMethod]
        public void ObtenerTipoDeCambioMasReciente()
        {
            ColeccionTipoCambio.AgregarTipoDeCambio(TipoCambioDePrueba);

            TipoCambio segundoTipoDeCambio = new TipoCambio() { Compra = 20, Fecha = DateTime.Now, Origen = UnaMonedaUYU, Destino = UnaMonedaUSD };
            ColeccionTipoCambio.AgregarTipoDeCambio(segundoTipoDeCambio);

            TipoCambio tercerTipoDeCambio = new TipoCambio() { Compra = 30, Fecha = DateTime.Now, Origen = UnaMonedaUYU, Destino = UnaMonedaUSD };
            ColeccionTipoCambio.AgregarTipoDeCambio(tercerTipoDeCambio);

            TipoCambio resultadoCambioMasReciente = ColeccionTipoCambio.ObtenerTipoDeCambioMasReciente(UnaMonedaUYU, UnaMonedaUSD);
            Assert.AreEqual(resultadoCambioMasReciente.Fecha, tercerTipoDeCambio.Fecha);
        }

        [TestMethod]
        public void ObtenerTipoDeCambioMasRecienteInvertido()
        {
            ColeccionTipoCambio.AgregarTipoDeCambio(TipoCambioDePrueba);

            TipoCambio segundoTipoDeCambio = new TipoCambio() { Compra = 20, Fecha = DateTime.Now, Origen = UnaMonedaUYU, Destino = UnaMonedaUSD };
            ColeccionTipoCambio.AgregarTipoDeCambio(segundoTipoDeCambio);

            TipoCambio tercerTipoDeCambio = new TipoCambio() { Compra = 30, Fecha = DateTime.Now, Origen = UnaMonedaUYU, Destino = UnaMonedaUSD };
            ColeccionTipoCambio.AgregarTipoDeCambio(tercerTipoDeCambio);

            TipoCambio resultadoCambioMasReciente = ColeccionTipoCambio.ObtenerTipoDeCambioMasReciente(UnaMonedaUSD, UnaMonedaUYU);
            Assert.AreEqual(resultadoCambioMasReciente, tercerTipoDeCambio);
        }

        [TestMethod]
        public void ObtenerNumeroDeTipoCambioRegistrados()
        {
            int numeroDeTipoDeCambioRegistrados = ColeccionTipoCambio.ObtenerNumeroDeTipoCambioRegistrados();
            ColeccionTipoCambio.AgregarTipoDeCambio(TipoCambioDePrueba);
            int resultadoNumeroDeTipoDeCambioRegistrados = ColeccionTipoCambio.ObtenerNumeroDeTipoCambioRegistrados();

            Assert.AreEqual(resultadoNumeroDeTipoDeCambioRegistrados, numeroDeTipoDeCambioRegistrados + 1);
        }

        [TestMethod]
        public void ExisteTipoCambio()
        {
            ColeccionTipoCambio.AgregarTipoDeCambio(TipoCambioDePrueba);
            bool resultado = ColeccionTipoCambio.ExisteTipoCambio(TipoCambioDePrueba);

            Assert.IsTrue(resultado);
        }
    }
}
