﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;
using FluentApi;

namespace Pruebas
{
	[TestClass]
	public class PruebaColeccionCategoriaGastosBD
	{
        private CategoriaGasto CategoriaGastoDePrueba;

        [TestInitialize]
        public void CargarDatos()
        {
            CategoriaGastoDePrueba = new CategoriaGasto() { EsPadre=true};

        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AgregarCategoriaGasto()
        {
            int tamanioColeccionCategoriaGasto = ColeccionCategoriaGastos.ObtenerNumeroDeCategoriasRegistradas();
            ColeccionCategoriaGastos.AgregarCategoriaGasto(CategoriaGastoDePrueba);
            int resultadoTamanioColeccionCategorias = ColeccionCategoriaGastos.ObtenerNumeroDeCategoriasRegistradas();

            Assert.AreEqual(resultadoTamanioColeccionCategorias, tamanioColeccionCategoriaGasto + 1);
        }

        [TestMethod]
        public void RemoverCategoriaGasto()
        {
            ColeccionCategoriaGastos.AgregarCategoriaGasto(CategoriaGastoDePrueba);
            int tamanioColeccionCategoriaGasto = ColeccionCategoriaGastos.ObtenerNumeroDeCategoriasRegistradas();

            ColeccionCategoriaGastos.RemoverCategoriaGasto(CategoriaGastoDePrueba);
            int resultadoTamanioColeccionCategoriaGastos = ColeccionCategoriaGastos.ObtenerNumeroDeCategoriasRegistradas();

            Assert.AreNotEqual(resultadoTamanioColeccionCategoriaGastos, tamanioColeccionCategoriaGasto);
        }

        [TestMethod]
        public void AgregarSubCategoriaGasto()
        {
            CategoriaGasto subCat = new CategoriaGasto { Categoria = "lapiz" };
            ColeccionCategoriaGastos.AgregarCategoriaGasto(CategoriaGastoDePrueba);
            ColeccionCategoriaGastos.AgregarSubCategoriaGasto(CategoriaGastoDePrueba, subCat);

            CategoriaGasto[] catGastos = ColeccionCategoriaGastos.ObtenerColeccionDeCategoriasDeGastoEnUnaLista().ToArray();

            bool resultadoSubCategoria = catGastos[0].SubCategorias.Contains(subCat);

            Assert.IsTrue(resultadoSubCategoria);
        }

        [TestMethod]
        public void RemoverSubCategoriaGasto()
        {
            CategoriaGasto subCat = new CategoriaGasto { Categoria = "detergente" };
            CategoriaGastoDePrueba.SubCategorias.Add(subCat);
            ColeccionCategoriaGastos.AgregarCategoriaGasto(CategoriaGastoDePrueba);

            ColeccionCategoriaGastos.RemoverSubCategoriaGasto(CategoriaGastoDePrueba, subCat);
            CategoriaGasto[] catGastos = ColeccionCategoriaGastos.ObtenerColeccionDeCategoriasDeGastoEnUnaLista().ToArray();

            bool resultadoSubCategoria = catGastos[0].SubCategorias.Contains(subCat);

            Assert.IsFalse(resultadoSubCategoria);
        }

        [TestMethod]
        public void ExisteCategoriaGasto()
        {
            ColeccionCategoriaGastos.AgregarCategoriaGasto(CategoriaGastoDePrueba);
            Assert.IsTrue(ColeccionCategoriaGastos.ExisteCategoriaGasto(CategoriaGastoDePrueba));
        }

        [TestMethod]
        public void ObtenerNumeroDeCategoriasRegistradas()
        {
            ColeccionCategoriaGastos.AgregarCategoriaGasto(CategoriaGastoDePrueba);
            ColeccionCategoriaGastos.AgregarCategoriaGasto(CategoriaGastoDePrueba);
            int numeroCategorias = 2;
            int resultadoNumeroCategorias = ColeccionCategoriaGastos.ObtenerNumeroDeCategoriasRegistradas();

            Assert.AreEqual(resultadoNumeroCategorias, numeroCategorias);
        }

        [TestMethod]
        public void ObtenerColeccionDeCategoriasDeGastoEnUnaLista()
        {
            ColeccionCategoriaGastos.AgregarCategoriaGasto(CategoriaGastoDePrueba);
            int categoriasTamanioInicial = ColeccionCategoriaGastos.ObtenerNumeroDeCategoriasRegistradas();

            int categoriasTamanioFinal = ColeccionCategoriaGastos.ObtenerNumeroDeCategoriasRegistradas();

            Assert.AreEqual(categoriasTamanioInicial, categoriasTamanioFinal);
        }
    }
}
