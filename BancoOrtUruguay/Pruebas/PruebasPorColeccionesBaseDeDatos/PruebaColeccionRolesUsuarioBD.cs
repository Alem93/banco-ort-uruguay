﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;
using FluentApi;

namespace Pruebas
{
	[TestClass]
	public class PruebaColeccionRolesUsuarioBD
	{
        private RolUsuario RoldePrueba;

        [TestInitialize]
        public void CargarDatos()
        {
            RoldePrueba = new RolUsuario();

        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AgregarRol()
        {
            int numeroRolesRegistradas = ColeccionRolesUsuario.ObtenerNumeroDeRolesRegistrados();
            ColeccionRolesUsuario.AgregarRol(RoldePrueba);
            int resultadoNumeroDeRolesRegistrados = ColeccionRolesUsuario.ObtenerNumeroDeRolesRegistrados();

            Assert.AreEqual(resultadoNumeroDeRolesRegistrados, numeroRolesRegistradas + 1);
        }

        [TestMethod]
        public void ObtenerNumeroDeRolesRegistrados()
        {
            ColeccionRolesUsuario.AgregarRol(RoldePrueba);
            ColeccionRolesUsuario.AgregarRol(RoldePrueba);
            int numeroDeRoles = 2;
            int resultadoNumeroDeRoles = ColeccionRolesUsuario.ObtenerNumeroDeRolesRegistrados();

            Assert.AreEqual(resultadoNumeroDeRoles, numeroDeRoles);
        }

        [TestMethod]
        public void ExisteRol()
        {
            ColeccionRolesUsuario.AgregarRol(RoldePrueba);
            Assert.IsTrue(ColeccionRolesUsuario.ExisteRol(RoldePrueba));
        }

        [TestMethod]
        public void ObtenerColeccionEnUnArray()
        {
            ColeccionRolesUsuario.AgregarRol(RoldePrueba);
            int RolesTamanioInicial = ColeccionRolesUsuario.ObtenerNumeroDeRolesRegistrados();

            RolUsuario[] rolesArray = ColeccionRolesUsuario.ObtenerColeccionEnUnArray();
            int RolesTamanioFinal = rolesArray.Length;

            Assert.AreEqual(RolesTamanioInicial, RolesTamanioFinal);
        }
    }
}
