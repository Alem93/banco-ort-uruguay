﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;
using FluentApi;

namespace Pruebas
{
    [TestClass]
    public class PruebaColeccionCuentasBD
    {
        private Moneda UnaMonedaUSD;
        private Moneda UnaMonedaUYU;
        private Usuario UsuarioDePrueba;
        private Cuenta CuentaDePrueba;
        private Cuenta UltimaCuenta;

        [TestInitialize]
        public void CargarDatos()
        {
            UnaMonedaUSD = new Moneda();
            UnaMonedaUYU = new Moneda();
            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaUSD, TIPO_DE_MONEDA.USD);
            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaUYU, TIPO_DE_MONEDA.UYU);

            UsuarioDePrueba = new Usuario();
            UtilidadesBaseDeDatos.AltaDeUsurioSinRol(UsuarioDePrueba);

            CuentaDePrueba = new Cuenta()
            {
                UnaMoneda = UnaMonedaUSD,
                UnCliente = UsuarioDePrueba
            };

            UltimaCuenta = new Cuenta()
            {
                UnaMoneda = UnaMonedaUYU,
                UnCliente = UsuarioDePrueba
            };
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AgregarCuenta()
        {
            int tamanioColeccionCuentas = ColeccionCuentas.ObtenerNumeroDeCuentasRegistradas();
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
            int resultadoTamanioColeccionCuentas = ColeccionCuentas.ObtenerNumeroDeCuentasRegistradas();

            Assert.AreEqual(resultadoTamanioColeccionCuentas, tamanioColeccionCuentas + 1);
        }

        [TestMethod]
        public void ExisteCuenta()
        {
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
            Assert.IsTrue(ColeccionCuentas.ExisteCuenta(CuentaDePrueba));
        }

        [TestMethod]
        public void RemoverCuenta()
        {
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
            ColeccionCuentas.RemoverCuenta(CuentaDePrueba);

            Assert.IsFalse(ColeccionCuentas.ExisteCuenta(CuentaDePrueba));
        }

        //[TestMethod]
        //public void ObtenerUltimoNumeroReferencia()
        //{
        //    int nroReferencia;
        //    nroReferencia=UltimaCuenta.NumeroDeReferencia;

        //    ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
        //    ColeccionCuentas.AgregarCuenta(UltimaCuenta);

        //    int resultadoUltimoNumeroReferencia = ColeccionCuentas.ObtenerUltimoNumeroReferencia();
        //    Assert.AreEqual(resultadoUltimoNumeroReferencia, nroReferencia);
        //}

        [TestMethod]
        public void ObtenerNumeroDeCuentasRegistradas()
        {
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
            int numeroCuentas = 2;
            int resultadoNumeroCuentas = ColeccionCuentas.ObtenerNumeroDeCuentasRegistradas();

            Assert.AreEqual(resultadoNumeroCuentas, numeroCuentas);
        }

        [TestMethod]
        public void ObtenerColeccionDeUsuarioEnUnArray()
        {
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);

            int CuentasTamanioInicial = ColeccionCuentas.ObtenerNumeroDeCuentasRegistradas();

            Cuenta[] cuentasArray = ColeccionCuentas.ObtenerColeccionDeUsuarioEnUnArray(UsuarioDePrueba);
            int CuentasTamanioFinal = cuentasArray.Length;

            Assert.AreEqual(CuentasTamanioInicial, CuentasTamanioFinal);
        }

        [TestMethod]
        public void ObtenerColeccionSegunEstadoEnUnArray()
        {
            CuentaDePrueba.EstadoCuenta = ESTADO_SOLICITUD_CUENTA.ACEPTADA;
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
            int CuentasTamanioInicial = ColeccionCuentas.ObtenerNumeroDeCuentasRegistradas();

            Cuenta[] cuentasArray = ColeccionCuentas.ObtenerColeccionSegunEstadoEnUnArray(ESTADO_SOLICITUD_CUENTA.ACEPTADA);
            int CuentasTamanioFinal = cuentasArray.Length;

            Assert.AreEqual(CuentasTamanioInicial, CuentasTamanioFinal);
        }

        [TestMethod]
        public void ObtenerColeccionActivaDeUsuarioEnUnArray()
        {
            CuentaDePrueba.EstadoCuenta = ESTADO_SOLICITUD_CUENTA.ACEPTADA;
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
            int CuentasTamanioInicial = ColeccionCuentas.ObtenerNumeroDeCuentasRegistradas();

            Cuenta[] cuentasArray = ColeccionCuentas.ObtenerColeccionActivaDeUsuarioEnUnArray(UsuarioDePrueba);
            int CuentasTamanioFinal = cuentasArray.Length;

            Assert.AreEqual(CuentasTamanioInicial, CuentasTamanioFinal);
        }

        //[TestMethod]
        //public void ObtenerCuentaPorNumeroDeRefencia()
        //{
        //    int nroRef = 1;
        //    CuentaDePrueba.NumeroDeReferencia = nroRef;
        //    ColeccionCuentas.AgregarCuenta(CuentaDePrueba);

        //    Cuenta resultadoCuenta = ColeccionCuentas.ObtenerCuentaPorNumeroDeRefencia(nroRef);

        //    Assert.AreEqual(resultadoCuenta, CuentaDePrueba);
        //}

        [TestMethod]
        public void ObtenerColeccionCuentasEnUnaLista()
        {
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
            ColeccionCuentas.AgregarCuenta(CuentaDePrueba);
            int numeroCuentas = 2;
            int resultadoNumeroCuentas = ColeccionCuentas.ObtenerColeccionCuentasEnUnaLista().Count;

            Assert.AreEqual(resultadoNumeroCuentas, numeroCuentas);
        }
    }
}
