﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentApi;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaColeccionUsuariosBD
    {
        private Usuario UsuarioDePrueba;
        private RolUsuario RolUsu;

        [TestInitialize]
        public void CargarDatos()
        {
            RolUsu = new RolUsuario();

            UtilidadesBaseDeDatos.CargarRolSegunTipoDeUsuario(RolUsu, TIPO_DE_USUARIO.CLIENTE);

            UsuarioDePrueba = new Usuario()
            {
                Nombre = "Jose",
                Cedula = 4567,
                Contrasenia = "contr24",
                Email = "def@"

            };
            UsuarioDePrueba.RolesDeUsuario.Add(RolUsu);
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AgregarUsuario()
        {
            int tamanioColeccionUsuario = ColeccionUsuarios.ObtenerNumeroDeUsuariosRegistrados();
            ColeccionUsuarios.AgregarUsuario(UsuarioDePrueba);
            int resultadoTamanioColeccionUsuarios = ColeccionUsuarios.ObtenerNumeroDeUsuariosRegistrados();

            Assert.AreEqual(resultadoTamanioColeccionUsuarios, tamanioColeccionUsuario + 1);
        }

        [TestMethod]
        public void ObtenerUsuarioLogueado()
        {
            ColeccionUsuarios.AgregarUsuario(UsuarioDePrueba);
            Usuario usuarioLogueado = ColeccionUsuarios.ObtenerUsuarioLogueado(UsuarioDePrueba.Cedula,
                UsuarioDePrueba.Contrasenia);

            Assert.AreEqual(usuarioLogueado, UsuarioDePrueba);
        }

        [TestMethod]
        public void ObtenerNumeroDeUsuariosRegistrados()
        {
            ColeccionUsuarios.AgregarUsuario(UsuarioDePrueba);
            ColeccionUsuarios.AgregarUsuario(UsuarioDePrueba);
            int numeroUsuarios = 2;
            int resultadoNumeroUsuarios = ColeccionUsuarios.ObtenerNumeroDeUsuariosRegistrados();

            Assert.AreEqual(resultadoNumeroUsuarios, numeroUsuarios);
        }

        [TestMethod]
        public void ExisteUsuario()
        {
            ColeccionUsuarios.AgregarUsuario(UsuarioDePrueba);
            Assert.IsTrue(ColeccionUsuarios.ExisteUsuario(UsuarioDePrueba));
        }

        [TestMethod]
        public void ObtenerColeccionEnUnArray()
        {
            ColeccionUsuarios.AgregarUsuario(UsuarioDePrueba);
            int UsuariosTamanioInicial = ColeccionUsuarios.ObtenerNumeroDeUsuariosRegistrados();

            Usuario[] usuariosArray = ColeccionUsuarios.ObtenerColeccionEnUnArray();
            int UsuariosTamanioFinal = usuariosArray.Length;

            Assert.AreEqual(UsuariosTamanioInicial, UsuariosTamanioFinal);
        }

        [TestMethod]
        public void ObtenerColeccionUsuariosEnUnaLista()
        {
            ColeccionUsuarios.AgregarUsuario(UsuarioDePrueba);
            ColeccionUsuarios.AgregarUsuario(UsuarioDePrueba);
            int numeroUsuarios = 2;
            int resultadoNumeroUsuarios = ColeccionUsuarios.ObtenerColeccionUsuariosEnUnaLista().Count;

            Assert.AreEqual(resultadoNumeroUsuarios, numeroUsuarios);
        }
    }
}
