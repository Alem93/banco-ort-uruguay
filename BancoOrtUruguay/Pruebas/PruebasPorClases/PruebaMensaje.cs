﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaMensaje
    {
        private Mensaje PrimerMensaje;
        private Mensaje SegundoMensaje;

        public PruebaMensaje()
        {
            this.PrimerMensaje = new Mensaje();
            this.SegundoMensaje = new Mensaje();
        }


        [TestMethod]
        public void SetearIdMensaje()
        {
            int id = 20;
            PrimerMensaje.Id = id;
            int resultadoId = PrimerMensaje.Id;

            Assert.AreEqual(resultadoId, id);
        }

        [TestMethod]
        public void SetearUnMensajeMensaje()
        {
            string mensaje = "Cuenta rechazada";
            PrimerMensaje.unMensaje= mensaje;
            string resultadoMensaje = PrimerMensaje.unMensaje;

            Assert.AreEqual(resultadoMensaje, mensaje);
        }

        [TestMethod]
        public void CompararMensaIguales()
        {
            Assert.AreEqual(PrimerMensaje, SegundoMensaje);
        }

        [TestMethod]
        public void CompararUsuariosDistintos()
        {
            PrimerMensaje.unMensaje = "otro mensaje";

            Assert.AreNotEqual(PrimerMensaje, SegundoMensaje);
        }
    }
}
