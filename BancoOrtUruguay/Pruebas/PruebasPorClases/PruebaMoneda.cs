﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;
using Excepciones;

namespace Pruebas
{
    [TestClass]
    public class PruebaMoneda
    {
        private static TIPO_DE_MONEDA MONEDA_TIPO_USD = TIPO_DE_MONEDA.USD;
        private static TIPO_DE_MONEDA MONEDA_TIPO_UYU = TIPO_DE_MONEDA.UYU;
        private Moneda PrimeraMoneda;
        private Moneda SegundaMoneda;

        public PruebaMoneda()
        {
            this.PrimeraMoneda = new Moneda();
            this.SegundaMoneda = new Moneda();
        }


        [TestMethod]
        public void SetearIdMoneda()
        {
            int id = 20;
            PrimeraMoneda.Id = id;
            int resultadoId = PrimeraMoneda.Id;

            Assert.AreEqual(resultadoId, id);
        }

        [TestMethod]
        public void SetearNombreMoneda()
        {
            PrimeraMoneda.Nombre = MONEDA_TIPO_USD;
            TIPO_DE_MONEDA resultadoNombre = PrimeraMoneda.Nombre;

            Assert.AreEqual(resultadoNombre, MONEDA_TIPO_USD);
        }

        [TestMethod]
        public void CompararMonedasDistintas()
        {
            PrimeraMoneda.Nombre = MONEDA_TIPO_USD;
            SegundaMoneda.Nombre = MONEDA_TIPO_UYU;
            Assert.AreNotEqual(PrimeraMoneda, SegundaMoneda);
        }

        [TestMethod]
        public void CompararMonedasIguales()
        {
            PrimeraMoneda.Nombre = MONEDA_TIPO_USD;
            SegundaMoneda.Nombre = MONEDA_TIPO_USD;
            Assert.AreEqual(PrimeraMoneda, SegundaMoneda);
        }
    }
}
