﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Logica;
using BancoOrtUruguay;
using Excepciones;

namespace Pruebas
{
    [TestClass]
    public class PruebaUsuario
    {
        private Usuario PrimerUsuario;
        private Usuario SegundoUsuario;
        private static Mensaje UnMensaje = new Mensaje() { unMensaje = "Prueba" };

        public PruebaUsuario()
        {
            this.PrimerUsuario = new Usuario();
            this.SegundoUsuario = new Usuario();
        }

        [TestMethod]
        public void SetearIdUsuario()
        {
            int id = 20;
            PrimerUsuario.Id = id;
            int resultadoId = PrimerUsuario.Id;

            Assert.AreEqual(resultadoId, id);
        }

        [TestMethod]
        public void SetearNombreUsuario()
        {
            string nombre = "Jose";
            PrimerUsuario.Nombre = nombre;
            string resultadoNombre = PrimerUsuario.Nombre;

            Assert.AreEqual(resultadoNombre, nombre);
        }

        [TestMethod]
        public void SetearCedulaUsuario()
        {
            int cedula = 899;
            PrimerUsuario.Cedula = cedula;
            int resultadoCedula = PrimerUsuario.Cedula;

            Assert.AreEqual(resultadoCedula, cedula);
        }

        [TestMethod]
        public void SetearEMailUsuario()
        {
            string email = "jose@";
            PrimerUsuario.Email = email;
            string resultadoEmail = PrimerUsuario.Email;

            Assert.AreEqual(resultadoEmail, email);
        }

        [TestMethod]
        public void SetearContraseniaUsuario()
        {
            string contrasenia = "un23";
            PrimerUsuario.Contrasenia = contrasenia;
            string resultadoContrasenia = PrimerUsuario.Contrasenia;

            Assert.AreEqual(resultadoContrasenia, contrasenia);
        }

        [TestMethod]
        public void SetearTipoUsuario()
        {
            RolUsuario tipoUsuario = new RolUsuario() { NombreRol=TIPO_DE_USUARIO.ADMINISTRADOR };
            PrimerUsuario.RolesDeUsuario.Add(tipoUsuario);

            Assert.IsTrue(PrimerUsuario.RolesDeUsuario.Contains(tipoUsuario));
        }

        [TestMethod]
        public void SetearBuzonDeMensajeUsuario()
        {
            PrimerUsuario.BuzonDeMensajes.Add(UnMensaje);

            Assert.IsTrue(PrimerUsuario.BuzonDeMensajes.Contains(UnMensaje));
        }

        [TestMethod]
        public void SetearFavoritasUsuario()
        {
            Favorita unaFavorita = new Favorita();
            unaFavorita.Alias = "Prueba";
            unaFavorita.NumeroDeReferencia = 45;

            PrimerUsuario.Favoritas.Add(unaFavorita);

            Assert.IsTrue(PrimerUsuario.Favoritas.Contains(unaFavorita));
        }

        [TestMethod]
        public void CompararUsuariosIguales()
        {
            Assert.AreEqual(PrimerUsuario, SegundoUsuario);
        }

        [TestMethod]
        public void CompararUsuariosDistintos()
        {
            PrimerUsuario.Cedula = 789;

            Assert.AreNotEqual(PrimerUsuario, SegundoUsuario);
        }
    }
}
