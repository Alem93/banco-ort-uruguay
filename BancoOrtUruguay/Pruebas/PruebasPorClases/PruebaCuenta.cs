﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaCuenta
    {
        private static Moneda UnaMoneda = new Moneda() { Nombre = TIPO_DE_MONEDA.USD };
        private static Usuario UnCliente = new Usuario()
        {
            Nombre = "JUAN",
            Cedula = 45,
            Contrasenia = "contr1",
            Email = "juan@"
        };

        private Cuenta PrimeraCuenta;
        private Cuenta SegundaCuenta;

        public PruebaCuenta()
        {
            this.PrimeraCuenta = new Cuenta();
            this.SegundaCuenta = new Cuenta();
        }


        [TestMethod]
        public void SetearIdCuenta()
        {
            int id = 20;
            PrimeraCuenta.Id = id;
            int resultadoId = PrimeraCuenta.Id;

            Assert.AreEqual(resultadoId, id);
        }

        [TestMethod]
        public void SetearMonedaCuenta()
        {
            PrimeraCuenta.UnaMoneda = UnaMoneda;
            Moneda resultadoMoneda = PrimeraCuenta.UnaMoneda;

            Assert.AreEqual(resultadoMoneda, UnaMoneda);
        }

        [TestMethod]
        public void SetearClienteCuenta()
        {
            PrimeraCuenta.UnCliente = UnCliente;
            Usuario resultadoCliente = PrimeraCuenta.UnCliente;

            Assert.AreEqual(resultadoCliente, UnCliente);
        }

        [TestMethod]
        public void SetearBalanceCuenta()
        {
            int balance = 500;
            PrimeraCuenta.Balance = balance;
            double resultadoBalance = PrimeraCuenta.Balance;

            Assert.AreEqual(resultadoBalance, balance);
        }

        [TestMethod]
        public void SetearMontoMinimo()
        {
            int montoMinimo = -500;
            PrimeraCuenta.MontoMinimo = montoMinimo;
            double resultadoMontoMinimo = PrimeraCuenta.MontoMinimo;

            Assert.AreEqual(resultadoMontoMinimo, montoMinimo);
        }

        [TestMethod]
        public void SetearNumeroDeReferencia()
        {
            int numeroReferencia = 80;
            PrimeraCuenta.NumeroDeReferencia = numeroReferencia;
            int resultadoNumeroReferencia = PrimeraCuenta.NumeroDeReferencia;

            Assert.AreEqual(resultadoNumeroReferencia, numeroReferencia);
        }

        [TestMethod]
        public void SetearDescripcionCuenta()
        {
            string descripcion = "Esta es una descripcion de prueba";
            PrimeraCuenta.Descripcion = descripcion;
            string resultadoDescripcion = PrimeraCuenta.Descripcion;

            Assert.AreEqual(resultadoDescripcion, descripcion);
        }

        [TestMethod]
        public void SetearTipoDeCuenta()
        {
            TIPO_DE_CUENTA tipoCuenta = TIPO_DE_CUENTA.CAJA_DE_AHORRO;
            PrimeraCuenta.TipoDeCuenta = tipoCuenta;
            TIPO_DE_CUENTA resultadoTipoDeCuenta = PrimeraCuenta.TipoDeCuenta;

            Assert.AreEqual(resultadoTipoDeCuenta, tipoCuenta);
        }

        [TestMethod]
        public void SetearEstadoSolicitudCuenta()
        {
            ESTADO_SOLICITUD_CUENTA estadoCuenta = ESTADO_SOLICITUD_CUENTA.ACEPTADA;
            PrimeraCuenta.EstadoCuenta = estadoCuenta;
            ESTADO_SOLICITUD_CUENTA resultadoEstadoCuenta = PrimeraCuenta.EstadoCuenta;

            Assert.AreEqual(resultadoEstadoCuenta, estadoCuenta);
        }

        [TestMethod]
        public void CompararCuentasIguales()
        {
            Assert.AreEqual(PrimeraCuenta, SegundaCuenta);
        }

        [TestMethod]
        public void CompararCuentasDistintas()
        {
            PrimeraCuenta.Id = 5;
            Assert.AreNotEqual(PrimeraCuenta, SegundaCuenta);
        }
    }
}
