﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaTransaccionInternacional
    {
        private static long UnNumeroSwft = 13456234896;

        private static long UnNumeroCuentaDestino = 56787777796;

        private static string UnNombreDestinatario = "Jose";

        private static int CantidadDebitada = 890;

        private static Moneda UnaMoneda = new Moneda() { Nombre = TIPO_DE_MONEDA.USD };

        private static Usuario UnUsuario = new Usuario()
        {
            Nombre = "Jose",
            Cedula = 456,
            Contrasenia = "25sd",
            Email = "jos@"
        };
        private static Cuenta UnaCuenta = new Cuenta()
        {
            Balance = 24,
            Descripcion = "cuenta de prueba",
            EstadoCuenta = ESTADO_SOLICITUD_CUENTA.ACEPTADA,
            NumeroDeReferencia = 4567,
            TipoDeCuenta = TIPO_DE_CUENTA.CAJA_DE_AHORRO,
            UnaMoneda = UnaMoneda,
            UnCliente = UnUsuario
        };

        private Transaccion PrimeraTransaccionInternacional;
        private Transaccion SegundaTransaccionInternacional;

        public PruebaTransaccionInternacional()
        {
            this.PrimeraTransaccionInternacional = new TransaccionInternacional();
            this.SegundaTransaccionInternacional = new TransaccionInternacional();
        }

        [TestMethod]
        public void SetearCuentaOrigenTransaccionInternacioal()
        {
            ((TransaccionInternacional)PrimeraTransaccionInternacional).CuentaOrigen = UnaCuenta;

            Assert.AreEqual(((TransaccionInternacional)PrimeraTransaccionInternacional).CuentaOrigen, UnaCuenta);
        }

        [TestMethod]
        public void SetearNullCuentaDestinoTransaccionInternacional()
        {
            Assert.IsNull(((TransaccionInternacional)PrimeraTransaccionInternacional).CuentaDestino);
        }

        [TestMethod]
        public void SetearCantidadDebitadaTransaccionInternacional()
        {
            ((TransaccionInternacional)PrimeraTransaccionInternacional).CantidadDebitada = CantidadDebitada;

            Assert.AreEqual(((TransaccionInternacional)PrimeraTransaccionInternacional).CantidadDebitada, CantidadDebitada);
        }

        [TestMethod]
        public void SetearCategoriaGastoTransaccionInternacional()
        {
            string nombreCategoriaGasto = "Ferreateria-tornillos";
            ((TransaccionInternacional)PrimeraTransaccionInternacional).CategoriaDeGasto = nombreCategoriaGasto;

            Assert.AreEqual(((TransaccionInternacional)PrimeraTransaccionInternacional).CategoriaDeGasto, nombreCategoriaGasto);
        }

        [TestMethod]
        public void SetearFechaTransaccionInternacional()
        {
            DateTime fecha = DateTime.Now;
            ((TransaccionInternacional)PrimeraTransaccionInternacional).Fecha = fecha;
            DateTime resultadoFecha = ((TransaccionInternacional)PrimeraTransaccionInternacional).Fecha;

            Assert.AreEqual(resultadoFecha, fecha);
        }

        [TestMethod]
        public void SetearNumeroSwiftTransaccionInternacioal()
        {
            ((TransaccionInternacional)PrimeraTransaccionInternacional).NumeroSwift = UnNumeroSwft;

            Assert.AreEqual(((TransaccionInternacional)PrimeraTransaccionInternacional).NumeroSwift, UnNumeroSwft);
        }

        [TestMethod]
        public void SetearNumeroCuentaDestinoTransaccionInternacioal()
        {
            ((TransaccionInternacional)PrimeraTransaccionInternacional).NumeroCuentaDestino = UnNumeroCuentaDestino;

            Assert.AreEqual(((TransaccionInternacional)PrimeraTransaccionInternacional).NumeroCuentaDestino, UnNumeroCuentaDestino);
        }

        [TestMethod]
        public void SetearNombreDestinatarioTransaccionInternacioal()
        {
            ((TransaccionInternacional)PrimeraTransaccionInternacional).NombreDestinatario = UnNombreDestinatario;

            Assert.AreEqual(((TransaccionInternacional)PrimeraTransaccionInternacional).NombreDestinatario, UnNombreDestinatario);
        }

        [TestMethod]
        public void CompararTransaccionesInternacionalesIguales()
        {
            ((TransaccionInternacional)PrimeraTransaccionInternacional).CantidadDebitada = CantidadDebitada;
            ((TransaccionInternacional)PrimeraTransaccionInternacional).NumeroSwift = UnNumeroSwft;
            ((TransaccionInternacional)PrimeraTransaccionInternacional).CuentaOrigen = UnaCuenta;
            ((TransaccionInternacional)PrimeraTransaccionInternacional).NumeroCuentaDestino = UnNumeroCuentaDestino;

            ((TransaccionInternacional)SegundaTransaccionInternacional).CantidadDebitada = CantidadDebitada;
            ((TransaccionInternacional)SegundaTransaccionInternacional).NumeroSwift = UnNumeroSwft;
            ((TransaccionInternacional)SegundaTransaccionInternacional).CuentaOrigen = UnaCuenta;
            ((TransaccionInternacional)SegundaTransaccionInternacional).NumeroCuentaDestino = UnNumeroCuentaDestino;

            Assert.AreEqual(((TransaccionInternacional)PrimeraTransaccionInternacional), ((TransaccionInternacional)SegundaTransaccionInternacional));
        }

        [TestMethod]
        public void CompararTransaccionesInternacionalesDistintas()
        {
            ((TransaccionInternacional)PrimeraTransaccionInternacional).CantidadDebitada = CantidadDebitada;
            ((TransaccionInternacional)PrimeraTransaccionInternacional).CuentaOrigen = UnaCuenta;

            ((TransaccionInternacional)SegundaTransaccionInternacional).CantidadDebitada = 85;
            ((TransaccionInternacional)SegundaTransaccionInternacional).CuentaOrigen = UnaCuenta;

            Assert.AreNotEqual(PrimeraTransaccionInternacional, ((TransaccionInternacional)SegundaTransaccionInternacional));
        }
    }
}
