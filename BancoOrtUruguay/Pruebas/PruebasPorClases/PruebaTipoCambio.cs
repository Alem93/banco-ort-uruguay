﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;
using Excepciones;

namespace Pruebas
{
    [TestClass]
    public class PruebaTipoCambio
    {
        private Moneda UnaMoneda;
        private TipoCambio PrimerTipoCambio;
        private TipoCambio SegundoTipoCambio;

        public PruebaTipoCambio()
        {
            this.PrimerTipoCambio = new TipoCambio();
            this.SegundoTipoCambio = new TipoCambio();
            this.UnaMoneda = new Moneda();
        }


        [TestMethod]
        public void SetearIdTipoCambio()
        {
            int id = 20;
            PrimerTipoCambio.Id = id;
            int resultadoId = PrimerTipoCambio.Id;

            Assert.AreEqual(resultadoId, id);
        }

        [TestMethod]
        public void SetearOrigenTipoCambio()
        {
            UnaMoneda.Nombre = TIPO_DE_MONEDA.USD;
            PrimerTipoCambio.Origen = UnaMoneda;
            Moneda resultadoMonedaOrigen = PrimerTipoCambio.Origen;

            Assert.AreEqual(resultadoMonedaOrigen, UnaMoneda);
        }

        [TestMethod]
        public void SetearDestinoTipoCambio()
        {
            UnaMoneda.Nombre = TIPO_DE_MONEDA.UYU;
            PrimerTipoCambio.Destino = UnaMoneda;
            Moneda resultadoMonedaDestino = PrimerTipoCambio.Destino;

            Assert.AreEqual(resultadoMonedaDestino, UnaMoneda);
        }

        [TestMethod]
        public void SetearFechaTipoCambio()
        {
            DateTime fecha = DateTime.Now;
            PrimerTipoCambio.Fecha = fecha;
            DateTime resultadoFecha = PrimerTipoCambio.Fecha;

            Assert.AreEqual(resultadoFecha, fecha);
        }

        [TestMethod]
        public void SetearCompraCambio()
        {
            double compra = 50;
            PrimerTipoCambio.Compra = compra;
            double resultadoCompra = PrimerTipoCambio.Compra;

            Assert.AreEqual(resultadoCompra, compra);
        }

        [TestMethod]
        public void CompararTipoCambioIguales()
        {
            Assert.AreEqual(PrimerTipoCambio, SegundoTipoCambio);
        }
    }
}
