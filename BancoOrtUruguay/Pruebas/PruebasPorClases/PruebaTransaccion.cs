﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaTransaccion
    {
        private static Moneda UnaMoneda = new Moneda() { Nombre = TIPO_DE_MONEDA.USD };
        private static Usuario UnUsuario = new Usuario()
        {
            Nombre = "Jose",
            Cedula = 456,
            Contrasenia = "25sd",
            Email = "jos@"
        };
        private static Cuenta UnaCuenta = new Cuenta()
        {
            Balance = 24,
            Descripcion = "cuenta de prueba",
            EstadoCuenta = ESTADO_SOLICITUD_CUENTA.ACEPTADA,
            NumeroDeReferencia = 4567,
            TipoDeCuenta = TIPO_DE_CUENTA.CAJA_DE_AHORRO,
            UnaMoneda = UnaMoneda,
            UnCliente = UnUsuario
        };

        private static int CantidadDebitada = 890;
        private Transaccion PrimeraTransaccion;
        private Transaccion SegundaTransaccion;

        [TestMethod]
        public void SetearIdTransaccion()
        {
            int id = 20;
            PrimeraTransaccion.Id = id;
            int resultadoId = PrimeraTransaccion.Id;

            Assert.AreEqual(resultadoId, id);
        }

        public PruebaTransaccion()
        {
            this.PrimeraTransaccion = new Transaccion();
            this.SegundaTransaccion = new Transaccion();
        }

        [TestMethod]
        public void SetearCuentaOrigenTransaccion()
        {
            PrimeraTransaccion.CuentaOrigen = UnaCuenta;

            Assert.AreEqual(PrimeraTransaccion.CuentaOrigen, UnaCuenta);
        }

        [TestMethod]
        public void SetearCuentaDestinoTransaccion()
        {
            PrimeraTransaccion.CuentaDestino = UnaCuenta;

            Assert.AreEqual(PrimeraTransaccion.CuentaDestino, UnaCuenta);
        }

        [TestMethod]
        public void SetearCantidadDebitadaTransaccion()
        {
            PrimeraTransaccion.CantidadDebitada = CantidadDebitada;

            Assert.AreEqual(PrimeraTransaccion.CantidadDebitada, CantidadDebitada);
        }

        [TestMethod]
        public void SetearCategoriaGastoTransaccion()
        {
            string nombreCategoriaGasto = "Limpieza-escoba";
            PrimeraTransaccion.CategoriaDeGasto = nombreCategoriaGasto;

            Assert.AreEqual(PrimeraTransaccion.CategoriaDeGasto, nombreCategoriaGasto);
        }

        [TestMethod]
        public void SetearFechaTransaccion()
        {
            DateTime fecha = DateTime.Now;
            PrimeraTransaccion.Fecha = fecha;
            DateTime resultadoFecha = PrimeraTransaccion.Fecha;

            Assert.AreEqual(resultadoFecha, fecha);
        }

        [TestMethod]
        public void CompararTransaccionesIguales()
        {
            PrimeraTransaccion.CantidadDebitada = CantidadDebitada;
            PrimeraTransaccion.CuentaDestino = UnaCuenta;
            PrimeraTransaccion.CuentaOrigen = UnaCuenta;

            SegundaTransaccion.CantidadDebitada = CantidadDebitada;
            SegundaTransaccion.CuentaDestino = UnaCuenta;
            SegundaTransaccion.CuentaOrigen = UnaCuenta;

            Assert.AreEqual(PrimeraTransaccion, SegundaTransaccion);
        }

        [TestMethod]
        public void CompararTransaccionesDistintas()
        {
            PrimeraTransaccion.CantidadDebitada = CantidadDebitada;
            PrimeraTransaccion.CuentaDestino = UnaCuenta;
            PrimeraTransaccion.CuentaOrigen = UnaCuenta;

            SegundaTransaccion.CantidadDebitada = 85;
            SegundaTransaccion.CuentaOrigen = UnaCuenta;
            SegundaTransaccion.CuentaDestino = UnaCuenta;

            Assert.AreNotEqual(PrimeraTransaccion, SegundaTransaccion);
        }
    }
}
