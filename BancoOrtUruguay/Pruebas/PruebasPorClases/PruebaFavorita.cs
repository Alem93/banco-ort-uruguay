﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaFavorita
    {
        private static int NUMERO_REF = 45;
        private static string ALIAS = "Alias de prueba";
        private Favorita PrimeraFavorita;
        private Favorita SegundaFavorita;

        public PruebaFavorita()
        {
            this.PrimeraFavorita = new Favorita();
            this.SegundaFavorita = new Favorita();
        }


        [TestMethod]
        public void SetearIdFavorita()
        {
            int id = 20;
            PrimeraFavorita.Id = id;
            int resultadoId = PrimeraFavorita.Id;

            Assert.AreEqual(resultadoId, id);
        }

        [TestMethod]
        public void SetearNumeroReferenciaFavorita()
        {
            PrimeraFavorita.NumeroDeReferencia = NUMERO_REF;
            int resultadoNumeroReferencia = PrimeraFavorita.NumeroDeReferencia;

            Assert.AreEqual(resultadoNumeroReferencia, NUMERO_REF);
        }

        [TestMethod]
        public void SetearAliasFavorita()
        {
            PrimeraFavorita.Alias = ALIAS;
            string resultadoAlias = PrimeraFavorita.Alias;

            Assert.AreEqual(resultadoAlias, ALIAS);
        }

        [TestMethod]
        public void CompararFavoritasIguales()
        {
            Assert.AreEqual(PrimeraFavorita, SegundaFavorita);
        }

        [TestMethod]
        public void CompararFavoritasDistintas()
        {
            PrimeraFavorita.NumeroDeReferencia = 5;
            Assert.AreNotEqual(PrimeraFavorita, SegundaFavorita);
        }
    }
}
