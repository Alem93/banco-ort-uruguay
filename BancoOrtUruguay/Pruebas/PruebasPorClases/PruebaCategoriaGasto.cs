﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaCategoriaGasto
    {
        private CategoriaGasto PrimeraCategoria;
        private CategoriaGasto SegundaCategoria;

        public PruebaCategoriaGasto()
        {
            this.PrimeraCategoria = new CategoriaGasto();
            this.SegundaCategoria = new CategoriaGasto();
        }


        [TestMethod]
        public void SetearIdCategoriaGasto()
        {
            int id = 20;
            PrimeraCategoria.Id = id;
            int resultadoId = PrimeraCategoria.Id;

            Assert.AreEqual(resultadoId, id);
        }

        [TestMethod]
        public void SetearEsPadreCategoriaGasto()
        {
            bool esPadre = true;
            PrimeraCategoria.EsPadre = esPadre;
            bool resultadoId = PrimeraCategoria.EsPadre;

            Assert.IsTrue(resultadoId);
        }

        [TestMethod]
        public void SetearNombreCategoria()
        {
            string nombre = "Limpieza";
            PrimeraCategoria.Categoria = nombre;
            string resultadoNombreCategoria = PrimeraCategoria.Categoria;

            Assert.AreEqual(resultadoNombreCategoria, nombre);
        }

        [TestMethod]
        public void SetearSubcategoriaDeCategoria()
        {
            CategoriaGasto nombreSubCategoria =new CategoriaGasto {Categoria= "detergente" };
            PrimeraCategoria.SubCategorias.Add(nombreSubCategoria);

            Assert.IsTrue(PrimeraCategoria.SubCategorias.Contains(nombreSubCategoria));
        }

        [TestMethod]
        public void CompararCategoriasIguales()
        {
            Assert.AreEqual(PrimeraCategoria, SegundaCategoria);
        }

        [TestMethod]
        public void CompararCategoriasDistintos()
        {
            PrimeraCategoria.Categoria="otro";

            Assert.AreNotEqual(PrimeraCategoria, SegundaCategoria);
        }
    }
}
