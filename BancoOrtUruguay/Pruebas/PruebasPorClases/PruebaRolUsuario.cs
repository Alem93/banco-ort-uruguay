﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaRolUsuario
    {
        private static TIPO_DE_USUARIO ROL_CLIENTE = TIPO_DE_USUARIO.CLIENTE;
        private static TIPO_DE_USUARIO ROL_ADMINISTRADOR = TIPO_DE_USUARIO.ADMINISTRADOR;
        private RolUsuario RolUsu;
        private RolUsuario RolAdmin;

        public PruebaRolUsuario()
        {
            this.RolUsu = new RolUsuario();
            this.RolAdmin = new RolUsuario();
        }

        [TestMethod]
        public void SetearIdRolUsuario()
        {
            int id = 20;
            RolUsu.Id = id;
            int resultadoId = RolUsu.Id;

            Assert.AreEqual(resultadoId, id);
        }

        [TestMethod]
        public void SetearNombreRol()
        {
            RolUsu.NombreRol = ROL_CLIENTE;
            TIPO_DE_USUARIO resultadoNombre = RolUsu.NombreRol;

            Assert.AreEqual(resultadoNombre, ROL_CLIENTE);
        }

        [TestMethod]
        public void CompararRolesDistintos()
        {
            RolUsu.NombreRol = ROL_CLIENTE;
            RolAdmin.NombreRol = ROL_ADMINISTRADOR;
            Assert.AreNotEqual(RolUsu, RolAdmin);
        }

        [TestMethod]
        public void CompararRolesIguales()
        {
            RolUsu.NombreRol = ROL_CLIENTE;
            RolAdmin.NombreRol = ROL_CLIENTE;
            Assert.AreEqual(RolUsu, RolAdmin);
        }
    }
}
