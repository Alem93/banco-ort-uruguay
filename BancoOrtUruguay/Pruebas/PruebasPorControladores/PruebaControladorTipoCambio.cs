﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentApi;
using Logica;
using Excepciones;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaControladorTipoCambio
    {
        private Moneda UnaMonedaEnUSD;
        private Moneda UnaMonedaEnUYU;
        private TipoCambio UnTipoCambio;
        private ControladorTipoCambio ControladorDePruebaTipoCambio;

        [TestInitialize]
        public void CargarDatos()
        {
            UnaMonedaEnUSD = new Moneda();
            UnaMonedaEnUYU = new Moneda();
            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaEnUSD, TIPO_DE_MONEDA.USD);
            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaEnUYU, TIPO_DE_MONEDA.UYU);

            this.UnTipoCambio = new TipoCambio();
            this.ControladorDePruebaTipoCambio = new ControladorTipoCambio();
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AltaTipoDeCambioCorrecto()
        {
            UnTipoCambio.Origen = UnaMonedaEnUSD;
            UnTipoCambio.Destino = UnaMonedaEnUYU;
            UnTipoCambio.Compra = 30;
            ControladorDePruebaTipoCambio.AltaTipoDeCambio(UnTipoCambio);

            Assert.IsTrue(ControladorDePruebaTipoCambio.ExisteTipoCambio(UnTipoCambio));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTipoCambioSinMonedaAsignada))]
        public void AltaTipoDeCambioErrorSinMonedaOrigenDestino()
        {
            UnTipoCambio.Origen = UnaMonedaEnUSD;
            UnTipoCambio.Compra = 30;

            ControladorDePruebaTipoCambio.AltaTipoDeCambio(UnTipoCambio);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTipoCambioOrigenDestinoIguales))]
        public void AltaTipoDeCambioErrorMonedaOrigenDestinoIguales()
        {
            UnTipoCambio.Destino = UnaMonedaEnUSD;
            UnTipoCambio.Origen = UnaMonedaEnUSD;
            UnTipoCambio.Compra = 30;

            ControladorDePruebaTipoCambio.AltaTipoDeCambio(UnTipoCambio);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTipoDeCambioSinCantidad))]
        public void AltaTipoDeCambioErrorSinCantidadIngresada()
        {
            UnTipoCambio.Destino = UnaMonedaEnUSD;
            UnTipoCambio.Origen = UnaMonedaEnUYU;

            ControladorDePruebaTipoCambio.AltaTipoDeCambio(UnTipoCambio);
        }

        [TestMethod]
        public void ConversionCambioCorrecto()
        {
            UnTipoCambio.Origen = UnaMonedaEnUSD;
            UnTipoCambio.Destino = UnaMonedaEnUYU;
            UnTipoCambio.Compra = 30.0;
            ControladorDePruebaTipoCambio.AltaTipoDeCambio(UnTipoCambio);
            double cantidadATransferirUSD = 5000;

            double resultadoCantidadATransferirUSD = cantidadATransferirUSD * 30.0;

            ControladorDePruebaTipoCambio.ConvertirTipoCambio(UnaMonedaEnUSD, ref cantidadATransferirUSD, UnaMonedaEnUYU);

            Assert.AreEqual(resultadoCantidadATransferirUSD, cantidadATransferirUSD);
        }

        [TestMethod]
        public void ConversionCambioCorrectoVarianteEnFechaMasReciente()
        {
            DateTime nuevaFecha = new DateTime(2016, 11, 22, 12, 20, 45);
            UnTipoCambio.Origen = UnaMonedaEnUSD;
            UnTipoCambio.Destino = UnaMonedaEnUYU;
            UnTipoCambio.Compra = 28.0;
            ControladorDePruebaTipoCambio.AltaTipoDeCambio(UnTipoCambio);
            UtilidadesBaseDeDatos.ModificarFechaTipoCambio(UnTipoCambio, nuevaFecha);

            TipoCambio tipoCambioMasReciente = new TipoCambio();
            tipoCambioMasReciente.Origen = UnaMonedaEnUSD;
            tipoCambioMasReciente.Destino = UnaMonedaEnUYU;
            tipoCambioMasReciente.Compra = 30.0;
            ControladorDePruebaTipoCambio.AltaTipoDeCambio(tipoCambioMasReciente);

            double cantidadATransferirUSD = 5000;
            double resultadoEsperadoUYU = cantidadATransferirUSD * 30.0;

            ControladorDePruebaTipoCambio.ConvertirTipoCambio(UnaMonedaEnUSD, ref cantidadATransferirUSD, UnaMonedaEnUYU);

            Assert.AreEqual(resultadoEsperadoUYU, cantidadATransferirUSD);
        }

        [TestMethod]
        public void ConversionCambioCorrectoVarianteEnTipoCambio()
        {
            double cambioInvertido = (1.0 / 30.0);
            UnTipoCambio.Origen = UnaMonedaEnUSD;
            UnTipoCambio.Destino = UnaMonedaEnUYU;
            UnTipoCambio.Compra = 30;
            ControladorDePruebaTipoCambio.AltaTipoDeCambio(UnTipoCambio);

            double cantidadATransferirUYU = 150000;
            double resultadoEsperadoUSD = cantidadATransferirUYU * cambioInvertido;

            ControladorDePruebaTipoCambio.ConvertirTipoCambio(UnaMonedaEnUYU, ref cantidadATransferirUYU, UnaMonedaEnUSD);

            Assert.AreEqual(resultadoEsperadoUSD, cantidadATransferirUYU);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTipoDeCambioNoRegistrado))]
        public void ConversionCambioErrorTipoCambioNoExiste()
        {
            double cantidadATransferir = 5000;
            ControladorDePruebaTipoCambio.ConvertirTipoCambio(UnaMonedaEnUSD, ref cantidadATransferir, UnaMonedaEnUYU);
        }
    }
}
