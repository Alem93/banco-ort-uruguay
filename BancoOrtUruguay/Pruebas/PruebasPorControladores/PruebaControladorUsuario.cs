﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;
using Excepciones;
using Logica;

namespace Pruebas
{
    [TestClass]
    public class PruebaControladorUsuario
    {
        private Usuario UsuarioDePrueba;
        private Mensaje UnMensaje;
        private Favorita UnaFavorita;
        private ControladorUsuario ControladorDePruebaUsuario;
        private static bool ES_DUENIO=true;
        private static bool EXISTE_NRO_REF = true;
        RolUsuario RolCli;

        [TestInitialize]
        public void CargarDatos()
        {
            RolCli = new RolUsuario();
            UtilidadesBaseDeDatos.CargarRolSegunTipoDeUsuario(RolCli, TIPO_DE_USUARIO.CLIENTE);

            this.UsuarioDePrueba = new Usuario();
            this.ControladorDePruebaUsuario = new ControladorUsuario();
            this.UnMensaje = new Mensaje();
            this.UnaFavorita = new Favorita();
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }


        [TestMethod]
        public void AltaDeUsuarioCorrecto()
        {
            ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);

            Assert.IsTrue(ControladorDePruebaUsuario.ExisteUsuario(UsuarioDePrueba));
        }

        [TestMethod]
        public void AltaDeUsuarioCorrectoUsuarioVacio()
        {
            Usuario usuarioVacio = new Usuario();
            ControladorDePruebaUsuario.AltaDeUsuario(usuarioVacio, usuarioVacio.Contrasenia);
            Assert.IsTrue(ControladorDePruebaUsuario.ExisteUsuario(usuarioVacio));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionUsuarioContraseniaIncorrecta))]
        public void AltaDeUsuarioErrorContraseniaIncorrecta()
        {
            string verificadorContraseniaIncorrecto = "ap234";
            ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, verificadorContraseniaIncorrecto);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionUsuarioExiste))]
        public void AltaDeUsuarioErrorYaExiste()
        {
            ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
        }


        [TestMethod]
        public void LoginCorrecto()
        {
            ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            ControladorDePruebaUsuario.Login(UsuarioDePrueba.Cedula, UsuarioDePrueba.Contrasenia);
            Usuario usuarioLogueado = ControladorDePruebaUsuario.ObtenerUsuarioLogin();

            Assert.AreEqual(usuarioLogueado, UsuarioDePrueba);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionLoginUsuarioInvalido))]
        public void LoginErrorUsuarioInvalido()
        {
            ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            string verificadorContrasenia = "asp25";
            ControladorDePruebaUsuario.Login(UsuarioDePrueba.Cedula, verificadorContrasenia);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionLoginSinUsuario))]
        public void DesLoguear()
        {
            ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            ControladorDePruebaUsuario.Login(UsuarioDePrueba.Cedula, UsuarioDePrueba.Contrasenia);
            Usuario usuarioLogueado = ControladorDePruebaUsuario.ObtenerUsuarioLogin();
            ControladorDePruebaUsuario.DesLoguear();
            Usuario usuarioDesLogueado = ControladorDePruebaUsuario.ObtenerUsuarioLogin();
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionLoginSinUsuario))]
        public void ObtenerUsuarioLoginErrorUsuarioInvalido()
        {
            Usuario usuarioLogueado = ControladorDePruebaUsuario.ObtenerUsuarioLogin();
        }

        [TestMethod]
        public void ObtenerUsuariosClientesEnUnArray()
        {
            UsuarioDePrueba.RolesDeUsuario.Add(RolCli);
            this.ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            int UsuariosTamanioInicial = ControladorDePruebaUsuario.ObtenerNumeroUsuariosRegistrados();

            Usuario[] usuariosArray = ControladorDePruebaUsuario.ObtenerUsuariosClientesEnUnArray();
            int UsuariosTamanioFinal = usuariosArray.Length;

            Assert.AreEqual(UsuariosTamanioInicial, UsuariosTamanioFinal);
        }

        [TestMethod]
        public void ObtenerNumeroUsuariosRegistrados()
        {
            ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            Usuario otroUsuario = new Usuario();
            otroUsuario.Cedula = 45;
            ControladorDePruebaUsuario.AltaDeUsuario(otroUsuario, otroUsuario.Contrasenia);
            int numeroUsuarios = 2;
            int resultadoNumeroUsuarios = ControladorDePruebaUsuario.ObtenerNumeroUsuariosRegistrados();

            Assert.AreEqual(resultadoNumeroUsuarios, numeroUsuarios);
        }

        [TestMethod]
        public void EnviarMensajeUsuarioCorrecto()
        {
            string mensaje = "Cuenta aprobada";
            UnMensaje.unMensaje = mensaje;
            this.ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);

            this.ControladorDePruebaUsuario.EnviarMensajeUsuario(UnMensaje, UsuarioDePrueba);

            Assert.IsTrue(UtilidadesBaseDeDatos.ExisteMensaje(UsuarioDePrueba,UnMensaje));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionMensajeSinMensaje))]
        public void EnviarMensajeUsuarioErrorSinMensaje()
        {
            this.ControladorDePruebaUsuario.EnviarMensajeUsuario(UnMensaje, UsuarioDePrueba);
        }

        [TestMethod]
        public void EliminarMensajeUsuario()
        {
            string mensaje = "Cuenta aprobada";
            UnMensaje.unMensaje = mensaje;

            this.ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            this.ControladorDePruebaUsuario.EnviarMensajeUsuario(UnMensaje, UsuarioDePrueba);
            this.ControladorDePruebaUsuario.EliminarMensajeUsuario(UnMensaje, UsuarioDePrueba);

            Assert.IsFalse(UtilidadesBaseDeDatos.ExisteMensaje(UsuarioDePrueba, UnMensaje));
        }

        [TestMethod]
        public void AgregarFavoritoUsuarioCorrecto()
        {
            string alias = "Cuenta de jose";
            int nroRef = 45;
            UnaFavorita.Alias = alias;
            UnaFavorita.NumeroDeReferencia = nroRef;

            this.ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            this.ControladorDePruebaUsuario.AgregarFavoritaUsuario(UnaFavorita, UsuarioDePrueba,!ES_DUENIO,EXISTE_NRO_REF);

            Assert.IsTrue(UtilidadesBaseDeDatos.ExisteFavorito(UsuarioDePrueba,UnaFavorita));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionFavoritaYaExiste))]
        public void AgregarFavoritoUsuarioErrorYaExiste()
        {
            string alias = "Cuenta de jose";
            int nroRef = 45;
            UnaFavorita.Alias = alias;
            UnaFavorita.NumeroDeReferencia = nroRef;

            this.ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            this.ControladorDePruebaUsuario.AgregarFavoritaUsuario(UnaFavorita, UsuarioDePrueba, !ES_DUENIO, EXISTE_NRO_REF);
            this.ControladorDePruebaUsuario.AgregarFavoritaUsuario(UnaFavorita, UsuarioDePrueba, !ES_DUENIO, EXISTE_NRO_REF);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionFavoritaFaltanCompletarCampos))]
        public void AgregarFavoritoUsuarioErrorSinAlias()
        {
            int nroRef = 45;
            UnaFavorita.NumeroDeReferencia = nroRef;

            this.ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            this.ControladorDePruebaUsuario.AgregarFavoritaUsuario(UnaFavorita, UsuarioDePrueba, !ES_DUENIO, EXISTE_NRO_REF);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionFavoritaFaltanCompletarCampos))]
        public void AgregarFavoritoUsuarioErrorSinNroRef()
        {
            string alias = "Cuenta de jose";
            UnaFavorita.Alias = alias;

            this.ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            this.ControladorDePruebaUsuario.AgregarFavoritaUsuario(UnaFavorita, UsuarioDePrueba, !ES_DUENIO, EXISTE_NRO_REF);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionFavoritaDueniosIguales))]
        public void AgregarFavoritoUsuarioErrorDueniosiguales()
        {
            string alias = "Cuenta de jose";
            int nroRef = 45;
            UnaFavorita.Alias = alias;
            UnaFavorita.NumeroDeReferencia = nroRef;

            this.ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            this.ControladorDePruebaUsuario.AgregarFavoritaUsuario(UnaFavorita, UsuarioDePrueba, ES_DUENIO, EXISTE_NRO_REF);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCuentaNoExisteNroRef))]
        public void AgregarFavoritoUsuarioErrorNoExisteCuenta()
        {
            string alias = "Cuenta de jose";
            int nroRef = 45;
            UnaFavorita.Alias = alias;
            UnaFavorita.NumeroDeReferencia = nroRef;
            this.ControladorDePruebaUsuario.AgregarFavoritaUsuario(UnaFavorita, UsuarioDePrueba, !ES_DUENIO, !EXISTE_NRO_REF);
        }

        [TestMethod]
        public void EliminarFavoritoUsuario()
        {
            string alias = "Cuenta de jose";
            int nroRef = 45;
            UnaFavorita.Alias = alias;
            UnaFavorita.NumeroDeReferencia = nroRef;

            this.ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);
            this.ControladorDePruebaUsuario.AgregarFavoritaUsuario(UnaFavorita, UsuarioDePrueba, !ES_DUENIO, EXISTE_NRO_REF);
            this.ControladorDePruebaUsuario.EliminarFavoritaUsuario(UnaFavorita, UsuarioDePrueba);

            Assert.IsFalse(UtilidadesBaseDeDatos.ExisteFavorito(UsuarioDePrueba,UnaFavorita));
        }

        [TestMethod]
        public void ObtenerUsuariosEnUnaLista()
        {
            int tamanioInicialNumeroUsuarios = ControladorDePruebaUsuario.ObtenerUsuariosEnUnaLista().Count;
            Usuario otroUsuario = new Usuario();
            otroUsuario.Cedula = 45;
            ControladorDePruebaUsuario.AltaDeUsuario(otroUsuario, otroUsuario.Contrasenia);
            ControladorDePruebaUsuario.AltaDeUsuario(UsuarioDePrueba, UsuarioDePrueba.Contrasenia);

            int resultadoFinalNumeroUsuarios = ControladorDePruebaUsuario.ObtenerUsuariosEnUnaLista().Count;

            Assert.AreEqual(resultadoFinalNumeroUsuarios, tamanioInicialNumeroUsuarios + resultadoFinalNumeroUsuarios);
        }
    }
}
