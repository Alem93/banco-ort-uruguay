﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Logica;
using Excepciones;
using BancoOrtUruguay;

namespace Pruebas
{
    [TestClass]
    public class PruebaControladorMoneda
    {
        private ControladorMoneda ControladorDePruebaMoneda;
        private static Moneda MonedaDolarAmericano = new Moneda() { Nombre = TIPO_DE_MONEDA.USD };
        private static Moneda MonedaPesoUruguayo = new Moneda() { Nombre = TIPO_DE_MONEDA.UYU };

        [TestInitialize]
        public void CargarDatos()
        {
            this.ControladorDePruebaMoneda = new ControladorMoneda();
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AltaDeMonedaCorrecta()
        {
            ControladorDePruebaMoneda.AltaDeMoneda(MonedaPesoUruguayo);
            Assert.IsTrue(ControladorDePruebaMoneda.ExisteMoneda(MonedaPesoUruguayo));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionMonedaExiste))]
        public void AltaDeMonedaErrorYaExiste()
        {
            ControladorDePruebaMoneda.AltaDeMoneda(MonedaDolarAmericano);
            ControladorDePruebaMoneda.AltaDeMoneda(MonedaDolarAmericano);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionMonedaSinNombre))]
        public void AltaDeMonedaErrorSinNombre()
        {
            Moneda monedaVacia = new Moneda();
            ControladorDePruebaMoneda.AltaDeMoneda(monedaVacia);
        }

        [TestMethod]
        public void ObtenerMonedasEnUnArray()
        {
            this.ControladorDePruebaMoneda.AltaDeMoneda(MonedaDolarAmericano);
            int MonedasTamanioInicial = ControladorDePruebaMoneda.ObtenerNumeroMonedasRegistradas();

            Moneda[] monedasArray = ControladorDePruebaMoneda.ObtenerMonedasEnUnArray();
            int monedasTamanioFinal = monedasArray.Length;

            Assert.AreEqual(MonedasTamanioInicial, monedasTamanioFinal);
        }

        [TestMethod]
        public void ObtenerNumeroMonedasRegistradas()
        {
            ControladorDePruebaMoneda.AltaDeMoneda(MonedaDolarAmericano);
            ControladorDePruebaMoneda.AltaDeMoneda(MonedaPesoUruguayo);
            int numeroMonedas = 2;
            int resultadoNumeroMonedas = ControladorDePruebaMoneda.ObtenerNumeroMonedasRegistradas();

            Assert.AreEqual(resultadoNumeroMonedas, numeroMonedas);
        }
    }
}
