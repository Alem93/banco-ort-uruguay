﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Logica;
using BancoOrtUruguay;
using Excepciones;

namespace Pruebas
{
    [TestClass]
    public class PruebaControladorCategoriaGasto
    {
        private ControladorCategoriaGasto ControladorDePruebaCategoriaGasto;
        private CategoriaGasto CategoriaGastoDePrueba;
        private static string NOMBRE_CATEGORIA = "Perfumeria";
        private static CategoriaGasto NOMBRE_SUB_CATEGORIA =new CategoriaGasto {Categoria= "crema" };

        [TestInitialize]
        public void CargarDatos()
        {
            this.ControladorDePruebaCategoriaGasto = new ControladorCategoriaGasto();
            this.CategoriaGastoDePrueba = new CategoriaGasto();
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AltaDeCategoriaGastoCorrecto()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            CategoriaGastoDePrueba.EsPadre = true;
            CategoriaGastoDePrueba.SubCategorias.Add(NOMBRE_SUB_CATEGORIA);

            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);

            Assert.IsTrue(ControladorDePruebaCategoriaGasto.ExisteCategoriaGasto(CategoriaGastoDePrueba));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCategoriaGastoSinNombre))]
        public void AltaDeCategoriaGastoErrorSinNombre()
        {
            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCategoriaGastoNombresIguales))]
        public void AltaDeCategoriaGastoErrorNombresIguales()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);
            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);
        }

        [TestMethod]
        public void AltaSubCategoriaGastoCorrecto()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            CategoriaGastoDePrueba.EsPadre = true;
            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);

            CategoriaGastoDePrueba.EsPadre = false;
            ControladorDePruebaCategoriaGasto.AltaSubCategoriaGasto(CategoriaGastoDePrueba, NOMBRE_SUB_CATEGORIA);

            CategoriaGasto[] catGastos = ControladorDePruebaCategoriaGasto.ObtenerCategoriasDeGastoEnUnaLista().ToArray();
            bool resultadoSubCategoria = catGastos[0].SubCategorias.Contains(NOMBRE_SUB_CATEGORIA);

            Assert.IsTrue(resultadoSubCategoria);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCategoriaGastoSinCategoria))]
        public void AltaSubCategoriaGastoErrorSinCategoria()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            ControladorDePruebaCategoriaGasto.AltaSubCategoriaGasto(CategoriaGastoDePrueba, NOMBRE_SUB_CATEGORIA);
        }

        [TestMethod]
        public void RemoverCategoriaGasto()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            CategoriaGastoDePrueba.SubCategorias.Add(NOMBRE_SUB_CATEGORIA);

            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);
            ControladorDePruebaCategoriaGasto.RemoverCategoriaGasto(CategoriaGastoDePrueba);

           Assert.IsFalse(ControladorDePruebaCategoriaGasto.ExisteCategoriaGasto(CategoriaGastoDePrueba));
        }

        [TestMethod]
        public void RemoverSubCategoriaGastoCorrecto()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            CategoriaGastoDePrueba.EsPadre = true;
            CategoriaGastoDePrueba.SubCategorias.Add(NOMBRE_SUB_CATEGORIA);

            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);
            CategoriaGastoDePrueba.EsPadre = false;
            ControladorDePruebaCategoriaGasto.RemoverSubCategoriaGasto(CategoriaGastoDePrueba,NOMBRE_SUB_CATEGORIA);

            CategoriaGasto[] catGastos = ControladorDePruebaCategoriaGasto.ObtenerCategoriasDeGastoEnUnaLista().ToArray();
            bool resultadoSubCategoria = catGastos[0].SubCategorias.Contains(NOMBRE_SUB_CATEGORIA);

            Assert.IsFalse(resultadoSubCategoria);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCategoriaGastoSinCategoria))]
        public void RemoverSubCategoriaGastoErrorSinCategoria()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            CategoriaGastoDePrueba.SubCategorias.Add(NOMBRE_SUB_CATEGORIA);
            ControladorDePruebaCategoriaGasto.RemoverSubCategoriaGasto(CategoriaGastoDePrueba, NOMBRE_SUB_CATEGORIA);
        }

        [TestMethod]
        public void ObtenerNumeroDeCategoriasRegistradas()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);

            CategoriaGasto nuevaCategoria = new CategoriaGasto();
            nuevaCategoria.Categoria = "OtraCategoria";
            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(nuevaCategoria);

            int numeroCategorias = 2;
            int resultadoNumeroCategorias = ControladorDePruebaCategoriaGasto.ObtenerNumeroDeCategoriasRegistradas();

            Assert.AreEqual(resultadoNumeroCategorias, numeroCategorias);
        }

        [TestMethod]
        public void ExisteCategoriaGasto()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);

            Assert.IsTrue(ControladorDePruebaCategoriaGasto.ExisteCategoriaGasto(CategoriaGastoDePrueba));
        }

        [TestMethod]
        public void ObtenerCategoriasDeGastoEnUnaLista()
        {
            CategoriaGastoDePrueba.Categoria = NOMBRE_CATEGORIA;
            CategoriaGastoDePrueba.EsPadre = true;
            ControladorDePruebaCategoriaGasto.AltaDeCategoriaGasto(CategoriaGastoDePrueba);

            int resultadoCategoriaGasto = ControladorDePruebaCategoriaGasto.ObtenerCategoriasDeGastoEnUnaLista().Count;
            int tamanioEsperado = 1;

            Assert.AreEqual(resultadoCategoriaGasto,tamanioEsperado);
        }
    }
}
