﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Logica;
using BancoOrtUruguay;
using Excepciones;

namespace Pruebas
{
    [TestClass]
    public class PruebaControladorTransaccion
    {
        ControladorTransaccion ControladorDePruebaTransaccion;

        Transaccion TransaccionDePrueba;
        Transaccion TransaccionInternacionalDePrueba;

        private Moneda UnaMonedaUSD;
        private Moneda UnaMonedaUYU;

        private CategoriaGasto UnaCategoriaDeGasto;

        private static Usuario UsuarioDuenio;

        private Cuenta PrimeraCuenta;
        private Cuenta SegundaCuenta;

        private static long UnNumeroSwift = 273849294038;
        private static long UnNumeroCuentaDestino = 34234234234324;
        public static string UnNombreDestinatario = "pepe";

        [TestInitialize]
        public void CargarDatos()
        {
            UnaMonedaUSD = new Moneda();
            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaUSD,TIPO_DE_MONEDA.USD);
            UnaMonedaUYU = new Moneda();
            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaUYU,TIPO_DE_MONEDA.UYU);

            UnaCategoriaDeGasto = new CategoriaGasto();
            UtilidadesBaseDeDatos.AltaDeCategoriaDeGasto(UnaCategoriaDeGasto);

            UsuarioDuenio = new Usuario();
            UtilidadesBaseDeDatos.AltaDeUsurioSinRol(UsuarioDuenio);
            
            this.PrimeraCuenta = new Cuenta();
            UtilidadesBaseDeDatos.AltaDeCuenta(PrimeraCuenta,UnaMonedaUSD,UsuarioDuenio);
            this.SegundaCuenta = new Cuenta();
            UtilidadesBaseDeDatos.AltaDeCuenta(SegundaCuenta, UnaMonedaUSD, UsuarioDuenio);

            this.TransaccionDePrueba = new Transaccion();
            this.TransaccionInternacionalDePrueba = new TransaccionInternacional();

            this.ControladorDePruebaTransaccion = new ControladorTransaccion();
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void TransaccionSuspensionDecuenta()
        {
            int cantidadATransferir = 600;
            TransaccionDePrueba.CantidadDebitada = cantidadATransferir;
            TransaccionDePrueba.CuentaOrigen = PrimeraCuenta;
            TransaccionDePrueba.CuentaDestino = SegundaCuenta;
            TransaccionDePrueba.CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;

            ControladorDePruebaTransaccion.TransaccionDentroDelBanco(TransaccionDePrueba);
            ESTADO_SOLICITUD_CUENTA estadoDeLaCuenta = TransaccionDePrueba.CuentaOrigen.EstadoCuenta;
            ESTADO_SOLICITUD_CUENTA resultadoEsperado = ESTADO_SOLICITUD_CUENTA.SUSPENDIDA;

            Assert.AreEqual(estadoDeLaCuenta, resultadoEsperado);
        }

        [TestMethod]
        public void TransaccionDentroDelBancoCorrectoChequearBalanceCuentaDestino()
        {
            int cantidadATransferir = 50;
            double resultadoEsperado = SegundaCuenta.Balance + cantidadATransferir;

            TransaccionDePrueba.CantidadDebitada = cantidadATransferir;
            TransaccionDePrueba.CuentaOrigen = PrimeraCuenta;
            TransaccionDePrueba.CuentaDestino = SegundaCuenta;
            TransaccionDePrueba.CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;

            ControladorDePruebaTransaccion.TransaccionDentroDelBanco(TransaccionDePrueba);
            double resultadoTransaccion = TransaccionDePrueba.CuentaDestino.Balance;

            Assert.AreEqual(resultadoTransaccion, resultadoEsperado);
        }

        [TestMethod]
        public void TransaccionDentroDelBancoCorrectoChequearBalanceCuentaOrigen()
        {
            double balanceInicialOrigen = PrimeraCuenta.Balance;

            int cantidadATransferir = 50;
            TransaccionDePrueba.CantidadDebitada = cantidadATransferir;
            TransaccionDePrueba.CuentaOrigen = PrimeraCuenta;
            TransaccionDePrueba.CuentaDestino = SegundaCuenta;
            TransaccionDePrueba.CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;

            ControladorDePruebaTransaccion.TransaccionDentroDelBanco(TransaccionDePrueba);
            double resultadoBalanceFinalOrigen = PrimeraCuenta.Balance;

            Assert.AreEqual(resultadoBalanceFinalOrigen + cantidadATransferir, balanceInicialOrigen);
        }

        [TestMethod]
        public void TransaccionFueraDelBancoCorrecto()
        {
            double balanceInicialOrigen = PrimeraCuenta.Balance;

            int cantidadATransferir = 50;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CantidadDebitada = cantidadATransferir;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CuentaOrigen = PrimeraCuenta;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroSwift = UnNumeroSwift;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroCuentaDestino = UnNumeroCuentaDestino;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NombreDestinatario = UnNombreDestinatario;

            ControladorDePruebaTransaccion.TransaccionFueraDelBanco(((TransaccionInternacional)TransaccionInternacionalDePrueba));
            double resultadoBalanceFinalOrigen = PrimeraCuenta.Balance;

            Assert.AreEqual(resultadoBalanceFinalOrigen + cantidadATransferir, balanceInicialOrigen);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionInternacionalNroSwiftInvalido))]
        public void TransaccionFueraDelBancoErrorNroSwiftInvalido()
        {

            int cantidadATransferir = 50;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CantidadDebitada = cantidadATransferir;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CuentaOrigen = PrimeraCuenta;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NombreDestinatario = UnNombreDestinatario;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroCuentaDestino = UnNumeroCuentaDestino;

            ControladorDePruebaTransaccion.TransaccionFueraDelBanco(((TransaccionInternacional)TransaccionInternacionalDePrueba));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionInternacionalSinNombreDestinatario))]
        public void TransaccionFueraDelBancoErrorSinNombreDestinatario()
        {

            int cantidadATransferir = 50;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CantidadDebitada = cantidadATransferir;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CuentaOrigen = PrimeraCuenta;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroSwift = UnNumeroSwift;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroCuentaDestino = UnNumeroCuentaDestino;

            ControladorDePruebaTransaccion.TransaccionFueraDelBanco(((TransaccionInternacional)TransaccionInternacionalDePrueba));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionInternacionalSinNroCuentaDestino))]
        public void TransaccionFueraDelBancoErrorSinNroCuentaDestino()
        {

            int cantidadATransferir = 50;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CantidadDebitada = cantidadATransferir;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CuentaOrigen = PrimeraCuenta;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroSwift = UnNumeroSwift;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NombreDestinatario = UnNombreDestinatario;

            ControladorDePruebaTransaccion.TransaccionFueraDelBanco(((TransaccionInternacional)TransaccionInternacionalDePrueba));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionSinCuentaOrigen))]
        public void TransaccionFueraDelBancoErrorSinCuentaOrigen()
        {
            int cantidadATransferir = 50;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CantidadDebitada = cantidadATransferir;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroSwift = UnNumeroSwift;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroCuentaDestino = UnNumeroCuentaDestino;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NombreDestinatario = UnNombreDestinatario;

            ControladorDePruebaTransaccion.TransaccionFueraDelBanco(((TransaccionInternacional)TransaccionInternacionalDePrueba));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionSinCantidadATransferir))]
        public void TransaccionFueraDelBancoErrorSinCantidadATransferir()
        {

            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CuentaOrigen = PrimeraCuenta;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroSwift = UnNumeroSwift;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroCuentaDestino = UnNumeroCuentaDestino;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NombreDestinatario = UnNombreDestinatario;

            ControladorDePruebaTransaccion.TransaccionFueraDelBanco(((TransaccionInternacional)TransaccionInternacionalDePrueba));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionSinCategoriaGasto))]
        public void TransaccionFueraDelBancoErrorSinCategoriaGasto()
        {

            int cantidadATransferir = 50;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CuentaOrigen = PrimeraCuenta;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).CantidadDebitada = cantidadATransferir;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroSwift = UnNumeroSwift;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NumeroCuentaDestino = UnNumeroCuentaDestino;
            ((TransaccionInternacional)TransaccionInternacionalDePrueba).NombreDestinatario = UnNombreDestinatario;

            ControladorDePruebaTransaccion.TransaccionFueraDelBanco(((TransaccionInternacional)TransaccionInternacionalDePrueba));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionSinCuentaOrigen))]
        public void TransaccionDentroDelBancoErrorSinCuentaOrigen()
        {

            int cantidadATransferir = 50;
            TransaccionDePrueba.CantidadDebitada = cantidadATransferir;
            TransaccionDePrueba.CuentaDestino = SegundaCuenta;
            TransaccionDePrueba.CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;

            ControladorDePruebaTransaccion.TransaccionDentroDelBanco(TransaccionDePrueba);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionSinCuentaDestino))]
        public void TransaccionDentroDelBancoErrorSinCuentaDestino()
        {

            int cantidadATransferir = 50;
            TransaccionDePrueba.CantidadDebitada = cantidadATransferir;
            TransaccionDePrueba.CuentaOrigen = PrimeraCuenta;
            TransaccionDePrueba.CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;

            ControladorDePruebaTransaccion.TransaccionDentroDelBanco(TransaccionDePrueba);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionSinCantidadATransferir))]
        public void TransaccionDentroDelBancoErrorSinCantidadATransferir()
        {

            TransaccionDePrueba.CuentaOrigen = PrimeraCuenta;
            TransaccionDePrueba.CuentaDestino = SegundaCuenta;
            TransaccionDePrueba.CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;

            ControladorDePruebaTransaccion.TransaccionDentroDelBanco(TransaccionDePrueba);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionTransaccionSinCategoriaGasto))]
        public void TransaccionDentroDelBancoErrorSinCategoriaGasto()
        {
            PrimeraCuenta.Balance = 450;
            PrimeraCuenta.NumeroDeReferencia = 45;
            PrimeraCuenta.UnaMoneda = UnaMonedaUYU;
            PrimeraCuenta.UnCliente = UsuarioDuenio;

            SegundaCuenta.Balance = 0;
            SegundaCuenta.NumeroDeReferencia = 45;
            SegundaCuenta.UnaMoneda = UnaMonedaUYU;
            SegundaCuenta.UnCliente = UsuarioDuenio;

            int cantidadATransferir = 50;
            TransaccionDePrueba.CuentaOrigen = PrimeraCuenta;
            TransaccionDePrueba.CuentaDestino = SegundaCuenta;
            TransaccionDePrueba.CantidadDebitada = cantidadATransferir;

            ControladorDePruebaTransaccion.TransaccionDentroDelBanco(TransaccionDePrueba);
        }

        [TestMethod]
        public void ObtenerTransaccionesEnUnaLista()
        {
            int tamanioInicialNumeroTransacciones = ControladorDePruebaTransaccion.ObtenerTransaccionesEnUnaLista().Count;

            int cantidadATransferir = 50;
            TransaccionDePrueba.CantidadDebitada = cantidadATransferir;
            TransaccionDePrueba.CuentaOrigen = PrimeraCuenta;
            TransaccionDePrueba.CuentaDestino = SegundaCuenta;
            TransaccionDePrueba.CategoriaDeGasto = UnaCategoriaDeGasto.Categoria;

            ControladorDePruebaTransaccion.TransaccionDentroDelBanco(TransaccionDePrueba);
            ControladorDePruebaTransaccion.TransaccionDentroDelBanco(TransaccionDePrueba);
            int resultadoFinalNumeroTransacciones = ControladorDePruebaTransaccion.ObtenerTransaccionesEnUnaLista().Count;

            Assert.AreEqual(resultadoFinalNumeroTransacciones, tamanioInicialNumeroTransacciones + resultadoFinalNumeroTransacciones);
        }
    }
}
