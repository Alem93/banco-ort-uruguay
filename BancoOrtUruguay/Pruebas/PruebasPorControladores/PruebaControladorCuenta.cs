﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BancoOrtUruguay;
using Logica;
using Excepciones;

namespace Pruebas
{
    [TestClass]
    public class PruebaControladorCuenta
    {
        private Cuenta CuentaCajaDeAhorros;
        private Cuenta CuentaCorriente;
        private Moneda UnaMonedaEnUSD;
        private Usuario UnUsuario;
        private ControladorCuenta ControladorDePruebaCuenta;

        [TestInitialize]
        public void CargarDatos()
        {
            UnUsuario = new Usuario();
            UtilidadesBaseDeDatos.AltaDeUsurioSinRol(UnUsuario);

            UnaMonedaEnUSD = new Moneda();
            UtilidadesBaseDeDatos.AltaDeMonedaSegunTipoDeMoneda(UnaMonedaEnUSD, TIPO_DE_MONEDA.USD);

            CuentaCajaDeAhorros = new Cuenta();
            CuentaCajaDeAhorros.TipoDeCuenta = TIPO_DE_CUENTA.CAJA_DE_AHORRO;

            CuentaCorriente = new Cuenta();
            CuentaCorriente.TipoDeCuenta = TIPO_DE_CUENTA.CUENTA_CORRIENTE;

            this.ControladorDePruebaCuenta = new ControladorCuenta();
        }

        [TestCleanup]
        public void VaciarTabla()
        {
            UtilidadesBaseDeDatos.VaciarTablas();
        }

        [TestMethod]
        public void AprobarAperturaCuentaAceptado()
        {
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            ControladorDePruebaCuenta.SolicitudAperturaCuentaBancaria(CuentaCajaDeAhorros);
            ControladorDePruebaCuenta.AprobarAperturaCuenta(ref CuentaCajaDeAhorros, ESTADO_SOLICITUD_CUENTA.ACEPTADA);
            Cuenta unaCuentaResultado = CuentaCajaDeAhorros;

            Assert.AreEqual(unaCuentaResultado.EstadoCuenta, ESTADO_SOLICITUD_CUENTA.ACEPTADA);
        }

        [TestMethod]
        public void AprobarAperturaCuentaRechazado()
        {
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            ControladorDePruebaCuenta.SolicitudAperturaCuentaBancaria(CuentaCajaDeAhorros);
            ControladorDePruebaCuenta.AprobarAperturaCuenta(ref CuentaCajaDeAhorros, ESTADO_SOLICITUD_CUENTA.RECHAZADA);

            Assert.IsFalse(ControladorDePruebaCuenta.ExisteCuenta(CuentaCajaDeAhorros));
        }

        [TestMethod]
        public void SolicitudApertuaCuentaBancariaCorrectoDatosBasicos()
        {
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            //Cuando es una solicitud de cuenta,
            //el sistema asigna como usuario a la cuenta el usuarioLogueado
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            ControladorDePruebaCuenta.SolicitudAperturaCuentaBancaria(CuentaCajaDeAhorros);

            Assert.IsTrue(ControladorDePruebaCuenta.ExisteCuenta(CuentaCajaDeAhorros));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCuentaSinMoneda))]
        public void SolicitudApertuaCuentaBancariaErrorMonedaNoAsignada()
        {
            CuentaCajaDeAhorros.UnCliente = UnUsuario;

            ControladorDePruebaCuenta.SolicitudAperturaCuentaBancaria(CuentaCorriente);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCuentaSinTipoDeCuenta))]
        public void SolicitudApertuaCuentaBancariaErrorSinTipoDeCuentaAsignado()
        {
            Cuenta cuentaSinTipo = new Cuenta();
            cuentaSinTipo.UnCliente = UnUsuario;
            cuentaSinTipo.UnaMoneda = UnaMonedaEnUSD;

            ControladorDePruebaCuenta.SolicitudAperturaCuentaBancaria(cuentaSinTipo);
        }

        [TestMethod]
        public void AltaCuentaBancariaCorrectoDatosBasicos()
        {
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);

            Assert.IsTrue(ControladorDePruebaCuenta.ExisteCuenta(CuentaCajaDeAhorros));
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCuentaSinMoneda))]
        public void AltaCuentaBancariaErrorMonedaNoAsignada()
        {
            CuentaCajaDeAhorros.UnCliente = UnUsuario;

            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCorriente);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCuentaSinTipoDeCuenta))]
        public void AltaCuentaBancariaErrorSinTipoDeCuentaAsignado()
        {
            Cuenta cuentaSinTipo = new Cuenta();
            cuentaSinTipo.UnCliente = UnUsuario;
            cuentaSinTipo.UnaMoneda = UnaMonedaEnUSD;

            ControladorDePruebaCuenta.AltaCuentaBancaria(cuentaSinTipo);
        }

        [TestMethod]
        [ExpectedException(typeof(ExcepcionCuentaSinUsuarioAsignado))]
        public void AltaCuentaBancariaErrorSinUsuarioAsignado()
        {
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;

            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);
        }

        [TestMethod]
        public void CambiarSaldoMinimo()
        {
            double saldoMinimoNuevo = -100;
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);
            
            ControladorDePruebaCuenta.CambiarSaldoMinimo(CuentaCajaDeAhorros, saldoMinimoNuevo);
            double saldoMinimoActual = CuentaCajaDeAhorros.MontoMinimo;


            Assert.AreEqual(saldoMinimoActual,saldoMinimoNuevo);
        }

        //[TestMethod]
        //public void AsignarNumeroDeReferenciaCorrecto()
        //{
        //    CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
        //    CuentaCajaDeAhorros.UnCliente = UnUsuario;

        //    CuentaCorriente.UnaMoneda = UnaMonedaEnUSD;
        //    CuentaCorriente.UnCliente = UnUsuario;

        //    ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);
        //    ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCorriente);

        //    Assert.AreNotEqual(CuentaCorriente, CuentaCajaDeAhorros);
        //}

        [TestMethod]
        public void ExisteCuenta()
        {
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);

            Assert.IsTrue(ControladorDePruebaCuenta.ExisteCuenta(CuentaCajaDeAhorros));
        }

        [TestMethod]
        public void EsDuenioDeCuenta()
        {
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);

            Assert.IsTrue(ControladorDePruebaCuenta.EsDuenioDeCuenta(CuentaCajaDeAhorros, UnUsuario));
        }

        [TestMethod]
        public void ObtenerCuentasDeUsuarioEnUnArray()
        {
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);
            int CuentasDeUsuarioTamanioInicial = ControladorDePruebaCuenta.ObtenerNumeroDeCuentasRegistradas();

            Cuenta[] cuentasUsuarioArray = ControladorDePruebaCuenta.ObtenerCuentasDeUsuarioEnUnArray(UnUsuario);
            int CuentasDeUsuarioTamanioFinal = cuentasUsuarioArray.Length;

            Assert.AreEqual(CuentasDeUsuarioTamanioInicial, CuentasDeUsuarioTamanioFinal);
        }

        [TestMethod]
        public void ObtenerCuentasPendientesEnUnArray()
        {
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            ControladorDePruebaCuenta.SolicitudAperturaCuentaBancaria(CuentaCajaDeAhorros);
            int CuentasPendientesTamanioInicial = ControladorDePruebaCuenta.ObtenerNumeroDeCuentasRegistradas();

            Cuenta[] cuentasPendientesArray = ControladorDePruebaCuenta.ObtenerCuentasPendientesEnUnArray();
            int CuentasPendientesTamanioFinal = cuentasPendientesArray.Length;

            Assert.AreEqual(CuentasPendientesTamanioInicial, CuentasPendientesTamanioFinal);
        }

        [TestMethod]
        public void ObtenerCuentasAceptadasEnUnArray()
        {
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);
            int CuentasAceptadasTamanioInicial = ControladorDePruebaCuenta.ObtenerNumeroDeCuentasRegistradas();

            Cuenta[] cuentasAceptadasArray = ControladorDePruebaCuenta.ObtenerCuentasAceptadasEnUnArray();
            int CuentasAceptadasTamanioFinal = cuentasAceptadasArray.Length;

            Assert.AreEqual(CuentasAceptadasTamanioInicial, CuentasAceptadasTamanioFinal);
        }

        //[TestMethod]
        //public void ObtenerCuentaPorNumeroDeRefencia()
        //{
        //    CuentaCajaDeAhorros.UnCliente = UnUsuario;
        //    CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
        //    ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);

        //    //A la primera cuenta se le asigna 1 como nroRef
        //    int nroRefPorDefecto = 1;
        //    Cuenta resultadoCuenta = ControladorDePruebaCuenta.ObtenerCuentaPorNumeroDeRefencia(nroRefPorDefecto);

        //    Assert.AreEqual(resultadoCuenta, CuentaCajaDeAhorros);
        //}

        [TestMethod]
        public void ObtenerNumeroDeCuentasRegistradas()
        {
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);

            CuentaCorriente.UnCliente = UnUsuario;
            CuentaCorriente.UnaMoneda = UnaMonedaEnUSD;
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCorriente);

            int numeroCuentas = 2;
            int resultadoNumeroCuentas = ControladorDePruebaCuenta.ObtenerNumeroDeCuentasRegistradas();

            Assert.AreEqual(resultadoNumeroCuentas, numeroCuentas);
        }

        [TestMethod]
        public void ObtenerCuentasEnUnaLista()
        {
            int tamanioInicialNumeroCuentas = ControladorDePruebaCuenta.ObtenerCuentasEnUnaLista().Count;

            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);
            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCajaDeAhorros);

            int resultadoFinalNumeroCuentas = ControladorDePruebaCuenta.ObtenerCuentasEnUnaLista().Count;

            Assert.AreEqual(resultadoFinalNumeroCuentas, tamanioInicialNumeroCuentas + resultadoFinalNumeroCuentas);
        }

        [TestMethod]
        public void ObtenerCuentasActivasDeUsuarioEnUnArray()
        {
            CuentaCajaDeAhorros.UnCliente = UnUsuario;
            CuentaCajaDeAhorros.UnaMoneda = UnaMonedaEnUSD;
            CuentaCajaDeAhorros.EstadoCuenta = ESTADO_SOLICITUD_CUENTA.PENDIENTE;

            CuentaCorriente.UnCliente = UnUsuario;
            CuentaCorriente.UnaMoneda = UnaMonedaEnUSD;
            CuentaCorriente.EstadoCuenta = ESTADO_SOLICITUD_CUENTA.ACEPTADA;

            ControladorDePruebaCuenta.AltaCuentaBancaria(CuentaCorriente);
            ControladorDePruebaCuenta.SolicitudAperturaCuentaBancaria(CuentaCajaDeAhorros);

            int ResultadoEsperado = 1;

            Cuenta[] cuentasActivasUsuarioArray = ControladorDePruebaCuenta.ObtenerCuentasActivasDeUsuarioEnUnArray(UnUsuario);
            int CuentasDeUsuarioActivasTamanioFinal = cuentasActivasUsuarioArray.Length;

            Assert.AreEqual(ResultadoEsperado, CuentasDeUsuarioActivasTamanioFinal);
        }
    }
}
