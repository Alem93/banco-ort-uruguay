﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class TipoCambio
    {
        public int Id { get; set; }
        public Moneda Origen { get; set; }
        public Moneda Destino { get; set; }
        public DateTime Fecha { get; set; }
        public double Compra { get; set; }

        public TipoCambio() {
            this.Id = ConstantesGenerales.IdPorDefecto;
            this.Origen = new Moneda();
            this.Destino = new Moneda();
            this.Fecha = DateTime.Now;
            this.Compra = 0;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(this.Origen);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.Destino);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.Compra);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.Fecha);
            stringBuilder.Append(ConstantesGenerales.Separador);

            return stringBuilder.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            TipoCambio unaTipoDeCambio = (TipoCambio)obj;
            return unaTipoDeCambio.Fecha.Equals(this.Fecha);
        }
    }
}
