﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class TransaccionInternacional:Transaccion
    {
        public long NumeroSwift { get; set; }
        public long NumeroCuentaDestino { get; set; }
        public string NombreDestinatario { get; set; }

        public TransaccionInternacional():base()
        {
            this.NumeroSwift = 0;
            this.NumeroCuentaDestino = 0;
            this.NombreDestinatario = string.Empty;
            this.CuentaDestino = null;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(base.ToString());
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.NumeroSwift);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.NumeroCuentaDestino);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.NombreDestinatario);
            stringBuilder.Append(ConstantesGenerales.Separador);

            return stringBuilder.ToString();
        }

        public override bool Equals(object obj)
        {
            bool equals = false;
            if (obj != null && obj.GetType().Equals(this.GetType()))
            {
                TransaccionInternacional transaccionInternacional = (TransaccionInternacional)obj;
                equals = this.NumeroSwift.Equals(transaccionInternacional.NumeroSwift) &&
                    this.NumeroCuentaDestino.Equals(transaccionInternacional.NumeroCuentaDestino) &&
                    this.CantidadDebitada.Equals(transaccionInternacional.CantidadDebitada) &&
                    this.CuentaOrigen.Equals(transaccionInternacional.CuentaOrigen);
            }
            return equals;
        }
    }
}
