﻿using BancoOrtUruguay.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Cedula { get; set; }
        public string Email { get; set; }
        public string Contrasenia { get; set; }
        public List<RolUsuario> RolesDeUsuario { get; set; }
        public List<Mensaje> BuzonDeMensajes { get; set; }
        public List<Favorita> Favoritas { get; set; }

        public Usuario()
        {
            this.Id = ConstantesGenerales.IdPorDefecto;
            this.Nombre = "Usuario sin nombre";
            this.Cedula = 0;
            this.Email = "Usuario sin EMail";
            this.Contrasenia = String.Empty;
            this.RolesDeUsuario = new List<RolUsuario>();
            this.BuzonDeMensajes = new List<Mensaje>();
            this.Favoritas = new List<Favorita>();
        }


        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(this.Nombre);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.Cedula);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.Email);
            stringBuilder.Append(ConstantesGenerales.Separador);

            return stringBuilder.ToString();
        }

        public override bool Equals(object obj)
        {
            bool equals = false;
            if (obj != null && obj.GetType().Equals(this.GetType()))
            {
                Usuario usuario = (Usuario)obj;
                equals = this.Cedula.Equals(usuario.Cedula);
            }
            return equals;
        }
    }
}
