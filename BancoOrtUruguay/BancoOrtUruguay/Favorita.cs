﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class Favorita
    {
        public int Id { get; set; }
        public int NumeroDeReferencia { get; set; }
        public string Alias { get; set; }

        public Favorita()
        {
            this.Id = ConstantesGenerales.IdPorDefecto;
            this.Alias = string.Empty;
            this.NumeroDeReferencia = 0;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(this.Alias);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.NumeroDeReferencia);
            stringBuilder.Append(ConstantesGenerales.Separador);

            return stringBuilder.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            Favorita unNumeroRef = (Favorita)obj;
            return unNumeroRef.NumeroDeReferencia.Equals(this.NumeroDeReferencia);
        }
    }
}
