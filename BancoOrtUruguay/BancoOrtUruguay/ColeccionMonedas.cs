﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class ColeccionMonedas
    {
        private List<Moneda> Monedas;

        public ColeccionMonedas()
        {
            Monedas = new List<Moneda>();
        }

        public void AgregarMoneda(Moneda unaMoneda)
        {
            this.Monedas.Add(unaMoneda);
        }

        public int ObtenerNumeroDeMonedasRegistradas()
        {
            return Monedas.Count;
        }

        public bool ExisteMoneda(Moneda unaMoneda)
        {
            return this.Monedas.Any(moneda => unaMoneda.Equals(moneda));
        }

        public Moneda[] ObtenerColeccionEnUnArray()
        {
            return this.Monedas.ToArray();
        }
    }
}
