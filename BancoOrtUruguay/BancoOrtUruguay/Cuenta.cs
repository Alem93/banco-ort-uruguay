﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class Cuenta
    {
        public int Id { get; set; }
        public Moneda UnaMoneda { get; set; }
        public Usuario UnCliente { get; set; }
        public double Balance { get; set; }
        public double MontoMinimo { get; set; }
        public int NumeroDeReferencia { get; set; }
        public string Descripcion { get; set; }
        public TIPO_DE_CUENTA TipoDeCuenta{ get; set; }
        public ESTADO_SOLICITUD_CUENTA EstadoCuenta{ get; set; }

        public Cuenta() {
            this.Id = ConstantesGenerales.IdPorDefecto;
            this.UnaMoneda = new Moneda();
            this.UnCliente = new Usuario();
            this.Balance = 0;
            this.MontoMinimo = 0;
            this.NumeroDeReferencia = 0;
            this.Descripcion = string.Empty;
            this.TipoDeCuenta = TIPO_DE_CUENTA.SIN_TIPO;
            this.EstadoCuenta = ESTADO_SOLICITUD_CUENTA.PENDIENTE;

        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(this.NumeroDeReferencia);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.UnCliente.Cedula);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.UnaMoneda.ToString());
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.Balance);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.MontoMinimo);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.EstadoCuenta);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.Descripcion);
            stringBuilder.Append(ConstantesGenerales.Separador);

            return stringBuilder.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            Cuenta unaCuenta = (Cuenta)obj;
            return unaCuenta.Id.Equals(this.Id);
        }
    }
}
