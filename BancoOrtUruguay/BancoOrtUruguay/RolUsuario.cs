﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class RolUsuario
    {
        public int Id { get; set; }
        public TIPO_DE_USUARIO NombreRol { get; set; }

        public RolUsuario()
        {
            this.Id = ConstantesGenerales.IdPorDefecto;
            this.NombreRol = TIPO_DE_USUARIO.SIN_ROL;
        }

        public override string ToString()
        {
            return this.NombreRol.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            RolUsuario rolUsuario = (RolUsuario)obj;
            return rolUsuario.NombreRol.Equals(this.NombreRol);
        }
    }
}
