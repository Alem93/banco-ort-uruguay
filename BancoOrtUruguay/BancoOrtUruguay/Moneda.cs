﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
   public class Moneda
    {
        public int Id { get; set; }
        public TIPO_DE_MONEDA Nombre { get; set; }

        public Moneda() {
            this.Id = ConstantesGenerales.IdPorDefecto;
            this.Nombre = TIPO_DE_MONEDA.SIN_TIPO;
        }

        public override string ToString()
        {
            return this.Nombre.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            Moneda unaMoneda = (Moneda)obj;
            return unaMoneda.Nombre.Equals(this.Nombre);
        }
    }
}
