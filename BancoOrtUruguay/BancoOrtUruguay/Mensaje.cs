﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class Mensaje
    {
        public int Id { get; set; }
        public string unMensaje { get; set; }

        public Mensaje()
        {
            this.Id = ConstantesGenerales.IdPorDefecto;
            this.unMensaje = string.Empty;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(this.unMensaje);
            stringBuilder.Append(ConstantesGenerales.Separador);

            return stringBuilder.ToString();
        }

        public override bool Equals(object obj)
        {
            bool equals = false;
            if (obj != null && obj.GetType().Equals(this.GetType()))
            {
                Mensaje mensaje = (Mensaje)obj;
                equals = this.unMensaje.Equals(mensaje.unMensaje);
            }
            return equals;
        }
    }
}
