﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class ColeccionTipoCambio
    {
        private List<TipoCambio> TiposDeCambio;

        public ColeccionTipoCambio()
        {
            this.TiposDeCambio = new List<TipoCambio>();
        }


        public void AgregarTipoDeCambio(TipoCambio unTipoDeCambio)
        {
            this.TiposDeCambio.Add(unTipoDeCambio);
        }

        public TipoCambio ObtenerTipoDeCambioMasReciente(Moneda unaMonedaOrigen, Moneda unaMonedaDestino)
        {
            TipoCambio tipoDeCambio = TiposDeCambio.FindLast(
                aValue => aValue.Origen.Equals(unaMonedaOrigen) && aValue.Destino.Equals(unaMonedaDestino) ||
            aValue.Origen.Equals(unaMonedaDestino) && aValue.Destino.Equals(unaMonedaOrigen));

            return tipoDeCambio;
        }

        public int ObtenerNumeroDeTipoCambioRegistrados()
        {
            return TiposDeCambio.Count;
        }

        public bool ExisteTipoCambio(TipoCambio tipoCambioBuscado)
        {
            return this.TiposDeCambio.Any(unTipoCambio => tipoCambioBuscado.Equals(unTipoCambio)); ;
        }
    }
}
