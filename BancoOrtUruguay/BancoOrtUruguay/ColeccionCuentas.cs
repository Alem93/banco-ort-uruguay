﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class ColeccionCuentas
    {
        private List<Cuenta> Cuentas;

        public ColeccionCuentas()
        {
            this.Cuentas = new List<Cuenta>();
        }


        public void AgregarCuenta(Cuenta unaCuenta)
        {
            this.Cuentas.Add(unaCuenta);
        }

        public void RemoverCuenta(Cuenta unaCuenta)
        {
            this.Cuentas.Remove(unaCuenta);
        }

        public bool ExisteCuenta(Cuenta unaCuenta)
        {
            return this.Cuentas.Any(cuenta => unaCuenta.Equals(cuenta));
        }

        public int ObtenerUltimoNumeroReferencia()
        {
            Cuenta ultimaCuenta = this.Cuentas.Last<Cuenta>();
            return ultimaCuenta.NumeroDeReferencia;
        }

        public int ObtenerNumeroDeCuentasRegistradas()
        {
            return this.Cuentas.Count;
        }

        public Cuenta[] ObtenerColeccionDeUsuarioEnUnArray(Usuario unUsuario)
        {
            List<Cuenta> cuentasUsuario = this.Cuentas.FindAll(unaCuenta => unaCuenta.UnCliente.Equals(unUsuario));
            return cuentasUsuario.ToArray();
        }

        public Cuenta[] ObtenerColeccionSegunEstadoEnUnArray(ESTADO_SOLICITUD_CUENTA estadoCuenta)
        {
            List<Cuenta> estadoCuentas = this.Cuentas.FindAll(unaCuenta => unaCuenta.EstadoCuenta.Equals(estadoCuenta));
            return estadoCuentas.ToArray();
        }

        public Cuenta ObtenerCuentaPorNumeroDeRefencia(int nroRef)
        {
            Cuenta nroRefCuentas = this.Cuentas.Find(unaCuenta => unaCuenta.NumeroDeReferencia.Equals(nroRef));
            return nroRefCuentas;
        }

        public List<Cuenta> ObtenerColeccionCuentasEnUnaLista()
        {
            return this.Cuentas;
        }
    }
}
