﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class ColeccionCategoriaGastos
    {
        private List<CategoriaGasto> CategoriaGastos;

        public ColeccionCategoriaGastos()
        {
            this.CategoriaGastos = new List<CategoriaGasto>();
        }


        public void AgregarCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {
            this.CategoriaGastos.Add(unaCategoriaGasto);
        }

        public void RemoverCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {
            this.CategoriaGastos.Remove(unaCategoriaGasto);
        }


        public void AgregarSubCategoriaGasto(CategoriaGasto unaCategoriaGasto, string subcategoria)
        {
            CategoriaGasto categoria = this.CategoriaGastos.Find(unaCategoria => unaCategoria.Equals(unaCategoriaGasto));
            categoria.SubCategorias.Add(subcategoria);
        }

        public void RemoverSubCategoriaGasto(CategoriaGasto unaCategoriaGasto, string subcategoria)
        {
            CategoriaGasto categoria = this.CategoriaGastos.Find(unaCategoria => unaCategoria.Equals(unaCategoriaGasto));
            categoria.SubCategorias.Remove(subcategoria);
        }

        public bool ExisteCategoriaGasto(CategoriaGasto unaCategoriaGasto)
        {
            return this.CategoriaGastos.Any(categoriaGasto => unaCategoriaGasto.Equals(categoriaGasto));
        }

        public int ObtenerNumeroDeCategoriasRegistradas()
        {
            return this.CategoriaGastos.Count;
        }

        public List<CategoriaGasto> ObtenerColeccionDeCategoriasDeGastoEnUnaLista()
        {
            return this.CategoriaGastos;
        }
    }
}
