﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class CategoriaGasto
    {
        public int Id { get; set; }
        public string Categoria { get; set; }
        public bool EsPadre { get; set; }
        public List<CategoriaGasto> SubCategorias { get; set; }

        public CategoriaGasto()
        {
            this.Id = ConstantesGenerales.IdPorDefecto;
            this.Categoria = string.Empty;
            this.EsPadre = false;
            this.SubCategorias = new List<CategoriaGasto>();
        }


        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(this.Categoria);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.SubCategorias);
            stringBuilder.Append(ConstantesGenerales.Separador);

            return stringBuilder.ToString();
        }

        public override bool Equals(object obj)
        {
            bool equals = false;
            if (obj != null && obj.GetType().Equals(this.GetType()))
            {
                CategoriaGasto categoriaGasto = (CategoriaGasto)obj;
                equals = this.Categoria.Equals(categoriaGasto.Categoria);
            }
            return equals;
        }
    }
}
