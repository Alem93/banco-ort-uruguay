﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class Transaccion
    {
        public int Id { get; set; }
        public Cuenta CuentaOrigen { get; set; }
        public Cuenta CuentaDestino { get; set; }
        public double CantidadDebitada { get; set; }
        public string CategoriaDeGasto { get; set; }
        public DateTime Fecha { get; set; }

        public Transaccion()
        {
            this.Id = ConstantesGenerales.IdPorDefecto;
            this.CuentaOrigen = new Cuenta();
            this.CuentaDestino = new Cuenta();
            this.CantidadDebitada = 0;
            this.CategoriaDeGasto = string.Empty;
            this.Fecha = DateTime.Now;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(this.CuentaOrigen.ToString());
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.CantidadDebitada);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.CuentaDestino.ToString());
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.CategoriaDeGasto);
            stringBuilder.Append(ConstantesGenerales.Separador);
            stringBuilder.Append(this.Fecha);
            stringBuilder.Append(ConstantesGenerales.Separador);

            return stringBuilder.ToString();
        }

        public override bool Equals(object obj)
        {
            bool equals = false;
            if (obj != null && obj.GetType().Equals(this.GetType()))
            {
                Transaccion transaccion = (Transaccion)obj;
                equals = this.CuentaOrigen.Equals(transaccion.CuentaOrigen) &&
                    this.CuentaDestino.Equals(transaccion.CuentaDestino) &&
                    this.CantidadDebitada.Equals(transaccion.CantidadDebitada);
            }
            return equals;
        }
    }
}
