﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class ColeccionTransacciones
    {
        private List<Transaccion> Transacciones;

        public ColeccionTransacciones()
        {
            this.Transacciones = new List<Transaccion>();
        }


        public void AgregarTransaccion(Transaccion unaTransaccion)
        {
            this.Transacciones.Add(unaTransaccion);
        }

        public bool ExisteTransaccion(Transaccion transaccionBuscada)
        {
            return this.Transacciones.Any(unaTransaccion => transaccionBuscada.Equals(unaTransaccion)); ;
        }

        public int ObtenerNumeroDeTransaccionesRegistradas()
        {
            return this.Transacciones.Count;
        }

        public List<Transaccion> ObtenerColeccionTransaccionesEnUnaLista()
        {
            return this.Transacciones;
        }
    }
}
