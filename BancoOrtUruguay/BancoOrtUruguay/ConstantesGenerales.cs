﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class ConstantesGenerales
    {
        private static ConstantesGenerales instance = null;

        private ConstantesGenerales() { }

        public static int IdPorDefecto = -1;
        public static char Separador = '\t';
        public static ConstantesGenerales GetInstance()
        {
            if (instance == null)
                instance = new ConstantesGenerales();

            return instance;
        }
    }
}
