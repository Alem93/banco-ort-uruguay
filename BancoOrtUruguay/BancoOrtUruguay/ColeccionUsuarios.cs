﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoOrtUruguay
{
    public class ColeccionUsuarios
    {
        private List<Usuario> Usuarios;


        public ColeccionUsuarios()
        {
            this.Usuarios = new List<Usuario>();
        }
        public void AgregarUsuario(Usuario unUsuario)
        {
            this.Usuarios.Add(unUsuario);
        }

        public Usuario ObtenerUsuarioLogueado(int cedula, string contrasenia)
        {
            return this.Usuarios.Find(
                unUsuario => unUsuario.Cedula.Equals(cedula) && unUsuario.Contrasenia.Equals(contrasenia));
        }

        public int ObtenerNumeroDeUsuariosRegistrados()
        {
            return this.Usuarios.Count;
        }

        public bool ExisteUsuario(Usuario usuarioBuscado)
        {
            return this.Usuarios.Any(unUsuario => usuarioBuscado.Equals(unUsuario));
        }

        public Usuario[] ObtenerColeccionEnUnArray(TIPO_DE_USUARIO rolUsuario)
        {
            List<Usuario> usuarioRol = this.Usuarios.FindAll(unUsuario => unUsuario.RolesDeUsuario.Contains(rolUsuario));
            return usuarioRol.ToArray();
        }

        public List<Usuario> ObtenerColeccionUsuariosEnUnaLista()
        {
            return this.Usuarios;
        }
    }
}

